//
//  CTAppManager.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/5/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTBOUser.h"
#import "CTBOArea.h"

#define CTAppManagerInstance  [CTAppManager sharedInstance]

static NSString *const kAreaDidChangeNotification = @"kAreaDidChangeNotification";

@interface CTAppManager : NSObject

CT_DECLARE_SINGLETON(CTAppManager);

@property (nonatomic, strong) CTBOUser* currentUser;
@property (nonatomic, strong) CTBOArea* currentArea;
@property (nonatomic, strong) NSArray* allAreas;

- (NSString *)deviceId;

- (void)showRootController;

- (void)switchToLoginScreen;
- (void)switchToHomeScreen;

- (void)saveCurrentUser;

- (void)logout;

@end
