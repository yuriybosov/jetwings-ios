//
//  CTAppManager.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/5/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTAppManager.h"
#import "CTLoginController.h"
#import "CTDrawerController.h"
#import "AppDelegate.h"


#define kCTCurrentUser @"kCTCurrentUser"
#define kCTCurrentArea @"kCTCurrentArea"
#define kCTAllArea @"kCTAllArea"

@implementation CTAppManager

CT_IMPLEMENT_SINGLETON(CTAppManager);

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(languagedidChangeNotification) name:kLanguageDidChangeNotification object:nil];
        
        NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
        NSData* serializedCurrentUser = [ud objectForKey:kCTCurrentUser];
        if (serializedCurrentUser)
            _currentUser = [NSKeyedUnarchiver unarchiveObjectWithData:serializedCurrentUser];
        
        
        NSData* serializedCurrentArea = [ud objectForKey:kCTCurrentArea];
        if (serializedCurrentArea)
            _currentArea = [NSKeyedUnarchiver unarchiveObjectWithData:serializedCurrentArea];

        NSData* serializedAllArea = [ud objectForKey:kCTAllArea];
        if (serializedAllArea)
        {
            _allAreas = [NSKeyedUnarchiver unarchiveObjectWithData:serializedAllArea];
        }
        else
        {
            CTBOArea *singapore = [CTBOArea new];
            singapore.ID = @(1);
            singapore.title = CTLocalizedString(@"Singapore");
            
            CTBOArea *bintan = [CTBOArea new];
            bintan.ID = @(2);
            bintan.title = CTLocalizedString(@"Bintan");
            
            self.currentArea = singapore;
            self.allAreas = @[singapore, bintan];
        }
    }
    return self;
}

- (void)languagedidChangeNotification
{
    CTBOArea *singapore = [_allAreas objectAtIndex:0];
    singapore.title = CTLocalizedString(@"Singapore");
    
    CTBOArea *bintan = [_allAreas objectAtIndex:1];
    bintan.title = CTLocalizedString(@"Bintan");
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSData *serialized = [NSKeyedArchiver archivedDataWithRootObject:_allAreas];
    [ud setObject:serialized forKey:kCTAllArea];
    [ud synchronize];
}

- (void)setCurrentUser:(CTBOUser *)aCurrentUser
{
    _currentUser = aCurrentUser;
    [self saveCurrentUser];
}

- (void)setAllAreas:(NSArray *)allAreas
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if (allAreas.count)
    {
        _allAreas = allAreas;
        
        NSData *serialized = [NSKeyedArchiver archivedDataWithRootObject:_allAreas];
        [ud setObject:serialized forKey:kCTAllArea];
    }
    [ud synchronize];
}

- (void)setCurrentArea:(CTBOArea *)aCurrentArea
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if (aCurrentArea && [_currentArea.ID integerValue] != [aCurrentArea.ID integerValue])
    {
        _currentArea = aCurrentArea;
        
        NSData *serialized = [NSKeyedArchiver archivedDataWithRootObject:_currentArea];
        [ud setObject:serialized forKey:kCTCurrentArea];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kAreaDidChangeNotification object:nil];
    }
}

- (void)saveCurrentUser
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if (_currentUser)
    {
        NSData *serializedCurrentUser = [NSKeyedArchiver archivedDataWithRootObject:_currentUser];
        [ud setObject:serializedCurrentUser forKey:kCTCurrentUser];
    }
    else
    {
        [ud removeObjectForKey:kCTCurrentUser];
    }
    [ud synchronize];
}

- (NSString *)deviceId
{
    NSString *deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:@"kDeviceId"];
    if (deviceId.length > 0)
    {
        return deviceId;
    }
    
    CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
    deviceId = (NSString *)CFBridgingRelease(CFUUIDCreateString (kCFAllocatorDefault,uuidRef));
    [[NSUserDefaults standardUserDefaults] setObject:deviceId forKey:@"kDeviceId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return deviceId;
}

#pragma mark - Switch To Screen

- (void)showRootController
{
    if (self.currentUser)
    {
        [self switchToHomeScreen];
    }
    else
    {
        [self switchToLoginScreen];
    }
}

- (void)switchToLoginScreen
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Start" bundle:nil];
    CTLoginController *loginVC = [sb instantiateViewControllerWithIdentifier:kLoginScreenStoryBoardId];
    
    ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController = loginVC;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

- (void)switchToHomeScreen
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
    CTDrawerController *drawerController = [sb instantiateViewControllerWithIdentifier:kDrawerScreenStoryBoardId];
    
    ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController = drawerController;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

- (void)logout
{
    // TODO add request
    
    self.currentUser = nil;
    [self switchToLoginScreen];
}

@end
