//
//  CTTranslateManger.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/11/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *const kLanguageCH = @"zh";
static NSString *const kLanguageEN = @"en"; // default en

static NSString *const kLanguageDidChangeNotification = @"kLanguageDidChangeNotification";

NSString *CTLocalizedString(NSString *key);

#define CTLanguageMangerInstance  [CTLanguageManger sharedInstance]

@interface CTLanguageManger : NSObject
{
    NSDictionary *chData;
    NSDictionary *enData;
}

@property (nonatomic, strong) NSString *currentLanguage;

CT_DECLARE_SINGLETON(CTLanguageManger);

- (NSString *)localizedString:(NSString *)key;

- (NSString *)langNameByID:(NSString *)langID;

@end
