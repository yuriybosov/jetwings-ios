//
//  CTTranslateManger.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/11/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTLanguageManger.h"


NSString *CTLocalizedString(NSString *key)
{
    return [[CTLanguageManger sharedInstance] localizedString:key];
}

@interface CTLanguageManger ()

- (NSDictionary *)parsedLanguagePlistFile:(NSString *)plistFileName;

@end

@implementation CTLanguageManger

CT_IMPLEMENT_SINGLETON(CTLanguageManger);

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _currentLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentLanguage"];
        if (!_currentLanguage)
        {
            _currentLanguage = kLanguageEN;
            [[NSUserDefaults standardUserDefaults] setObject:_currentLanguage forKey:@"currentLanguage"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        chData = [self parsedLanguagePlistFile:@"translite_ch"];
        enData = [self parsedLanguagePlistFile:@"translite_en"];
    }
    return self;
}

- (NSDictionary *)parsedLanguagePlistFile:(NSString *)plistFileName;
{
    NSArray *array = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:plistFileName ofType:@"plist"]];
    NSString *key = nil;
    NSString *value = nil;
    
    NSMutableDictionary * result = [NSMutableDictionary new];
    
    for (NSDictionary *data in array)
    {
        key = [data objectForKey:@"key"];
        value = [data objectForKey:@"value"];
        
        if (key && value)
        {
            if (![result objectForKey:key])
            {
                [result setObject:value forKey:key];
            }
            else
            {
                CTLog(@"Language Parsing Error - duplicate data: key = %@, value = %@", key, value);
            }
        }
        else
        {
            CTLog(@"Language Parsing Error - empty data: key = %@, value = %@", key, value);
        }
    }
    
    return result;
}

- (NSString *)localizedString:(NSString *)key
{
    NSString *result = nil;
    
    if ([self.currentLanguage isEqualToString:kLanguageCH])
    {
        result = [chData objectForKey:key];
    }
    else
    {
        result = [enData objectForKey:key];
    }
    
    if (!result)
    {
        result = key;
        CTLog(@"Localized String Not Found: %@", key);
    }
    
    return result;
}

- (void)setCurrentLanguage:(NSString *)currentLanguage
{
    if (![_currentLanguage isEqualToString:currentLanguage])
    {
        _currentLanguage = currentLanguage;
        
        [[NSUserDefaults standardUserDefaults] setObject:_currentLanguage forKey:@"currentLanguage"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kLanguageDidChangeNotification object:nil];
    }
}

- (NSString *)langNameByID:(NSString *)langID
{
    return [langID isEqualToString:kLanguageEN] ? @"English" : @"中文";
}

@end
