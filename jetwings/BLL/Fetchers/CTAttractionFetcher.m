//
//  CTAttractionFetcher.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTAttractionFetcher.h"
#import "CTHotelsGateway.h"
#import "BOAttraction.h"

@implementation CTAttractionFetcher

- (CTDataBaseRequest *)dataBaseRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [BOAttraction attractionsList];
}

- (CTGatewayRequest *)gatewayRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [[CTHotelsGateway sharedInstance] attractionsList];
}

@end
