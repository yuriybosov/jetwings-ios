//
//  CTBaseFetcherIntoStorage.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseFetcherIntoStorage.h"

@implementation CTBaseFetcherIntoStorage

- (id)initWithStorage:(CTStorage *)aStorage
{
    self = [super initWithStorage:aStorage];
    if (self)
    {
        self.fetchFromDataBaseWhenGatewayRequestFailed = YES;
        self.callbackQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    }
    return self;
}

@end
