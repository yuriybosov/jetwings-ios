//
//  CTGuidePricingFetcher.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/20/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTGuidePricingFetcher.h"
#import "BOGuidePricing.h"
#import "CTHotelsGateway.h"


@implementation CTGuidePricingFetcher

- (CTDataBaseRequest *)dataBaseRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [BOGuidePricing guidesPricingList];
}

- (CTGatewayRequest *)gatewayRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [[CTHotelsGateway sharedInstance] guidesPricingList];
}

@end
