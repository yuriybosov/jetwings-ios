//
//  CTGuidesCompoundFetcher.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/20/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTGuidesCompoundFetcher.h"
#import "CTGuidesFetcher.h"
#import "CTGuidePricingFetcher.h"

@implementation CTGuidesCompoundFetcher

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.callbackQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        [self addFetcher:[[CTGuidesFetcher alloc] initWithStorage:CTMainStorageInstance]];
        [self addFetcher:[[CTGuidePricingFetcher alloc] initWithStorage:CTMainStorageInstance]];
    }
    return self;
}

- (CTResponse *)createFetcherResponse
{
    CTResponse *response = [CTResponse new];
    
    NSMutableArray* models = [NSMutableArray array];
    BOOL success = (self.successResponseIfAtLeastOne) ? NO : YES;
    
    NSUInteger index = 0;
    for(CTCompoundFetcherNode* node in fetcherNodes)
    {
        if (node.response)
        {
            if ([node.fetcher isKindOfClass:[CTGuidesFetcher class]] &&
                node.response.boArray.count)
            {
                [response.dataDictionary setObject:node.response.boArray forKey:@"Guides"];
            }
            else if ([node.fetcher isKindOfClass:[CTGuidePricingFetcher class]] &&
                     node.response.boArray.count)
            {
                [response.dataDictionary setObject:node.response.boArray forKey:@"Pricings"];
            }
            
            if (self.successResponseIfAtLeastOne)
                success |= node.response.success;
            else
                success &= node.response.success;
        }
        
        index++;
    }
    
    response.success = success;
    response.boArray = models;
    
    return response;
}

@end
