    //
//  CTGuidesFetcher.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/20/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTGuidesFetcher.h"
#import "BOGuide.h"
#import "CTHotelsGateway.h"


@implementation CTGuidesFetcher

- (CTDataBaseRequest *)dataBaseRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [BOGuide guidesList];
}

- (CTGatewayRequest *)gatewayRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [[CTHotelsGateway sharedInstance] guidesList];
}

@end
