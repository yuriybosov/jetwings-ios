//
//  CTHotelListFetcher.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTHotelListFetcher.h"
#import "CTHotelsGateway.h"
#import "BOHotel.h"

@implementation CTHotelListFetcher

- (CTDataBaseRequest *)dataBaseRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [BOHotel hotelsListByFilter:[aMessage.defaultParameters objectForKey:kFetcherMessageFilter]];
}

- (CTGatewayRequest *)gatewayRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [[CTHotelsGateway sharedInstance] hotelsListByFilter:[aMessage.defaultParameters objectForKey:kFetcherMessageFilter]];
}

@end
