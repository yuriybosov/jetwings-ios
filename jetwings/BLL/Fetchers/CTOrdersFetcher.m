//
//  CTOrdersFetcher.m
//  jetwings
//
//  Created by Yuriy Bosov on 2/4/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTOrdersFetcher.h"
#import "BOOrder.h"
#import "CTHotelsGateway.h"


@implementation CTOrdersFetcher

- (CTDataBaseRequest *)dataBaseRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [BOOrder ordersList];
}

- (CTGatewayRequest *)gatewayRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [[CTHotelsGateway sharedInstance] ordersList];
}

@end
