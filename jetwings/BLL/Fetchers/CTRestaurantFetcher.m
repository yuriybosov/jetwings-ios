//
//  CTRestaurantFetcher.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTRestaurantFetcher.h"
#import "CTHotelsGateway.h"
#import "BORestaurant.h"

@implementation CTRestaurantFetcher

- (CTDataBaseRequest *)dataBaseRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [BORestaurant restaurantsList];
}

- (CTGatewayRequest *)gatewayRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [[CTHotelsGateway sharedInstance] restaurantsList];
}

@end
