
//
//  CTTransportItemisedFetcher.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/16/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTTransportItemisedFetcher.h"
#import "BOTransportItemised.h"
#import "CTHotelsGateway.h"


@implementation CTTransportItemisedFetcher

- (CTDataBaseRequest *)dataBaseRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [BOTransportItemised transportItemisedList];
}

- (CTGatewayRequest *)gatewayRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [[CTHotelsGateway sharedInstance] transportItemisedList];
}

@end
