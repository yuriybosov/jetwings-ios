//
//  CTTransportPackagesFetcher.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/16/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTTransportPackagesFetcher.h"
#import "BOTransportPackages.h"
#import "CTHotelsGateway.h"


@implementation CTTransportPackagesFetcher

- (CTDataBaseRequest *)dataBaseRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [BOTransportPackages transportPackagesList];
}

- (CTGatewayRequest *)gatewayRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [[CTHotelsGateway sharedInstance] transportPackagesList];
}

@end
