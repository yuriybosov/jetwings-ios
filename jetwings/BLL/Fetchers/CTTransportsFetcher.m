//
//  CTTransportsFetcher.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/16/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTTransportsFetcher.h"
#import "CTTransportItemisedFetcher.h"
#import "CTTransportPackagesFetcher.h"
#import "CTVehicleFetcher.h"

@implementation CTTransportsFetcher

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.callbackQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        [self addFetcher:[[CTTransportItemisedFetcher alloc] initWithStorage:CTMainStorageInstance]];
        [self addFetcher:[[CTTransportPackagesFetcher alloc] initWithStorage:CTMainStorageInstance]];
        [self addFetcher:[[CTVehicleFetcher alloc] initWithStorage:CTMainStorageInstance]];
    }
    return self;
}

- (CTResponse *)createFetcherResponse
{
    CTResponse *response = [CTResponse new];
    
    NSMutableArray* models = [NSMutableArray array];
    BOOL success = (self.successResponseIfAtLeastOne) ? NO : YES;
    
    NSUInteger index = 0;
    for(CTCompoundFetcherNode* node in fetcherNodes)
    {
        if (node.response)
        {
            if ([node.fetcher isKindOfClass:[CTTransportItemisedFetcher class]] &&
                node.response.boArray.count)
            {
                [response.dataDictionary setObject:node.response.boArray forKey:@"Itemised"];
            }
            else if ([node.fetcher isKindOfClass:[CTTransportPackagesFetcher class]] &&
                     node.response.boArray.count)
            {
                [response.dataDictionary setObject:node.response.boArray forKey:@"Packages"];
            }
            else if ([node.fetcher isKindOfClass:[CTVehicleFetcher class]] &&
                     node.response.boArray.count)
            {
                [response.dataDictionary setObject:node.response.boArray forKey:@"Vehicle"];
            }
            
            if (self.successResponseIfAtLeastOne)
                success |= node.response.success;
            else
                success &= node.response.success;
        }
        
        index++;
    }
    
    response.success = success;
    response.boArray = models;
    
    return response;
}

@end
