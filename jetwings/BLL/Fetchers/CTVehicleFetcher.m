//
//  CTVehicleFetcher.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/16/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTVehicleFetcher.h"
#import "BOVehicle.h"
#import "CTHotelsGateway.h"

@implementation CTVehicleFetcher

- (CTDataBaseRequest *)dataBaseRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [BOVehicle vehicleList];
}

- (CTGatewayRequest *)gatewayRequestByMessage:(CTFetcherMessage *)aMessage
{
    return [[CTHotelsGateway sharedInstance] vehicleList];
}

@end
