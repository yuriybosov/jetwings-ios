//
//  UIConfigurator.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <Foundation/Foundation.h>

#define UIConfiguratorInstance  [UIConfigurator sharedInstance]

@interface UIConfigurator : NSObject

CT_DECLARE_SINGLETON(UIConfigurator);

- (void)customizeHomeButton:(UIButton *)button;

- (UIBarButtonItem *)menuButton;
- (UIBarButtonItem *)backButton;

- (CTToolbar*)defaultPickerToolbarWithTitle:(NSString *)title;

@end
