//
//  UIConfigurator.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "UIConfigurator.h"

@implementation UIConfigurator

CT_IMPLEMENT_SINGLETON(UIConfigurator);

- (void)customizeHomeButton:(UIButton *)button
{
    [button setExclusiveTouch:YES];
    
    // the space between the image and text
    CGFloat spacing = 15.0;
    
    // lower the text and push it left so it appears centered
    //  below the image
    CGSize imageSize = [button imageForState:UIControlStateNormal].size;
    button.titleEdgeInsets = UIEdgeInsetsMake(0.0, - imageSize.width, - (imageSize.height + spacing), 0.0);
    
    // raise the image and push it right so it appears centered
    //  above the text
    CGSize titleSize = [button.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: button.titleLabel.font}];
    button.imageEdgeInsets = UIEdgeInsetsMake(- (titleSize.height + spacing), 0.0, 0.0, - titleSize.width);
}

- (UIBarButtonItem *)menuButton
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 44, 44);
    [btn setImage:[UIImage imageNamed:@"ic_hamburger"] forState:UIControlStateNormal];
    return [[UIBarButtonItem alloc] initWithCustomView:btn];
}

- (UIBarButtonItem *)backButton
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 44, 44);
    [btn setImage:[UIImage imageNamed:@"ic_back_normal"] forState:UIControlStateNormal];
    return [[UIBarButtonItem alloc] initWithCustomView:btn];
}

- (CTToolbar*)defaultPickerToolbarWithTitle:(NSString *)title
{
    CTToolbar* toolbar = [[CTToolbar alloc] init];
    toolbar.backgroundColor = [UIColor colorWithRedI:231 greenI:234 blueI:240 alphaI:255];
    
    NSMutableArray* items = [NSMutableArray array];
    UIBarButtonItem* bbi;
    CTAutoresizeButton* button;
    
    bbi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    bbi.width = 5;
    [items addObject:bbi];
    
    button = [[CTAutoresizeButton alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
    [button setAutoresizeTitle:title];
    [button setTitleColor:[UIColor colorWithRedI:118 greenI:118 blueI:118 alphaI:255] forState:UIControlStateNormal];
    button.titleLabel.shadowOffset = CGSizeMake(0, 0.0f);
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
    button.titleLabel.textAlignment = NSTextAlignmentLeft;
    bbi = [[UIBarButtonItem alloc] initWithCustomView:button];
    bbi.enabled = NO;
    [items addObject:bbi];
    
    bbi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [items addObject:bbi];
    
    button = [[CTAutoresizeButton alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
    [button setAutoresizeTitle:CTLocalizedString(@"Done")];
    [button setTitleColor:[UIColor colorWithRedI:57 greenI:169 blueI:255 alphaI:255] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    button.titleLabel.shadowOffset = CGSizeMake(0, 0.0f);
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
    button.titleLabel.textAlignment = NSTextAlignmentRight;
    bbi = [[UIBarButtonItem alloc] initWithCustomView:button];
    [items addObject:bbi];
    
    toolbar.items = items;
    return toolbar;
}

@end
