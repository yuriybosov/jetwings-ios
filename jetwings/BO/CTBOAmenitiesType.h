//
//  CTBOAmenitiesType.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOBaseObject.h"

@interface CTBOAmenitiesType : CTBOBaseObject

@property (nonatomic, readonly) NSString *name;

@end
