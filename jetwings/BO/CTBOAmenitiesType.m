//
//  CTBOAmenitiesType.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOAmenitiesType.h"

@implementation CTBOAmenitiesType

- (void)setupWithData:(NSDictionary *)aData
{
    [super setupWithData:aData];
    _name = [aData nullProtectedObjectForKey:@"name"];
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    {
        _name = [aDecoder decodeObjectForKey:@"name"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.name forKey:@"name"];
}

@end
