//
//  CTBOArea.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOArea.h"

@implementation CTBOArea

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    {
        ID = [aDecoder decodeObjectForKey:@"id"];
        title = [aDecoder decodeObjectForKey:@"title"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:ID forKey:@"id"];
    [aCoder encodeObject:title forKey:@"title"];
}

@end
