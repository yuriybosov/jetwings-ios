//
//  CTBOBaseObject.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTBOBaseObject : NSObject  <NSCoding>

@property (nonatomic, readonly) NSNumber *ID;

+ (id)initializeWithData:(NSDictionary *)aData;

- (void)setupWithData:(NSDictionary *)aData;

- (NSString *)descriptionText;

@end
