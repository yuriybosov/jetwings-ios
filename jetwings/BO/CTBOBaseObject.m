//
//  CTBOBaseObject.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOBaseObject.h"

@implementation CTBOBaseObject

+ (id)initializeWithData:(NSDictionary *)aData
{
    CTBOBaseObject *obj = [[self class] new];
    [obj setupWithData:aData];
    return obj;
}

- (void)setupWithData:(NSDictionary *)aData
{
    _ID = [aData nullProtectedObjectForKey:@"id"];
}

- (NSString *)descriptionText
{
    return nil;
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    {
        _ID = [aDecoder decodeObjectForKey:@"id"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.ID forKey:@"id"];
}

@end
