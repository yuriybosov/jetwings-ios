//
//  CTBOHotelFilter.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum CTHotelFilterSorted
{
    CTHotelSortedTypeStarAsc,
    CTHotelSortedTypeStarDesc,
    CTHotelSortedTypePriceAsc,
    CTHotelSortedTypePriceDesc
    
}CTHotelSortedType;

@interface CTBOHotelFilter : NSObject

@property (nonatomic, assign) CTHotelSortedType sortedType;

- (NSDictionary *)gatewayRequestParams;

- (BOOL)isAsc;
- (BOOL)isSortedByStar;

@end
