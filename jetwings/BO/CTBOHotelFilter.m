//
//  CTBOHotelFilter.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOHotelFilter.h"

@implementation CTBOHotelFilter

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.sortedType = CTHotelSortedTypePriceDesc;
    }
    return self;
}

- (NSDictionary *)gatewayRequestParams
{
    NSDictionary *result = nil;
    if (self.sortedType == CTHotelSortedTypeStarAsc)
    {
        result = @{@"sort" : @"stars",
                   @"order" : @"asc"};
    }
    else if (self.sortedType == CTHotelSortedTypeStarDesc)
    {
        result = @{@"sort" : @"stars",
                   @"order" : @"desc"};
    }
    else if (self.sortedType == CTHotelSortedTypePriceAsc)
    {
        result = @{@"sort" : @"price",
                   @"order" : @"asc"};
    }
    else
    {
        result = @{@"sort" : @"price",
                   @"order" : @"desc"};
    }

    return result;
}

- (BOOL)isAsc
{
    return self.sortedType == CTHotelSortedTypeStarAsc || self.sortedType == CTHotelSortedTypePriceAsc;
}

- (BOOL)isSortedByStar
{
    return self.sortedType == CTHotelSortedTypeStarAsc || self.sortedType == CTHotelSortedTypeStarDesc;
}

@end
