//
//  CTBOHotelsRoom.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOBaseObject.h"
#import "CTBOPricing.h"
#import "CTPriceCellProtocol.h"

@interface CTBOHotelsRoom : CTBOBaseObject <CTPriceCellProtocol>

@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *size;
@property (nonatomic, readonly) NSArray *prising;

- (NSString *)allDates;
- (NSString *)allWeekDays;
- (NSString *)allPrices;

@end
