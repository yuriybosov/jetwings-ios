//
//  CTBOHotelsRoom.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOHotelsRoom.h"

@implementation CTBOHotelsRoom

- (void)setupWithData:(NSDictionary *)aData
{
    [super setupWithData:aData];
    
    _name = [aData nullProtectedObjectForKey:@"name"];
    _size = [aData nullProtectedObjectForKey:@"size"];
    
    NSArray* pricing = [aData nullProtectedObjectForKeyPath:@"pricing"];
    if ([pricing isKindOfClass:[NSArray class]] && pricing.count)
    {
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *data in pricing)
        {
            CTBOPricing* price = [CTBOPricing initializeWithData:data];
            if ([price.rate integerValue] > 0)
            {
                [tempArray addObject:price];
            }
        }
        
        if (tempArray.count)
        {
            _prising = [NSArray arrayWithArray:tempArray];
        }
    }
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    {
        _name = [aDecoder decodeObjectForKey:@"name"];
        _size = [aDecoder decodeObjectForKey:@"size"];
        _prising = [aDecoder decodeObjectForKey:@"pricing"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.size forKey:@"size"];
    [aCoder encodeObject:self.prising forKey:@"prising"];
}

#pragma mark - CTPriceCellProtocol

- (NSString *)priceTitle
{
    return self.name;
}

- (NSString *)priceDescriprion
{
    if (self.size.integerValue > 0)
        return [NSString stringWithFormat:@"%@: %@ m²", CTLocalizedString(@"Room size"), self.size];
    
    return nil;
}

- (NSNumber *)priceValue
{
    float temp = 0;
    for (CTBOPricing *price in self.prising)
    {
        temp += [[price rate] doubleValue];
    }
    
    return @(temp);
}

- (NSNumber *)priceIdentifier
{
    return self.ID;
}

- (NSString *)allDates
{
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      dateFormatter = [NSDateFormatter new];
                      dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
                      dateFormatter.dateFormat = @"dd MMM";
                  });
    NSMutableArray *dates = [NSMutableArray new];
    
    for (CTBOPricing *price in self.prising)
    {
        [dates addObject:[dateFormatter stringFromDate:price.date]];
    }
    
    return [dates componentsJoinedByString:@"\n\n"];
}

- (NSString *)allWeekDays
{
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      dateFormatter = [NSDateFormatter new];
                      dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
                      dateFormatter.dateFormat = @"EEEE";
                  });
    NSMutableArray *dates = [NSMutableArray new];
    
    for (CTBOPricing *price in self.prising)
    {
        [dates addObject:[dateFormatter stringFromDate:price.date]];
    }
    
    return [dates componentsJoinedByString:@"\n\n"];
}

- (NSString *)allPrices
{
    NSMutableArray *prices = [NSMutableArray new];
    
    for (CTBOPricing *price in self.prising)
    {
        [prices addObject:[NSString stringWithFormat:@"%@%0.2f",[CTAppManagerInstance.currentUser currencySymbol], [price.priceValue doubleValue]]];
    }
    
    return [prices componentsJoinedByString:@"\n\n"];
}

@end
