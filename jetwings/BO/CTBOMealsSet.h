//
//  CTBOMealsSet.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/18/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOBaseObject.h"
#import "CTBOMenuSet.h"
#import "CTPriceCellProtocol.h"

@interface CTBOMealsSet : CTBOBaseObject <CTPriceCellProtocol>

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *price;
@property (nonatomic, copy) NSString *picture;

@property (nonatomic, strong) NSArray *menuSets;

- (NSString *)menuSetsText;

@end
