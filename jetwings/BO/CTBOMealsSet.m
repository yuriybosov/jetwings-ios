//
//  CTBOMealsSet.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/18/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOMealsSet.h"

@implementation CTBOMealsSet

- (void)setupWithData:(NSDictionary *)aData
{
    [super setupWithData:aData];
    
    self.name = [aData nullProtectedObjectForKey:@"name"];
    self.price = [aData nullProtectedObjectForKey:@"price"];
    self.picture = [aData nullProtectedObjectForKey:@"picture"];
    
    NSArray *array = [aData nullProtectedObjectForKey:@"menuSet"];
    if ([array isKindOfClass:[NSArray class]] &&
        array.count)
    {
        NSMutableArray *menuSetsTemp = [NSMutableArray new];
        for (NSDictionary *menuData in array)
        {
            CTBOMenuSet *menu = [[CTBOMenuSet alloc] init];
            [menu setupWithData:menuData];
            [menuSetsTemp addObject:menu];
        }
        self.menuSets = [NSArray arrayWithArray:menuSetsTemp];
    }
}

- (NSString *)menuSetsText
{
    NSString *result = nil;
    
    if (self.menuSets.count)
    {
        NSMutableArray *temp = [NSMutableArray array];
        for (CTBOMenuSet *menuSet in self.menuSets)
        {
            [temp addObject:[NSString stringWithFormat:@"%@", menuSet.descriptionText ?: menuSet.name]];
        }
        result = [temp componentsJoinedByString:@"\n\n"];
    }
    
    return result;
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    {
        _name = [aDecoder decodeObjectForKey:@"name"];
        _price = [aDecoder decodeObjectForKey:@"price"];
        _picture = [aDecoder decodeObjectForKey:@"picture"];
        _menuSets = [aDecoder decodeObjectForKey:@"menuSets"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:_name forKey:@"name"];
    [aCoder encodeObject:_price forKey:@"price"];
    [aCoder encodeObject:_picture forKey:@"picture"];
    [aCoder encodeObject:_menuSets forKey:@"menuSets"];
}

#pragma mark - CTPriceCellProtocol

- (NSString *)priceTitle
{
    return _name;
}

- (NSString *)priceDescriprion
{
    return nil;
}

- (NSNumber *)priceValue
{
    return _price;
}

- (NSNumber *)priceIdentifier
{
    return self.ID;
}

@end
