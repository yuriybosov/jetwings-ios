//
//  CTMenuSet.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/18/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOBaseObject.h"

@interface CTBOMenuSet : CTBOBaseObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *descriptionText;

@end
