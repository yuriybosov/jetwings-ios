//
//  CTMenuSet.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/18/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOMenuSet.h"

@implementation CTBOMenuSet

- (void)setupWithData:(NSDictionary *)aData
{
    [super setupWithData:aData];
    
    self.name = [aData nullProtectedObjectForKey:@"name"];
    self.descriptionText = [aData nullProtectedObjectForKey:@"description"];
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    {
        _name = [aDecoder decodeObjectForKey:@"name"];
        _descriptionText = [aDecoder decodeObjectForKey:@"description"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:_name forKey:@"name"];
    [aCoder encodeObject:_descriptionText forKey:@"description"];
}

@end
