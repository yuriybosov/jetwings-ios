//
//  CTBONearbyAttraction.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOBaseObject.h"

@interface CTBONearbyAttraction : CTBOBaseObject

@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *picture;
@property (nonatomic, readonly) NSString *work;

@end
