//
//  CTBONearbyHotel.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/17/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOBaseObject.h"

@interface CTBONearbyHotel : CTBOBaseObject

@property (nonatomic, readonly) NSNumber *stars;
@property (nonatomic, readonly) NSString *picture;
@property (nonatomic, readonly) NSNumber *price;
@property (nonatomic, readonly) NSString *name;

@end
