//
//  CTBONearbyHotel.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/17/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBONearbyHotel.h"

@implementation CTBONearbyHotel

- (void)setupWithData:(NSDictionary *)aData
{
    [super setupWithData:aData];
    
    _stars = [aData nullProtectedObjectForKey:@"stars"];
    _name = [aData nullProtectedObjectForKey:@"name"];
    _picture = [aData nullProtectedObjectForKey:@"picture"];
    _price = [aData nullProtectedObjectForKey:@"price"];
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    {
        _stars = [aDecoder decodeObjectForKey:@"stars"];
        _name = [aDecoder decodeObjectForKey:@"name"];
        _picture = [aDecoder decodeObjectForKey:@"picture"];
        _price = [aDecoder decodeObjectForKey:@"price"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.stars forKey:@"stars"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.picture forKey:@"picture"];
    [aCoder encodeObject:self.price forKey:@"price"];
}

@end
