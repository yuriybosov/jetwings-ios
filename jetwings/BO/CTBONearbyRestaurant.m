//
//  CTNearbyRestaurant.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBONearbyRestaurant.h"

@implementation CTBONearbyRestaurant

- (void)setupWithData:(NSDictionary *)aData
{
    [super setupWithData:aData];
    
    _category = [aData nullProtectedObjectForKey:@"category"];
    _name = [aData nullProtectedObjectForKey:@"name"];
    _picture = [aData nullProtectedObjectForKey:@"picture"];
    _work = [aData nullProtectedObjectForKey:@"work"];
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    {
        _category = [aDecoder decodeObjectForKey:@"category"];
        _name = [aDecoder decodeObjectForKey:@"name"];
        _picture = [aDecoder decodeObjectForKey:@"picture"];
        _work = [aDecoder decodeObjectForKey:@"work"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.category forKey:@"category"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.picture forKey:@"picture"];
    [aCoder encodeObject:self.work forKey:@"work"];
}

@end
