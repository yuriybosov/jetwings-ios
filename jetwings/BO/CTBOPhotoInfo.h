//
//  CTBOPhotoInfo.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/14/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOBaseObject.h"

@interface CTBOPhotoInfo : CTBOBaseObject

@property (nonatomic, strong) NSString *photoPath;

@end
