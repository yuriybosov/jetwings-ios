//
//  CTBOPhotoInfo.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/14/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOPhotoInfo.h"

@implementation CTBOPhotoInfo

- (void)setupWithData:(NSDictionary *)aData
{
    [super setupWithData:aData];
    _photoPath = [aData nullProtectedObjectForKey:@"picture"];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    {
        _photoPath = [aDecoder decodeObjectForKey:@"photoPath"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.photoPath forKey:@"photoPath"];
}

@end
