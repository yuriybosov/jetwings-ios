//
//  CTBOPricing.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOBaseObject.h"
#import "CTPriceCellProtocol.h"

@interface CTBOPricing : CTBOBaseObject <CTPriceCellProtocol>

@property (nonatomic, readonly) NSDate *date;           // use hotel
@property (nonatomic, readonly) NSNumber *rate;         // use hotel

@end
