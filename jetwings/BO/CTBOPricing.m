//
//  CTBOPricing.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOPricing.h"

@implementation CTBOPricing

- (void)setupWithData:(NSDictionary *)aData
{
    [super setupWithData:aData];
    
    static NSDateFormatter *dateFormatterPricing = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      dateFormatterPricing = [NSDateFormatter new];
                      dateFormatterPricing.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
                      dateFormatterPricing.dateFormat = @"yyyy-MM-dd";
                  });
    
    _rate = [aData nullProtectedObjectForKey:@"rate"];
    
    id value = [aData nullProtectedObjectForKey:@"date"];
    if (value && [value isKindOfClass:[NSString class]])
    {
        _date = [dateFormatterPricing dateFromString:value];
    }
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    {
        _rate = [aDecoder decodeObjectForKey:@"rate"];
        _date = [NSDate dateWithTimeIntervalSince1970:[[aDecoder decodeObjectForKey:@"date"] doubleValue]];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.rate forKey:@"rate"];
    [aCoder encodeObject:@([self.date timeIntervalSinceNow]) forKey:@"date"];
}

#pragma mark - CTPriceCellProtocol

- (NSString *)priceTitle
{
    return nil;
}

- (NSString *)priceDescriprion
{
    return nil;
}

- (NSNumber *)priceValue
{
    return _rate;
}

- (NSNumber *)priceIdentifier
{
    return self.ID;
}

@end
