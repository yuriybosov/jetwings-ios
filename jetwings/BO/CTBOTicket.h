//
//  CTBOTicket.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/18/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOBaseObject.h"
#import "CTPriceCellProtocol.h"

@interface CTBOTicket : CTBOBaseObject <CTPriceCellProtocol>

@property (nonatomic, readonly) NSNumber *price;
@property (nonatomic, readonly) NSNumber *jetPrice;
@property (nonatomic, readonly) NSString *name;

@end
