//
//  CTBOTourTicket.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/25/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOBaseObject.h"
#import "CTPriceCellProtocol.h"


@interface CTBOTourTicket : CTBOBaseObject <CTPriceCellProtocol>

@property (nonatomic, readonly) NSNumber *price;
@property (nonatomic, readonly) NSString *name;

@end
