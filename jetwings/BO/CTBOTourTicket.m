//
//  CTBOTourTicket.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/25/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOTourTicket.h"

@implementation CTBOTourTicket

- (void)setupWithData:(NSDictionary *)aData
{
    [super setupWithData:aData];
    
    _price = [aData nullProtectedObjectForKey:@"price"];
    _name = [aData nullProtectedObjectForKey:@"name"];
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    {
        _price = [aDecoder decodeObjectForKey:@"price"];
        _name = [aDecoder decodeObjectForKey:@"name"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:_price forKey:@"price"];
    [aCoder encodeObject:_name forKey:@"name"];
}

#pragma mark - CTPriceCellProtocol

- (NSString *)priceTitle
{
    return _name;
}

- (NSString *)priceDescriprion
{
    return nil;
}

- (NSNumber *)priceValue
{
    return _price;
}

- (NSNumber *)priceIdentifier
{
    return self.ID;
}

@end
