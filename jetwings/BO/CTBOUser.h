//
//  CTBOUser.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/12/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTBOUser : NSObject <NSCoding>

@property (nonatomic, copy) NSNumber* userID;

@property (nonatomic, copy) NSNumber* userRole;
@property (nonatomic, copy) NSString* userEmail;
@property (nonatomic, copy) NSString* userLang;
@property (nonatomic, copy) NSString* userCurrency;


@property (nonatomic, assign) BOOL firstLogin;
@property (nonatomic, assign) BOOL isEdit;

@property (nonatomic, copy) NSString* userFirstName;
@property (nonatomic, copy) NSString* userLastName;
@property (nonatomic, copy) NSString* userPhone;
@property (nonatomic, copy) NSString* userAvatar;

@property (nonatomic, copy) NSString* userToken;

- (void)setupWithDictionary:(NSDictionary *)aData;
- (NSString *)fullName;
- (NSString *)currencySymbol;

@end
