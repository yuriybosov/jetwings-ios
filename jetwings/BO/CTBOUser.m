//
//  CTBOUser.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/12/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOUser.h"

@implementation CTBOUser

- (void)setupWithDictionary:(NSDictionary *)aData
{
    _userID = [aData nullProtectedObjectForKey:@"id"];
    
    _userAvatar = [aData nullProtectedObjectForKey:@"avatar"];
    _userCurrency = [aData nullProtectedObjectForKey:@"currency"];
    _userEmail = [aData nullProtectedObjectForKey:@"email"];
    
    _firstLogin = [[aData nullProtectedObjectForKey:@"firstLogin"] boolValue];
    _isEdit = [[aData nullProtectedObjectForKey:@"isEdit"] boolValue];
    
    _userFirstName = [aData nullProtectedObjectForKey:@"first_name"];
    _userLastName = [aData nullProtectedObjectForKey:@"last_name"];
    
    _userLang = [aData nullProtectedObjectForKey:@"lang"];
    _userPhone = [aData nullProtectedObjectForKey:@"phone"];
    _userRole = [aData nullProtectedObjectForKey:@"role"];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    {
        _userID = [aDecoder decodeObjectForKey:@"id"];
        _userToken = [aDecoder decodeObjectForKey:@"userToken"];
        
        _userAvatar = [aDecoder decodeObjectForKey:@"avatar"];
        _userCurrency = [aDecoder decodeObjectForKey:@"currency"];
        _userEmail = [aDecoder decodeObjectForKey:@"email"];
        
        _firstLogin = [aDecoder decodeBoolForKey:@"firstLogin"];
        _isEdit = [aDecoder decodeBoolForKey:@"isEdit"];
        
        _userFirstName = [aDecoder decodeObjectForKey:@"first_name"];
        _userLastName = [aDecoder decodeObjectForKey:@"last_name"];
        
        _userLang = [aDecoder decodeObjectForKey:@"lang"];
        _userPhone = [aDecoder decodeObjectForKey:@"phone"];
        _userRole = [aDecoder decodeObjectForKey:@"role"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_userID forKey:@"id"];
    [aCoder encodeObject:_userToken forKey:@"userToken"];
    
    [aCoder encodeObject:_userAvatar forKey:@"avatar"];
    [aCoder encodeObject:_userCurrency forKey:@"currency"];
    [aCoder encodeObject:_userEmail forKey:@"email"];
    
    [aCoder encodeBool:_firstLogin forKey:@"firstLogin"];
    [aCoder encodeBool:_isEdit forKey:@"isEdit"];
    
    [aCoder encodeObject:_userFirstName forKey:@"first_name"];
    [aCoder encodeObject:_userLastName forKey:@"last_name"];
    
    [aCoder encodeObject:_userLang forKey:@"lang"];
    [aCoder encodeObject:_userPhone forKey:@"phone"];
    [aCoder encodeObject:_userRole forKey:@"role"];
}

- (NSString *)fullName
{
    NSMutableArray *array = [NSMutableArray array];
    if (self.userFirstName)
        [array addObject:self.userFirstName];
    if (self.userLastName)
        [array addObject:self.userLastName];
    
    return [array componentsJoinedByString:@" "];
}

- (NSString *)currencySymbol //¥
{
    if ([self.userCurrency isEqualToString:@"RMB"])
        return @"¥";
    else
        return @"$";
}

@end
