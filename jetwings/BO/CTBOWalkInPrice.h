//
//  CTBOWalkInPrice.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/18/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTPriceCellProtocol.h"

@interface CTBOWalkInPrice : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSNumber *price;

+ (CTBOWalkInPrice*)walkInPriceByPriceObject:(id<CTPriceCellProtocol>)priceObject;

@end
