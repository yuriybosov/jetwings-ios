//
//  CTBOWalkInPrice.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/18/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOWalkInPrice.h"

@implementation CTBOWalkInPrice

+ (CTBOWalkInPrice*)walkInPriceByPriceObject:(id<CTPriceCellProtocol>)priceObject
{
    CTBOWalkInPrice* price = [CTBOWalkInPrice new];

    price.title = [priceObject priceTitle];
    price.price = [priceObject priceValue];
    
    return price;
}

@end
