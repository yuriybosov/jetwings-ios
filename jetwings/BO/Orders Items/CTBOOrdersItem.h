//
//  CTBOOrdersItem.h
//  jetwings
//
//  Created by Yuriy Bosov on 2/4/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOBaseObject.h"
#import "CTBOOrdersItemBooking.h"

/* type values:
1 - hotel
2 - atraction
3 - restran
4 - guide
5 - tour pack
6 - transport
*/


typedef enum OrdetType
{
    OrdetTypeHotel = 1,
    OrdetTypeAtraction,
    OrdetTypeRestran,
    OrdetTypeGuide,
    OrdetTypeTourPack,
    OrdetTypeTransport
}OrdetType;

@interface CTBOOrdersItem : CTBOBaseObject

@property (nonatomic, readonly) NSNumber *type;
@property (nonatomic, readonly) NSNumber *total;
@property (nonatomic, readonly) NSNumber *stars;
@property (nonatomic, readonly) NSNumber *category;

@property (nonatomic, readonly) NSString *picture;
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *work;
@property (nonatomic, strong) NSString *currencySymbol;

@property (nonatomic, readonly) NSString *startDate;
@property (nonatomic, readonly) NSString *endDate;

@property (nonatomic, readonly) NSArray *booking;

- (NSString *)nameText;
- (NSString *)descriptionText;
- (UIImage *)logo;

@end
