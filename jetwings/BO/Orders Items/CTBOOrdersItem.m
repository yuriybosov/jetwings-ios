//
//  CTBOOrdersItem.m
//  jetwings
//
//  Created by Yuriy Bosov on 2/4/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOOrdersItem.h"

@implementation CTBOOrdersItem

- (void)setupWithData:(NSDictionary *)aData
{
    [super setupWithData:aData];
    
    _type = [aData nullProtectedObjectForKey:@"type"];
    _total = [aData nullProtectedObjectForKey:@"total"];
    _stars = [aData nullProtectedObjectForKey:@"stars"];
    _category = [aData nullProtectedObjectForKey:@"category"];
    
    _picture = [aData nullProtectedObjectForKey:@"picture"];
    _name = [aData nullProtectedObjectForKey:@"name"];
    _work = [aData nullProtectedObjectForKey:@"work"];
    
    _startDate = [aData nullProtectedObjectForKey:@"start_date"];
    _endDate = [aData nullProtectedObjectForKey:@"end_date"];
    
    NSArray *bookings = [aData nullProtectedObjectForKey:@"booking"];
    if ([bookings isKindOfClass:[NSArray class]] && [bookings count])
    {
        NSMutableArray *temp = [NSMutableArray array];
        for (NSDictionary *data in bookings)
        {
            CTBOOrdersItemBooking *obj = [[CTBOOrdersItemBooking alloc] init];
            [obj setupWithData:data];
            [temp addObject:obj];
        }
        _booking = [NSArray arrayWithArray:temp];
    }
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    {
        _type = [aDecoder decodeObjectForKey:@"type"];
        _total = [aDecoder decodeObjectForKey:@"total"];
        _stars = [aDecoder decodeObjectForKey:@"stars"];
        _category = [aDecoder decodeObjectForKey:@"category"];
        
        _picture = [aDecoder decodeObjectForKey:@"picture"];
        _name = [aDecoder decodeObjectForKey:@"name"];
        _work = [aDecoder decodeObjectForKey:@"work"];
        _currencySymbol = [aDecoder decodeObjectForKey:@"currencySymbol"];
        _booking = [aDecoder decodeObjectForKey:@"booking"];
        
        _startDate = [aDecoder decodeObjectForKey:@"startDate"];
        _endDate = [aDecoder decodeObjectForKey:@"endDate"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:_type forKey:@"type"];
    [aCoder encodeObject:_total forKey:@"total"];
    [aCoder encodeObject:_stars forKey:@"stars"];
    [aCoder encodeObject:_category forKey:@"category"];
    
    [aCoder encodeObject:_picture forKey:@"picture"];
    [aCoder encodeObject:_name forKey:@"name"];
    [aCoder encodeObject:_work forKey:@"work"];
    [aCoder encodeObject:_currencySymbol forKey:@"currencySymbol"];
    [aCoder encodeObject:_booking forKey:@"booking"];
    
    [aCoder encodeObject:_startDate forKey:@"startDate"];
    [aCoder encodeObject:_endDate forKey:@"endDate"];
}

#pragma mark - 

- (NSString *)descriptionText
{
    NSString *text = nil;
    
    switch ([self.type integerValue])
    {
        case 1:
//            text = @"ic_hotel";
        {
            NSUInteger count = 0;
            for (CTBOOrdersItemBooking *item in self.booking)
            {
                count += [item.quantity integerValue];
            }
            text = [NSString stringWithFormat:@"%lu %@ %@%.2f",
                    count,
                    count > 1 ? CTLocalizedString(@"rooms") : CTLocalizedString(@"room"),
                    self.currencySymbol,
                    [self.total floatValue]];
        }
            break;
        case 2:
        case 5:
//            text = @"ic_attraction";
//            text = @"ic_package";
        {
            NSUInteger count = 0;
            for (CTBOOrdersItemBooking *item in self.booking)
            {
                count += [item.quantity integerValue];
            }
            text = [NSString stringWithFormat:@"%lu %@ %@%.2f",
                    count,
                    count > 1 ? CTLocalizedString(@"tickets") : CTLocalizedString(@"ticket"),
                    self.currencySymbol,
                    [self.total floatValue]];
        }
            break;
        case 3:
//            text = @"ic_rest";
        {
            NSUInteger count = 0;
            for (CTBOOrdersItemBooking *item in self.booking)
            {
                count += [item.quantity integerValue];
            }
            text = [NSString stringWithFormat:@"%lu %@ %@%.2f",
                    count,
                    count > 1 ? CTLocalizedString(@"set meals") : CTLocalizedString(@"set meal"),
                    self.currencySymbol,
                    [self.total floatValue]];
        }
            break;
        case 4:
        case 6:
//            text = @"ic_guide";
//            text = @"ic_transport";
        {
            NSUInteger count = 0;
            for (CTBOOrdersItemBooking *item in self.booking)
            {
                count += [item.quantity integerValue];
            }
            text = [NSString stringWithFormat:@"%lu %@ %@%.2f",
                    count,
                    count > 1 ? CTLocalizedString(@"services") : CTLocalizedString(@"service"),
                    self.currencySymbol,
                    [self.total floatValue]];
        }
            break;
            
        default:
        {
            NSUInteger count = 0;
            for (CTBOOrdersItemBooking *item in self.booking)
            {
                count += [item.quantity integerValue];
            }
            text = [NSString stringWithFormat:@"%lu %@ %@%.2f",
                    count,
                    count > 1 ? CTLocalizedString(@"services") : CTLocalizedString(@"service"),
                    self.currencySymbol,
                    [self.total floatValue]];
        }
            break;
    }
    
    return text;
}

- (NSString *)nameText
{
    if ([self.type integerValue] == 6)
    {
        return CTLocalizedString(@"Transport Services");
    }
    else if ([self.type integerValue] == 4)
    {
        return CTLocalizedString(@"Tour Guides Services");
    }
    return self.name;
}

- (UIImage *)logo
{
    NSString *imageName = nil;
    
    switch ([self.type integerValue])
    {
        case 1:
            imageName = @"ic_hotel";
            break;
        case 2:
            imageName = @"ic_attraction";
            break;
        case 3:
            imageName = @"ic_rest";
            break;
        case 4:
            imageName = @"ic_guide";
            break;
        case 5:
            imageName = @"ic_package";
            break;
            
        default:
            imageName = @"ic_transport";
            break;
    }
    
    return [UIImage imageNamed:imageName];
}

@end
