//
//  CTBOOrdersItemBooking.h
//  jetwings
//
//  Created by Yuriy Bosov on 2/4/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOBaseObject.h"

@interface CTBOOrdersItemBooking : CTBOBaseObject

@property (nonatomic, readonly) NSNumber *area;
@property (nonatomic, readonly) NSNumber *quantity;

@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *descriptionText;

@end
