//
//  CTBOOrdersItemBooking.m
//  jetwings
//
//  Created by Yuriy Bosov on 2/4/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBOOrdersItemBooking.h"

@implementation CTBOOrdersItemBooking

- (void)setupWithData:(NSDictionary *)aData
{
    [super setupWithData:aData];
    
    _quantity = [aData nullProtectedObjectForKey:@"quantity"];
    _area = [aData nullProtectedObjectForKey:@"area"];
    
    _name = [aData nullProtectedObjectForKey:@"name"];
    _descriptionText = [aData nullProtectedObjectForKey:@"description"];
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    {
        _quantity = [aDecoder decodeObjectForKey:@"quantity"];
        _area = [aDecoder decodeObjectForKey:@"area"];
        _name = [aDecoder decodeObjectForKey:@"name"];
        _descriptionText = [aDecoder decodeObjectForKey:@"description"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:_quantity forKey:@"quantity"];
    [aCoder encodeObject:_area forKey:@"area"];
    [aCoder encodeObject:_name forKey:@"name"];
    [aCoder encodeObject:_descriptionText forKey:@"description"];
}

@end
