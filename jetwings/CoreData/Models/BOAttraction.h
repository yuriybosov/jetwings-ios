#import "_BOAttraction.h"
#import "CTBOPhotoInfo.h"
#import "CTBOTicket.h"
#import "CTBONearbyHotel.h"
#import "CTBOWalkInPrice.h"

@interface BOAttraction : _BOAttraction {}

+ (CTDataBaseRequest *)attractionsList;

- (void)setupDetailedDictionary:(NSDictionary *)aData;

- (NSAttributedString *)nearbyHotelsText;
- (NSAttributedString *)perfomanceText;

- (NSArray *)walkInPricings;

@end
