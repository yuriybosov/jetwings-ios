#import "BOAttraction.h"


@implementation BOAttraction

+ (CTDataBaseRequest *)attractionsList
{
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[self entityName]];
    
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:BOAttractionAttributes.name
                                                         ascending:YES];
    fetchRequest.sortDescriptors = @[sd];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"self.areaIdentifier == %@", CTAppManagerInstance.currentArea.ID];
    
    CTDataBaseRequest* request = [[CTDataBaseRequest alloc] initWithStorage:[CTMainStorage sharedInstance]];
    request.fetchRequest = fetchRequest;
    
    return request;
}

- (void)setupWithDictionary:(NSDictionary *)aData
{
    [super setupWithDictionary:aData];
    
    self.name = [aData nullProtectedObjectForKey:@"name"];
    self.work = [aData nullProtectedObjectForKey:@"work"];
    self.picture = [aData nullProtectedObjectForKey:@"picture"];
    
    if (self.picture)
    {
        CTBOPhotoInfo *info = [CTBOPhotoInfo new];
        info.photoPath = self.picture;
        
        self.photos = @[info];
    }
}

- (void)setupDetailedDictionary:(NSDictionary *)aData
{
    self.address = [aData nullProtectedObjectForKey:@"address"];
    self.descriptionText = [aData nullProtectedObjectForKey:@"description"];
    
    self.links = [aData nullProtectedObjectForKey:@"links"];
    
    self.latitude = [aData nullProtectedObjectForKey:@"latitude"];
    self.longitude = [aData nullProtectedObjectForKey:@"longitude"];
    
    self.perfomance = [aData nullProtectedObjectForKey:@"perfomance"];
    self.work = [aData nullProtectedObjectForKey:@"work"];
    
    //
    self.photos = [self models:[CTBOPhotoInfo class] withRawData:[aData nullProtectedObjectForKey:@"attractionsPictures"]];
    self.tickets = [self models:[CTBOTicket class] withRawData:[aData nullProtectedObjectForKey:@"attractionsTicket"]];
    self.nearbyHotels = [self models:[CTBONearbyHotel class] withRawData:[aData nullProtectedObjectForKey:@"nearbyHotels"]];
}

- (NSAttributedString *)nearbyHotelsText
{
    NSMutableAttributedString *resultString = nil;
    if ([self.nearbyHotels count])
    {
        NSString *temp = @"";
        resultString = [[NSMutableAttributedString alloc] initWithString:temp
                                                              attributes:@{NSForegroundColorAttributeName : [UIColor colorWithRedI:83 greenI:87 blueI:100 alphaI:55],                                                                                                      NSFontAttributeName : [UIFont systemFontOfSize:13]}];

        
        NSString *title = CTLocalizedString(@"NEARBY ");
        NSString *attractionsTitles = [[self.nearbyHotels valueForKey:@"name"] componentsJoinedByString:@"\n"];
        NSString *nearbyRestaurantsString = [NSString stringWithFormat:@"%@\n%@",title,attractionsTitles];
        
        NSMutableAttributedString *nearbyAttractionsAttrString = [[NSMutableAttributedString alloc]initWithString:nearbyRestaurantsString];
        [nearbyAttractionsAttrString addAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRedI:177 greenI:181 blueI:195 alphaI:255]}
                                             range:[nearbyRestaurantsString rangeOfString:title]];
        [resultString appendAttributedString:nearbyAttractionsAttrString];
    }
    return resultString;
}

- (NSAttributedString *)perfomanceText
{
    NSMutableAttributedString *resultString = nil;
    if (self.perfomance.length)
    {
        NSString *temp = @"";
        resultString = [[NSMutableAttributedString alloc] initWithString:temp
                                                              attributes:@{NSForegroundColorAttributeName : [UIColor colorWithRedI:83 greenI:87 blueI:100 alphaI:55],                                                                                                      NSFontAttributeName : [UIFont systemFontOfSize:13]}];
        
        
        NSString *title = CTLocalizedString(@"PERFORMANCE SCHEDULE");
        NSString *perfomanceString = [NSString stringWithFormat:@"%@\n%@",title,self.perfomance];

        NSMutableAttributedString *perfomanceAttrString = [[NSMutableAttributedString alloc]initWithString:perfomanceString];
        [perfomanceAttrString addAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRedI:177 greenI:181 blueI:195 alphaI:255]}
                                             range:[perfomanceString rangeOfString:title]];
        [resultString appendAttributedString:perfomanceAttrString];
    }
    return resultString;
}

- (NSArray *)walkInPricings
{
    NSMutableArray *result = nil;
    if ([self.tickets count])
    {
        result = [NSMutableArray array];
        for (CTBOTicket *ticket in self.tickets)
        {
            CTBOWalkInPrice *walkInPrice = [CTBOWalkInPrice walkInPriceByPriceObject:ticket];
            walkInPrice.price = ticket.price;
            [result addObject:walkInPrice];
        }
    }
    return result;
}

@end
