#import "_BOGuide.h"

@interface BOGuide : _BOGuide {}

+ (CTDataBaseRequest *)guidesList;

- (void)setupDetailedDictionary:(NSDictionary *)aData;

- (NSString *)guideInfo;

@end
