#import "BOGuide.h"


@implementation BOGuide

+ (CTDataBaseRequest *)guidesList
{
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[self entityName]];
    
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:BOGuideAttributes.name
                                                         ascending:YES];
    fetchRequest.sortDescriptors = @[sd];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"self.areaIdentifier == %@", CTAppManagerInstance.currentArea.ID];
    
    CTDataBaseRequest* request = [[CTDataBaseRequest alloc] initWithStorage:[CTMainStorage sharedInstance]];
    request.fetchRequest = fetchRequest;
    
    return request;
}

- (void)setupWithDictionary:(NSDictionary *)aData
{
    [super setupWithDictionary:aData];
    
    self.name = [aData nullProtectedObjectForKey:@"username"];
    self.avatar = [aData nullProtectedObjectForKey:@"avatar"];
    self.speciality = [aData nullProtectedObjectForKey:@"speciality"];
}

- (void)setupDetailedDictionary:(NSDictionary *)aData
{
    self.gender = [aData nullProtectedObjectForKey:@"gender"];
    self.experience = [aData nullProtectedObjectForKey:@"experience"];
    self.age = [aData nullProtectedObjectForKey:@"age"];
    self.national = [aData nullProtectedObjectForKey:@"national"];
    self.license = [aData nullProtectedObjectForKey:@"license"];
    self.languages = [aData nullProtectedObjectForKey:@"languages"];
    self.speciality = [aData nullProtectedObjectForKey:@"speciality"];
    
    static NSDateFormatter *dateFormatterPricing = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      dateFormatterPricing = [NSDateFormatter new];
                      dateFormatterPricing.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
                      dateFormatterPricing.dateFormat = @"yyyy-MM-dd";
                  });
    
    id value = [aData nullProtectedObjectForKey:@"birthday"];
    if (value && [value isKindOfClass:[NSString class]])
    {
        self.birthday = [dateFormatterPricing dateFromString:value];
    }
}

- (NSString *)guideInfo
{
    NSString *result = nil;
    
    NSMutableArray *temp = [NSMutableArray array];
    
    if (self.national)
        [temp addObject:self.national];
    if (self.ageValue >0)
        [temp addObject:[NSString stringWithFormat:@"%@ %@", self.age, CTLocalizedString(@"years")]];
    if (self.gender)
        [temp addObject:[self.gender lowercaseString]];
    
    if (temp.count)
        result = [temp componentsJoinedByString:@", "];
    
    return result;
}

@end
