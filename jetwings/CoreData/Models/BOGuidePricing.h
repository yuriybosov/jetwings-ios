#import "_BOGuidePricing.h"
#import "CTPriceCellProtocol.h"

@interface BOGuidePricing : _BOGuidePricing <CTPriceCellProtocol> {}

+ (CTDataBaseRequest *)guidesPricingList;

@end
