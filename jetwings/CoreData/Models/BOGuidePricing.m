#import "BOGuidePricing.h"


@implementation BOGuidePricing

+ (CTDataBaseRequest *)guidesPricingList
{
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[self entityName]];
    
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:BOGuidePricingAttributes.name
                                                         ascending:YES];
    fetchRequest.sortDescriptors = @[sd];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"self.areaIdentifier == %@", CTAppManagerInstance.currentArea.ID];
    
    CTDataBaseRequest* request = [[CTDataBaseRequest alloc] initWithStorage:[CTMainStorage sharedInstance]];
    request.fetchRequest = fetchRequest;
    
    return request;
}

- (void)setupWithDictionary:(NSDictionary *)aData
{
    [super setupWithDictionary:aData];
    
    self.name = [aData nullProtectedObjectForKey:@"name"];
    self.areaIdentifier = [aData nullProtectedObjectForKey:@"area"];
    self.descriptionText = [aData nullProtectedObjectForKey:@"description"];
    self.price = [aData nullProtectedObjectForKey:@"price"];
}

#pragma mark - CTPriceCellProtocol

- (NSString *)priceTitle
{
    return self.name;
}

- (NSString *)priceDescriprion
{
    return nil;
}

- (NSNumber *)priceValue
{
    return self.price;
}

- (NSNumber *)priceIdentifier
{
    return self.identifier;
}

@end
