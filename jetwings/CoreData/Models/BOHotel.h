#import "_BOHotel.h"
#import "CTBOHotelFilter.h"
#import "CTBOPhotoInfo.h"
#import "CTBONearbyAttraction.h"
#import "CTBONearbyRestaurant.h"
#import "CTBOAmenitiesType.h"
#import "CTBOGuestType.h"
#import "CTBOHotelsRoom.h"

@interface BOHotel : _BOHotel {}

+ (CTDataBaseRequest *)hotelsListByFilter:(CTBOHotelFilter *)filter;

- (void)setupDetailedDictionary:(NSDictionary *)aData;

- (NSAttributedString *)descriptionText;

@end
