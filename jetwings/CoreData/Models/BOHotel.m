#import "BOHotel.h"

@implementation BOHotel

+ (CTDataBaseRequest *)hotelsListByFilter:(CTBOHotelFilter *)filter
{
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[self entityName]];
    
    NSSortDescriptor *sd1 = [NSSortDescriptor sortDescriptorWithKey:filter.isSortedByStar ? BOHotelAttributes.stars : BOHotelAttributes.price
                                                         ascending:filter.isAsc];
    NSSortDescriptor *sd2 = [NSSortDescriptor sortDescriptorWithKey:BOHotelAttributes.name
                                                          ascending:NO];
    fetchRequest.sortDescriptors = @[sd1,sd2];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"self.areaIdentifier == %@", CTAppManagerInstance.currentArea.ID];
    
    CTDataBaseRequest* request = [[CTDataBaseRequest alloc] initWithStorage:[CTMainStorage sharedInstance]];
    request.fetchRequest = fetchRequest;
    
    return request;
}

- (void)setupWithDictionary:(NSDictionary *)aData
{
    [super setupWithDictionary:aData];
    
    self.name = [aData nullProtectedObjectForKey:@"name"];
    self.stars = [aData nullProtectedObjectForKey:@"stars"];
    self.price = [aData nullProtectedObjectForKey:@"price"];
    self.picture = [aData nullProtectedObjectForKey:@"picture"];
    
    if (self.picture)
    {
        CTBOPhotoInfo *info = [CTBOPhotoInfo new];
        info.photoPath = self.picture;
        
        self.photos = @[info];
    }
}

- (void)setupDetailedDictionary:(NSDictionary *)aData
{
    self.stars = [aData nullProtectedObjectForKey:@"stars"];
    self.address = [aData nullProtectedObjectForKey:@"address"];
    
    self.rooms = [aData nullProtectedObjectForKey:@"rooms"];
    
    self.areaIdentifier = [aData nullProtectedObjectForKeyPath:@"areaName.id"];
    self.areaName = [aData nullProtectedObjectForKey:@"areaName.name"];
    
    self.latitude = [aData nullProtectedObjectForKey:@"latitude"];
    self.longitude = [aData nullProtectedObjectForKey:@"longitude"];
    
    self.district = [aData nullProtectedObjectForKey:@"district"];
    self.details = [aData nullProtectedObjectForKey:@"details"];
    
    //
    self.photos = [self models:[CTBOPhotoInfo class] withRawData:[aData nullProtectedObjectForKey:@"hotelsPictures"]];
    self.amenitiesType = [self models:[CTBOAmenitiesType class] withRawData:[aData nullProtectedObjectForKey:@"amenitiesType"]];
    self.guestType = [self models:[CTBOGuestType class] withRawData:[aData nullProtectedObjectForKey:@"guestType"]];
    self.nearbyAttractions = [self models:[CTBONearbyAttraction class] withRawData:[aData nullProtectedObjectForKey:@"nearbyAttractions"]];
    self.nearbyRestaurants = [self models:[CTBONearbyRestaurant class] withRawData:[aData nullProtectedObjectForKey:@"nearbyRestaurants"]];
    
    NSArray *tempHotelRooms = [self models:[CTBOHotelsRoom class] withRawData:[aData nullProtectedObjectForKey:@"hotelsRoom"]];
    NSMutableArray *tempValidHotelRooms = [NSMutableArray new];
    for (CTBOHotelsRoom *room in tempHotelRooms)
    {
        for (CTBOPricing *price in room.prising)
        {
            if ([price.priceValue integerValue] > 0)
            {
                [tempValidHotelRooms addObject:room];
                break;
            }
        }
    }
    self.hotelsRoom = [NSArray arrayWithArray:tempValidHotelRooms];
    
    self.initialDetailedInfoValue = [self.amenitiesType count] || self.district || self.address || [self.rooms integerValue] > 0;
}

- (NSAttributedString *)descriptionText
{
    
    NSString *tmp = self.details ?: @"";
    NSMutableAttributedString *resultString = [[NSMutableAttributedString alloc] initWithString:tmp
                                                                               attributes:@{NSForegroundColorAttributeName : [UIColor colorWithRedI:83 greenI:87 blueI:100 alphaI:255],
                                                                                            NSFontAttributeName : [UIFont systemFontOfSize:13]}];
    
    
    if ([self.nearbyRestaurants count])
    {
        NSString *title = [CTLocalizedString(@"Nearby restaurant") capitalizedString];
        NSString *attractionsTitles = [[self.nearbyRestaurants valueForKey:@"name"] componentsJoinedByString:@"\n"];
        NSString *nearbyRestaurantsString = [NSString stringWithFormat:@"\n\n%@\n%@",title,attractionsTitles];
        
        NSMutableAttributedString *nearbyAttractionsAttrString = [[NSMutableAttributedString alloc]initWithString:nearbyRestaurantsString];
        [nearbyAttractionsAttrString addAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRedI:177 greenI:181 blueI:195 alphaI:255]}
                                             range:[nearbyRestaurantsString rangeOfString:title]];
        [resultString appendAttributedString:nearbyAttractionsAttrString];
    }
    
    if ([self.nearbyAttractions count])
    {
        NSString *title = [CTLocalizedString(@"Nearby Attractions") capitalizedString];
        NSString *attractionsTitles = [[self.nearbyAttractions valueForKey:@"name"] componentsJoinedByString:@"\n"];
        NSString *nearbyAttractionsString = [NSString stringWithFormat:@"\n\n%@\n%@",title,attractionsTitles];
        
        NSMutableAttributedString *nearbyAttractionsAttrString = [[NSMutableAttributedString alloc]initWithString:nearbyAttractionsString];
        [nearbyAttractionsAttrString addAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRedI:177 greenI:181 blueI:195 alphaI:255]}
                                             range:[nearbyAttractionsString rangeOfString:title]];
        [resultString appendAttributedString:nearbyAttractionsAttrString];
    }
    
    return resultString;
}

@end
