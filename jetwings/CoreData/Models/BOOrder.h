#import "_BOOrder.h"
#import "CTBOOrdersItem.h"

@class BOOrder;

typedef void(^OrderComplitionBlock)(NSString *errorMessage, BOOrder *order);

@interface BOOrder : _BOOrder {}

+ (CTDataBaseRequest *)ordersList;
+ (void)currentOrder:(OrderComplitionBlock)complitionBlock;

- (void)setupDetailedDictionary:(NSDictionary *)aData;
- (BOOL)itHasBookingItemByType:(NSInteger)type;

- (CTBOOrdersItem *)bookingHotel;
- (NSString *)currencySymbol;

@end
