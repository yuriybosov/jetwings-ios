#import "BOOrder.h"
#import "CTHotelsGateway.h"


@implementation BOOrder

+ (CTDataBaseRequest *)ordersList
{
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[self entityName]];
    
    NSSortDescriptor *sd1 = [NSSortDescriptor sortDescriptorWithKey:BOOrderAttributes.identifier
                                                          ascending:YES];
    fetchRequest.sortDescriptors = @[sd1];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"self.areaIdentifier == %@", CTAppManagerInstance.currentArea.ID];
    
    CTDataBaseRequest* request = [[CTDataBaseRequest alloc] initWithStorage:[CTMainStorage sharedInstance]];
    request.fetchRequest = fetchRequest;
    
    return request;
}

+ (void)currentOrder:(OrderComplitionBlock)complitionBlock
{
    CTRequest *requet = [[CTHotelsGateway sharedInstance] orderCurrent];
    
    [requet addResponseBlock:^(CTResponse *aResponse)
    {
        if (complitionBlock)
        {
            complitionBlock(aResponse.textMessage, [aResponse.boArray firstObject]);
        }
    } responseQueue:dispatch_get_main_queue()];
    
    [requet start];
}

- (void)setupWithDictionary:(NSDictionary *)aData
{
    [super setupWithDictionary:aData];
    
    self.title = [aData nullProtectedObjectForKey:@"title"];
    self.descriptionText = [aData nullProtectedObjectForKey:@"description"];
    self.total = [aData nullProtectedObjectForKey:@"total"];
}

- (void)setupDetailedDictionary:(NSDictionary *)aData
{
    self.title = [aData nullProtectedObjectForKey:@"title"];
    self.descriptionText = [aData nullProtectedObjectForKey:@"description"];
    self.total = [aData nullProtectedObjectForKey:@"total"];
    self.areaIdentifier = [aData nullProtectedObjectForKey:@"area"];
    
    self.currensy = [aData nullProtectedObjectForKey:@"currency"];
    
    self.ordersItems = [self models:[CTBOOrdersItem class] withRawData:[aData nullProtectedObjectForKey:@"ordersItems"]];
    for (CTBOOrdersItem *item in self.ordersItems)
    {
        item.currencySymbol = [self currencySymbol];
    }
}

- (NSString *)currencySymbol //¥
{
    if ([self.currensy isEqualToString:@"RMB"])
        return @"¥";
    else
        return @"$";
}

- (BOOL)itHasBookingItemByType:(NSInteger)type
{
    BOOL result = NO;
    for (CTBOOrdersItem *item in self.ordersItems)
    {
        if ([item.type integerValue] == type)
        {
            result = YES;
            break;
        }
    }
    return result;
}

- (CTBOOrdersItem *)bookingHotel
{
    CTBOOrdersItem *result = nil;
    for (CTBOOrdersItem *item in self.ordersItems)
    {
        if ([item.type integerValue] == OrdetTypeHotel)
        {
            result = item;
            break;
        }
    }
    return result;
}

@end
