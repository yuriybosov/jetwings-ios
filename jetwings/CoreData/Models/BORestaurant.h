#import "_BORestaurant.h"
#import "CTBOMealsSet.h"
#import "CTBOPhotoInfo.h"
#import "CTBONearbyHotel.h"


@interface BORestaurant : _BORestaurant {}

+ (CTDataBaseRequest *)restaurantsList;

- (void)setupDetailedDictionary:(NSDictionary *)aData;

- (NSAttributedString *)nearbyHotelsText;

@end
