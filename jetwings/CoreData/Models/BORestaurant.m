#import "BORestaurant.h"


@implementation BORestaurant

+ (CTDataBaseRequest *)restaurantsList
{
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[self entityName]];
    
    NSSortDescriptor *sd1 = [NSSortDescriptor sortDescriptorWithKey:BORestaurantAttributes.category
                                                         ascending:NO];
    NSSortDescriptor *sd2 = [NSSortDescriptor sortDescriptorWithKey:BORestaurantAttributes.name
                                                          ascending:YES];
    fetchRequest.sortDescriptors = @[sd1,sd2];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"self.areaIdentifier == %@", CTAppManagerInstance.currentArea.ID];
    
    CTDataBaseRequest* request = [[CTDataBaseRequest alloc] initWithStorage:[CTMainStorage sharedInstance]];
    request.fetchRequest = fetchRequest;
    
    return request;
}

- (void)setupWithDictionary:(NSDictionary *)aData
{
    [super setupWithDictionary:aData];
    
    self.name = [aData nullProtectedObjectForKey:@"name"];
    self.work = [aData nullProtectedObjectForKey:@"work"];
    self.category = [aData nullProtectedObjectForKey:@"category"];
    self.picture = [aData nullProtectedObjectForKey:@"picture"];
    
    if (self.picture)
    {
        CTBOPhotoInfo *info = [CTBOPhotoInfo new];
        info.photoPath = self.picture;
        
        self.photos = @[info];
    }
}

- (void)setupDetailedDictionary:(NSDictionary *)aData
{
    self.address = [aData nullProtectedObjectForKey:@"address"];
    self.descriptionText = [aData nullProtectedObjectForKey:@"description"];
    
    self.latitude = [aData nullProtectedObjectForKey:@"latitude"];
    self.longitude = [aData nullProtectedObjectForKey:@"longitude"];
    
    self.photos = [self models:[CTBOPhotoInfo class] withRawData:[aData nullProtectedObjectForKey:@"restaurantsPictures"]];
    self.mealsSet = [self models:[CTBOMealsSet class] withRawData:[aData nullProtectedObjectForKey:@"mealsSet"]];
    self.nearbyHotels = [self models:[CTBONearbyHotel class] withRawData:[aData nullProtectedObjectForKey:@"nearbyHotels"]];
}

- (NSAttributedString *)nearbyHotelsText
{
    NSMutableAttributedString *resultString = nil;
    if ([self.nearbyHotels count])
    {
        NSString *temp = @"";
        resultString = [[NSMutableAttributedString alloc] initWithString:temp
                                                              attributes:@{NSForegroundColorAttributeName : [UIColor colorWithRedI:83 greenI:87 blueI:100 alphaI:55],                                                                                                      NSFontAttributeName : [UIFont systemFontOfSize:13]}];
        
        
        NSString *title = [CTLocalizedString(@"Nearby hotel") capitalizedString];
        NSString *attractionsTitles = [[self.nearbyHotels valueForKey:@"name"] componentsJoinedByString:@"\n"];
        NSString *nearbyRestaurantsString = [NSString stringWithFormat:@"%@\n%@",title,attractionsTitles];
        
        NSMutableAttributedString *nearbyAttractionsAttrString = [[NSMutableAttributedString alloc]initWithString:nearbyRestaurantsString];
        [nearbyAttractionsAttrString addAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRedI:177 greenI:181 blueI:195 alphaI:255]}
                                             range:[nearbyRestaurantsString rangeOfString:title]];
        [resultString appendAttributedString:nearbyAttractionsAttrString];
    }
    return resultString;
}
@end
