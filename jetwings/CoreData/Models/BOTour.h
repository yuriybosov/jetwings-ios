#import "_BOTour.h"
#import "CTBOTourTicket.h"
#import "CTBOPhotoInfo.h"

@interface BOTour : _BOTour {}

+ (CTDataBaseRequest *)toursList;

- (void)setupDetailedDictionary:(NSDictionary *)aData;

@end
