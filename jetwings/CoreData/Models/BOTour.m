#import "BOTour.h"


@implementation BOTour

+ (CTDataBaseRequest *)toursList
{
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[self entityName]];
    
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:BOTourAttributes.name
                                                         ascending:YES];
    fetchRequest.sortDescriptors = @[sd];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"self.areaIdentifier == %@", CTAppManagerInstance.currentArea.ID];
    
    CTDataBaseRequest* request = [[CTDataBaseRequest alloc] initWithStorage:[CTMainStorage sharedInstance]];
    request.fetchRequest = fetchRequest;
    
    return request;
}

- (void)setupWithDictionary:(NSDictionary *)aData
{
    [super setupWithDictionary:aData];
    
    self.name = [aData nullProtectedObjectForKey:@"name"];
    self.picture = [aData nullProtectedObjectForKey:@"picture"];
    
    static NSDateFormatter *dateFormatterPricing = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      dateFormatterPricing = [NSDateFormatter new];
                      dateFormatterPricing.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
                      dateFormatterPricing.dateFormat = @"yyyy-MM-dd";
                  });
    
    id value = [aData nullProtectedObjectForKey:@"start_date"];
    if (value && [value isKindOfClass:[NSString class]])
    {
        self.startDate = [dateFormatterPricing dateFromString:value];
    }
    
    value = [aData nullProtectedObjectForKey:@"end_date"];
    if (value && [value isKindOfClass:[NSString class]])
    {
        self.endDate = [dateFormatterPricing dateFromString:value];
    }
    
    if (self.picture)
    {
        CTBOPhotoInfo *info = [CTBOPhotoInfo new];
        info.photoPath = self.picture;
        
        self.tourPictures = @[info];
    }
}

- (void)setupDetailedDictionary:(NSDictionary *)aData
{
    self.descriptionText = [aData nullProtectedObjectForKey:@"description"];
    self.tourPictures = [self models:[CTBOPhotoInfo class] withRawData:[aData nullProtectedObjectForKey:@"tourPackagePictures"]];
    self.tourTicket = [self models:[CTBOTourTicket class] withRawData:[aData nullProtectedObjectForKey:@"tourPackageTicket"]];
}


@end
