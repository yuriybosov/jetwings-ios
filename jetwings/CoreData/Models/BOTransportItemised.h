#import "_BOTransportItemised.h"
#import "CTPriceCellProtocol.h"

@interface BOTransportItemised : _BOTransportItemised  <CTPriceCellProtocol> {}

+ (CTDataBaseRequest *)transportItemisedList;

@end
