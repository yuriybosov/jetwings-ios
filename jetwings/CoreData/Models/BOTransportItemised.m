#import "BOTransportItemised.h"

@implementation BOTransportItemised

+ (CTDataBaseRequest *)transportItemisedList
{
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[self entityName]];
    
    NSSortDescriptor *sd1 = [NSSortDescriptor sortDescriptorWithKey:BOTransportItemisedAttributes.priceBig
                                                          ascending:NO];
    NSSortDescriptor *sd2 = [NSSortDescriptor sortDescriptorWithKey:BOTransportItemisedAttributes.name
                                                          ascending:YES];
    fetchRequest.sortDescriptors = @[sd1,sd2];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"self.areaIdentifier == %@", CTAppManagerInstance.currentArea.ID];
    
    CTDataBaseRequest* request = [[CTDataBaseRequest alloc] initWithStorage:[CTMainStorage sharedInstance]];
    request.fetchRequest = fetchRequest;
    
    return request;
}

- (void)setupWithDictionary:(NSDictionary *)aData
{
    [super setupWithDictionary:aData];
    
    self.name = [aData nullProtectedObjectForKey:@"name"];
    self.descriptionText = [aData nullProtectedObjectForKey:@"description"];
    self.areaIdentifier = [aData nullProtectedObjectForKey:@"area"];
    self.priceSmall = [aData nullProtectedObjectForKey:@"price_small"];
    self.priceBig = [aData nullProtectedObjectForKey:@"price_big"];
}

#pragma mark - CTPriceCellProtocol

- (NSString *)priceTitle
{
    return self.name;
}

- (NSString *)priceDescriprion
{
    return self.descriptionText;
}

- (NSNumber *)priceValue
{
    return self.priceSmall;
}

- (NSNumber *)priceIdentifier
{
    return self.identifier;
}

@end
