#import "_BOTransportPackages.h"
#import "CTPriceCellProtocol.h"

@interface BOTransportPackages : _BOTransportPackages <CTPriceCellProtocol> {}

+ (CTDataBaseRequest *)transportPackagesList;

@end
