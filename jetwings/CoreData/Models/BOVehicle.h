#import "_BOVehicle.h"
#import "CTBOPhotoInfo.h"

@interface BOVehicle : _BOVehicle {}

+ (CTDataBaseRequest *)vehicleList;

- (void)setupDetailedDictionary:(NSDictionary *)aData;

@end
