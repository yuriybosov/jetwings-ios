#import "BOVehicle.h"

@implementation BOVehicle

+ (CTDataBaseRequest *)vehicleList
{
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[self entityName]];
    
    NSSortDescriptor *sd1 = [NSSortDescriptor sortDescriptorWithKey:BOVehicleAttributes.brand
                                                          ascending:YES];
    fetchRequest.sortDescriptors = @[sd1];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"self.areaIdentifier == %@", CTAppManagerInstance.currentArea.ID];
    
    CTDataBaseRequest* request = [[CTDataBaseRequest alloc] initWithStorage:[CTMainStorage sharedInstance]];
    request.fetchRequest = fetchRequest;
    
    return request;
}

- (void)setupWithDictionary:(NSDictionary *)aData
{
    [super setupWithDictionary:aData];
    
    self.areaIdentifier = [aData nullProtectedObjectForKey:@"area"];
    self.brand = [aData nullProtectedObjectForKey:@"brand"];
    self.model = [aData nullProtectedObjectForKey:@"model"];
}

- (void)setupDetailedDictionary:(NSDictionary *)aData
{
    self.year = [aData nullProtectedObjectForKey:@"year"];
    self.seats = [aData nullProtectedObjectForKey:@"seats"];
    self.type = [aData nullProtectedObjectForKey:@"type"];
    
    self.photos = [self models:[CTBOPhotoInfo class] withRawData:[aData nullProtectedObjectForKey:@"vehiclesPictures"]];
}

@end
