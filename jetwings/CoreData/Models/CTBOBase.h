#import <CoreData/CoreData.h>
#import "CTStorableObject.h"

//enum CTClonePolicy;
@class CTDataBaseRequest;

@interface CTBOBase : NSManagedObject <CTStorableObject>

@property (nonatomic, strong) NSNumber* identifier;
@property (nonatomic, strong) NSNumber* areaIdentifier;

+ (instancetype)makeObjectTemp;
+ (instancetype)makeObjectContext;
+ (instancetype)makeOrUpdateObjectWithDictionary:(NSDictionary *)aData;

+ (instancetype)objectByID:(NSString *)objID;
+ (NSArray *)objectsByIDs:(NSArray *)objIDs;

+ (NSArray *)objectsByAttributePredicat:(NSString *)aAtribute id:(NSString *)aId;
+ (NSArray*)allObjects;
+ (CTDataBaseRequest *)objectReqByID:(NSString *)objID;
+ (CTDataBaseRequest *)objectsReqByAttributePredicat:(NSString *)aAtribute id:(NSString *)aId;
+ (CTDataBaseRequest *)allObjectsReqOffset:(NSInteger)anOffset limit:(NSInteger)aLimit predicate:(NSPredicate *)aPredicate;
+ (CTDataBaseRequest *)allObjectsReq;


- (NSArray *)models:(Class)modelClass withRawData:(NSArray *)rawData;

@end
