#import "CTBOBase.h"
#import "CTMainStorage.h"
#import "CTBOBaseObject.h"


@interface CTBOBase ()

// Private interface goes here.

@end


@implementation CTBOBase

@dynamic identifier;
@dynamic areaIdentifier;

//- (void)dealloc
//{
//    NSLog(@"DEALLOC - %@",NSStringFromClass([self class]));
//}

#pragma mark - CTStorableObject

+ (NSManagedObjectID*)isStorageContainedObject:(NSManagedObject*)anObject
{
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"self.identifier == %@", ((CTBOBase*)anObject).identifier];
    return [CTMainStorageInstance isStorageContainedObject:anObject compareWithPredicate:predicate];
}

- (void)setupWithDictionary:(NSDictionary*)aData
{
    self.identifier = [aData nullProtectedObjectForKey:@"id"];
    self.areaIdentifier = CTAppManagerInstance.currentArea.ID;
}

- (void)insertIntoContext
{
    if (!self.managedObjectContext)
    {
        [CTMainStorageInstance executeBlock:^
         {
             [CTMainStorageInstance.managedObjectContext insertObject:self];
         }];
    }
}


+ (NSArray*)defaultSortDescriptors
{
    return @[[NSSortDescriptor sortDescriptorWithKey:@"identifier" ascending:YES]];
}

- (NSString *)mainModelInfoText
{
    return nil;
}

+ (instancetype)makeObjectTemp
{
    return (CTBOBase *)[CTMainStorageInstance tempObjectOfClass:[self class] fromEntityName:[[self class] entityName]];
}

+ (instancetype)makeObjectContext
{
    return (CTBOBase *)[CTMainStorageInstance objectOfClass:[self class] fromEntityName:[[self class] entityName]];
}

+ (instancetype)makeOrUpdateObjectWithDictionary:(NSDictionary *)aData
{
    CTBOBase *objectNew = [self makeObjectTemp];
    [objectNew setupWithDictionary:aData];

    CTBOBase *objectOld = [self objectByID:objectNew.identifier];
    
    if (!objectOld)
    {
        [objectNew insertIntoContext];
        objectOld = objectNew;
    } else
    {
        [objectOld setupWithDictionary:aData];
    }
    
    return objectOld;
}

+ (instancetype)objectByID:(NSNumber *)objID
{
    __block NSArray *array = nil;
    [CTMainStorageInstance executeBlock:^
     {
         NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[[self class] entityName]];
         request.sortDescriptors = [self defaultSortDescriptors];
         request.predicate = [NSPredicate predicateWithFormat:@"self.identifier == %@", objID];
         array = [CTMainStorageInstance.managedObjectContext executeFetchRequest:request error:nil];
     }];
    
    return [array lastObject];
}

+ (NSArray *)objectsByIDs:(NSArray *)objIDs
{
    __block NSMutableArray *array = [NSMutableArray new];
    
    for (NSString *objID in objIDs)
    {
        [CTMainStorageInstance executeBlock:^
         {
             NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[[self class] entityName]];
             request.sortDescriptors = [self defaultSortDescriptors];
             request.predicate = [NSPredicate predicateWithFormat:@"self.identifier == %@", objID];
             [array addObjectsFromArray: [CTMainStorageInstance.managedObjectContext executeFetchRequest:request error:nil]];
         }];
    }
    
    return array;
}


+ (NSArray *)objectsByAttributePredicat:(NSString *)aAtribute id:(NSString *)aId
{
    __block NSArray *array = nil;
    [CTMainStorageInstance executeBlock:^
    {
         NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[[self class] entityName]];
         request.sortDescriptors = [[self class] defaultSortDescriptors];
         request.predicate = [NSPredicate predicateWithFormat:@"self.%@ == %@",aAtribute, aId];
         array = [CTMainStorageInstance.managedObjectContext executeFetchRequest:request error:nil];
    }];
    
    return array;
}

+ (NSArray*)allObjects
{
    __block NSArray *array = nil;
    [CTMainStorageInstance executeBlock:^
     {
         NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[[self class] entityName]];
         request.sortDescriptors = [self defaultSortDescriptors];
         array = [CTMainStorageInstance.managedObjectContext executeFetchRequest:request error:nil];
     }];
    
    return array;
}

+ (CTDataBaseRequest *)objectReqByID:(NSString *)objID
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[[self class] entityName]];
    fetchRequest.sortDescriptors = [self defaultSortDescriptors];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"self.identifier == %@", objID];
    
    CTDataBaseRequest* request = [[CTDataBaseRequest alloc] initWithStorage:CTMainStorageInstance];
    request.fetchRequest = fetchRequest;
    
    return request;
}

+ (CTDataBaseRequest *)allObjectsReqOffset:(NSInteger)anOffset limit:(NSInteger)aLimit predicate:(NSPredicate *)aPredicate
{
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[[self class] entityName]];
    fetchRequest.sortDescriptors = [self defaultSortDescriptors];
    fetchRequest.predicate = aPredicate;
    
    fetchRequest.fetchOffset = anOffset;
    fetchRequest.fetchLimit = aLimit;
    
    CTDataBaseRequest* request = [[CTDataBaseRequest alloc] initWithStorage:CTMainStorageInstance];
    request.fetchRequest = fetchRequest;
    return request;
}

+ (CTDataBaseRequest *)objectsReqByAttributePredicat:(NSString *)aAtribute id:(NSString *)aId
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.%@ == %@",aAtribute, aId];
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[[self class] entityName]];
    fetchRequest.sortDescriptors = [self defaultSortDescriptors];
    fetchRequest.predicate = predicate;
    
    CTDataBaseRequest* request = [[CTDataBaseRequest alloc] initWithStorage:CTMainStorageInstance];
    request.fetchRequest = fetchRequest;
    return request;
}

+ (CTDataBaseRequest *)allObjectsReq
{
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[[self class] entityName]];
    fetchRequest.sortDescriptors = [self defaultSortDescriptors];
    
    CTDataBaseRequest* request = [[CTDataBaseRequest alloc] initWithStorage:CTMainStorageInstance];
    request.fetchRequest = fetchRequest;
    return request;
}

- (NSArray *)models:(Class)modelClass withRawData:(NSArray *)rawData
{
    NSArray *result = nil;
    if ([rawData isKindOfClass:[NSArray class]] && rawData.count &&
        [modelClass isSubclassOfClass:[CTBOBaseObject class]])
    {
        NSMutableArray *tempArray = [NSMutableArray new];
        for (NSDictionary *date in rawData)
        {
            CTBOBaseObject *obj = [modelClass initializeWithData:date];
            [tempArray addObject:obj];
        }
        result = [NSArray arrayWithArray:tempArray];
    }
    return result;
}

@end
