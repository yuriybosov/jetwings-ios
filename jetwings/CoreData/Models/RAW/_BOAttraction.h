// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BOAttraction.h instead.

@import CoreData;

#import "CTBOBase.h"

NS_ASSUME_NONNULL_BEGIN

@class NSObject;

@class NSObject;

@class NSObject;

@interface BOAttractionID : NSManagedObjectID {}
@end

@interface _BOAttraction : CTBOBase
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BOAttractionID*objectID;

@property (nonatomic, strong, nullable) NSString* address;

@property (nonatomic, strong, nullable) NSNumber* areaIdentifier;

@property (atomic) int16_t areaIdentifierValue;
- (int16_t)areaIdentifierValue;
- (void)setAreaIdentifierValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSString* descriptionText;

@property (nonatomic, strong, nullable) NSNumber* identifier;

@property (atomic) int32_t identifierValue;
- (int32_t)identifierValue;
- (void)setIdentifierValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSNumber* latitude;

@property (atomic) double latitudeValue;
- (double)latitudeValue;
- (void)setLatitudeValue:(double)value_;

@property (nonatomic, strong, nullable) NSString* links;

@property (nonatomic, strong, nullable) NSNumber* longitude;

@property (atomic) double longitudeValue;
- (double)longitudeValue;
- (void)setLongitudeValue:(double)value_;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) id nearbyHotels;

@property (nonatomic, strong, nullable) NSString* perfomance;

@property (nonatomic, strong, nullable) id photos;

@property (nonatomic, strong, nullable) NSString* picture;

@property (nonatomic, strong, nullable) id tickets;

@property (nonatomic, strong, nullable) NSString* work;

@end

@interface _BOAttraction (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAddress;
- (void)setPrimitiveAddress:(NSString*)value;

- (NSNumber*)primitiveAreaIdentifier;
- (void)setPrimitiveAreaIdentifier:(NSNumber*)value;

- (int16_t)primitiveAreaIdentifierValue;
- (void)setPrimitiveAreaIdentifierValue:(int16_t)value_;

- (NSString*)primitiveDescriptionText;
- (void)setPrimitiveDescriptionText:(NSString*)value;

- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int32_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int32_t)value_;

- (NSNumber*)primitiveLatitude;
- (void)setPrimitiveLatitude:(NSNumber*)value;

- (double)primitiveLatitudeValue;
- (void)setPrimitiveLatitudeValue:(double)value_;

- (NSString*)primitiveLinks;
- (void)setPrimitiveLinks:(NSString*)value;

- (NSNumber*)primitiveLongitude;
- (void)setPrimitiveLongitude:(NSNumber*)value;

- (double)primitiveLongitudeValue;
- (void)setPrimitiveLongitudeValue:(double)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (id)primitiveNearbyHotels;
- (void)setPrimitiveNearbyHotels:(id)value;

- (NSString*)primitivePerfomance;
- (void)setPrimitivePerfomance:(NSString*)value;

- (id)primitivePhotos;
- (void)setPrimitivePhotos:(id)value;

- (NSString*)primitivePicture;
- (void)setPrimitivePicture:(NSString*)value;

- (id)primitiveTickets;
- (void)setPrimitiveTickets:(id)value;

- (NSString*)primitiveWork;
- (void)setPrimitiveWork:(NSString*)value;

@end

@interface BOAttractionAttributes: NSObject 
+ (NSString *)address;
+ (NSString *)areaIdentifier;
+ (NSString *)descriptionText;
+ (NSString *)identifier;
+ (NSString *)latitude;
+ (NSString *)links;
+ (NSString *)longitude;
+ (NSString *)name;
+ (NSString *)nearbyHotels;
+ (NSString *)perfomance;
+ (NSString *)photos;
+ (NSString *)picture;
+ (NSString *)tickets;
+ (NSString *)work;
@end

NS_ASSUME_NONNULL_END
