// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BOGuide.h instead.

@import CoreData;

#import "CTBOBase.h"

NS_ASSUME_NONNULL_BEGIN

@interface BOGuideID : NSManagedObjectID {}
@end

@interface _BOGuide : CTBOBase
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BOGuideID*objectID;

@property (nonatomic, strong, nullable) NSNumber* age;

@property (atomic) int16_t ageValue;
- (int16_t)ageValue;
- (void)setAgeValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSNumber* areaIdentifier;

@property (atomic) int16_t areaIdentifierValue;
- (int16_t)areaIdentifierValue;
- (void)setAreaIdentifierValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSString* avatar;

@property (nonatomic, strong, nullable) NSDate* birthday;

@property (nonatomic, strong, nullable) NSNumber* experience;

@property (atomic) int16_t experienceValue;
- (int16_t)experienceValue;
- (void)setExperienceValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSString* gender;

@property (nonatomic, strong, nullable) NSNumber* identifier;

@property (atomic) int16_t identifierValue;
- (int16_t)identifierValue;
- (void)setIdentifierValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSString* languages;

@property (nonatomic, strong, nullable) NSString* license;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) NSString* national;

@property (nonatomic, strong, nullable) NSString* speciality;

@end

@interface _BOGuide (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAge;
- (void)setPrimitiveAge:(NSNumber*)value;

- (int16_t)primitiveAgeValue;
- (void)setPrimitiveAgeValue:(int16_t)value_;

- (NSNumber*)primitiveAreaIdentifier;
- (void)setPrimitiveAreaIdentifier:(NSNumber*)value;

- (int16_t)primitiveAreaIdentifierValue;
- (void)setPrimitiveAreaIdentifierValue:(int16_t)value_;

- (NSString*)primitiveAvatar;
- (void)setPrimitiveAvatar:(NSString*)value;

- (NSDate*)primitiveBirthday;
- (void)setPrimitiveBirthday:(NSDate*)value;

- (NSNumber*)primitiveExperience;
- (void)setPrimitiveExperience:(NSNumber*)value;

- (int16_t)primitiveExperienceValue;
- (void)setPrimitiveExperienceValue:(int16_t)value_;

- (NSString*)primitiveGender;
- (void)setPrimitiveGender:(NSString*)value;

- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int16_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int16_t)value_;

- (NSString*)primitiveLanguages;
- (void)setPrimitiveLanguages:(NSString*)value;

- (NSString*)primitiveLicense;
- (void)setPrimitiveLicense:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveNational;
- (void)setPrimitiveNational:(NSString*)value;

- (NSString*)primitiveSpeciality;
- (void)setPrimitiveSpeciality:(NSString*)value;

@end

@interface BOGuideAttributes: NSObject 
+ (NSString *)age;
+ (NSString *)areaIdentifier;
+ (NSString *)avatar;
+ (NSString *)birthday;
+ (NSString *)experience;
+ (NSString *)gender;
+ (NSString *)identifier;
+ (NSString *)languages;
+ (NSString *)license;
+ (NSString *)name;
+ (NSString *)national;
+ (NSString *)speciality;
@end

NS_ASSUME_NONNULL_END
