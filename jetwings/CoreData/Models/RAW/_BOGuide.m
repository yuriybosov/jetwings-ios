// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BOGuide.m instead.

#import "_BOGuide.h"

@implementation BOGuideID
@end

@implementation _BOGuide

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BOGuide" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BOGuide";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BOGuide" inManagedObjectContext:moc_];
}

- (BOGuideID*)objectID {
	return (BOGuideID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"ageValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"age"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"areaIdentifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"areaIdentifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"experienceValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"experience"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic age;

- (int16_t)ageValue {
	NSNumber *result = [self age];
	return [result shortValue];
}

- (void)setAgeValue:(int16_t)value_ {
	[self setAge:@(value_)];
}

- (int16_t)primitiveAgeValue {
	NSNumber *result = [self primitiveAge];
	return [result shortValue];
}

- (void)setPrimitiveAgeValue:(int16_t)value_ {
	[self setPrimitiveAge:@(value_)];
}

@dynamic areaIdentifier;

- (int16_t)areaIdentifierValue {
	NSNumber *result = [self areaIdentifier];
	return [result shortValue];
}

- (void)setAreaIdentifierValue:(int16_t)value_ {
	[self setAreaIdentifier:@(value_)];
}

- (int16_t)primitiveAreaIdentifierValue {
	NSNumber *result = [self primitiveAreaIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveAreaIdentifierValue:(int16_t)value_ {
	[self setPrimitiveAreaIdentifier:@(value_)];
}

@dynamic avatar;

@dynamic birthday;

@dynamic experience;

- (int16_t)experienceValue {
	NSNumber *result = [self experience];
	return [result shortValue];
}

- (void)setExperienceValue:(int16_t)value_ {
	[self setExperience:@(value_)];
}

- (int16_t)primitiveExperienceValue {
	NSNumber *result = [self primitiveExperience];
	return [result shortValue];
}

- (void)setPrimitiveExperienceValue:(int16_t)value_ {
	[self setPrimitiveExperience:@(value_)];
}

@dynamic gender;

@dynamic identifier;

- (int16_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result shortValue];
}

- (void)setIdentifierValue:(int16_t)value_ {
	[self setIdentifier:@(value_)];
}

- (int16_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveIdentifierValue:(int16_t)value_ {
	[self setPrimitiveIdentifier:@(value_)];
}

@dynamic languages;

@dynamic license;

@dynamic name;

@dynamic national;

@dynamic speciality;

@end

@implementation BOGuideAttributes 
+ (NSString *)age {
	return @"age";
}
+ (NSString *)areaIdentifier {
	return @"areaIdentifier";
}
+ (NSString *)avatar {
	return @"avatar";
}
+ (NSString *)birthday {
	return @"birthday";
}
+ (NSString *)experience {
	return @"experience";
}
+ (NSString *)gender {
	return @"gender";
}
+ (NSString *)identifier {
	return @"identifier";
}
+ (NSString *)languages {
	return @"languages";
}
+ (NSString *)license {
	return @"license";
}
+ (NSString *)name {
	return @"name";
}
+ (NSString *)national {
	return @"national";
}
+ (NSString *)speciality {
	return @"speciality";
}
@end

