// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BOGuidePricing.m instead.

#import "_BOGuidePricing.h"

@implementation BOGuidePricingID
@end

@implementation _BOGuidePricing

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BOGuidePricing" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BOGuidePricing";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BOGuidePricing" inManagedObjectContext:moc_];
}

- (BOGuidePricingID*)objectID {
	return (BOGuidePricingID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"areaIdentifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"areaIdentifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"priceValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"price"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic areaIdentifier;

- (int16_t)areaIdentifierValue {
	NSNumber *result = [self areaIdentifier];
	return [result shortValue];
}

- (void)setAreaIdentifierValue:(int16_t)value_ {
	[self setAreaIdentifier:@(value_)];
}

- (int16_t)primitiveAreaIdentifierValue {
	NSNumber *result = [self primitiveAreaIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveAreaIdentifierValue:(int16_t)value_ {
	[self setPrimitiveAreaIdentifier:@(value_)];
}

@dynamic descriptionText;

@dynamic identifier;

- (int16_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result shortValue];
}

- (void)setIdentifierValue:(int16_t)value_ {
	[self setIdentifier:@(value_)];
}

- (int16_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveIdentifierValue:(int16_t)value_ {
	[self setPrimitiveIdentifier:@(value_)];
}

@dynamic name;

@dynamic price;

- (float)priceValue {
	NSNumber *result = [self price];
	return [result floatValue];
}

- (void)setPriceValue:(float)value_ {
	[self setPrice:@(value_)];
}

- (float)primitivePriceValue {
	NSNumber *result = [self primitivePrice];
	return [result floatValue];
}

- (void)setPrimitivePriceValue:(float)value_ {
	[self setPrimitivePrice:@(value_)];
}

@end

@implementation BOGuidePricingAttributes 
+ (NSString *)areaIdentifier {
	return @"areaIdentifier";
}
+ (NSString *)descriptionText {
	return @"descriptionText";
}
+ (NSString *)identifier {
	return @"identifier";
}
+ (NSString *)name {
	return @"name";
}
+ (NSString *)price {
	return @"price";
}
@end

