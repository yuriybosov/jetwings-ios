// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BOHotel.h instead.

@import CoreData;

#import "CTBOBase.h"

NS_ASSUME_NONNULL_BEGIN

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@class NSObject;

@interface BOHotelID : NSManagedObjectID {}
@end

@interface _BOHotel : CTBOBase
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BOHotelID*objectID;

@property (nonatomic, strong, nullable) NSString* address;

@property (nonatomic, strong, nullable) id amenitiesType;

@property (nonatomic, strong, nullable) NSNumber* areaIdentifier;

@property (atomic) int16_t areaIdentifierValue;
- (int16_t)areaIdentifierValue;
- (void)setAreaIdentifierValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSString* areaName;

@property (nonatomic, strong, nullable) NSString* details;

@property (nonatomic, strong, nullable) NSString* district;

@property (nonatomic, strong, nullable) id guestType;

@property (nonatomic, strong, nullable) id hotelsRoom;

@property (nonatomic, strong, nullable) NSNumber* identifier;

@property (atomic) int32_t identifierValue;
- (int32_t)identifierValue;
- (void)setIdentifierValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSNumber* initialDetailedInfo;

@property (atomic) BOOL initialDetailedInfoValue;
- (BOOL)initialDetailedInfoValue;
- (void)setInitialDetailedInfoValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* latitude;

@property (atomic) double latitudeValue;
- (double)latitudeValue;
- (void)setLatitudeValue:(double)value_;

@property (nonatomic, strong, nullable) NSNumber* longitude;

@property (atomic) double longitudeValue;
- (double)longitudeValue;
- (void)setLongitudeValue:(double)value_;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) id nearbyAttractions;

@property (nonatomic, strong, nullable) id nearbyRestaurants;

@property (nonatomic, strong, nullable) id photos;

@property (nonatomic, strong, nullable) NSString* picture;

@property (nonatomic, strong, nullable) NSNumber* price;

@property (atomic) double priceValue;
- (double)priceValue;
- (void)setPriceValue:(double)value_;

@property (nonatomic, strong, nullable) NSNumber* rooms;

@property (atomic) int16_t roomsValue;
- (int16_t)roomsValue;
- (void)setRoomsValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSNumber* stars;

@property (atomic) int16_t starsValue;
- (int16_t)starsValue;
- (void)setStarsValue:(int16_t)value_;

@end

@interface _BOHotel (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAddress;
- (void)setPrimitiveAddress:(NSString*)value;

- (id)primitiveAmenitiesType;
- (void)setPrimitiveAmenitiesType:(id)value;

- (NSNumber*)primitiveAreaIdentifier;
- (void)setPrimitiveAreaIdentifier:(NSNumber*)value;

- (int16_t)primitiveAreaIdentifierValue;
- (void)setPrimitiveAreaIdentifierValue:(int16_t)value_;

- (NSString*)primitiveAreaName;
- (void)setPrimitiveAreaName:(NSString*)value;

- (NSString*)primitiveDetails;
- (void)setPrimitiveDetails:(NSString*)value;

- (NSString*)primitiveDistrict;
- (void)setPrimitiveDistrict:(NSString*)value;

- (id)primitiveGuestType;
- (void)setPrimitiveGuestType:(id)value;

- (id)primitiveHotelsRoom;
- (void)setPrimitiveHotelsRoom:(id)value;

- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int32_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int32_t)value_;

- (NSNumber*)primitiveInitialDetailedInfo;
- (void)setPrimitiveInitialDetailedInfo:(NSNumber*)value;

- (BOOL)primitiveInitialDetailedInfoValue;
- (void)setPrimitiveInitialDetailedInfoValue:(BOOL)value_;

- (NSNumber*)primitiveLatitude;
- (void)setPrimitiveLatitude:(NSNumber*)value;

- (double)primitiveLatitudeValue;
- (void)setPrimitiveLatitudeValue:(double)value_;

- (NSNumber*)primitiveLongitude;
- (void)setPrimitiveLongitude:(NSNumber*)value;

- (double)primitiveLongitudeValue;
- (void)setPrimitiveLongitudeValue:(double)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (id)primitiveNearbyAttractions;
- (void)setPrimitiveNearbyAttractions:(id)value;

- (id)primitiveNearbyRestaurants;
- (void)setPrimitiveNearbyRestaurants:(id)value;

- (id)primitivePhotos;
- (void)setPrimitivePhotos:(id)value;

- (NSString*)primitivePicture;
- (void)setPrimitivePicture:(NSString*)value;

- (NSNumber*)primitivePrice;
- (void)setPrimitivePrice:(NSNumber*)value;

- (double)primitivePriceValue;
- (void)setPrimitivePriceValue:(double)value_;

- (NSNumber*)primitiveRooms;
- (void)setPrimitiveRooms:(NSNumber*)value;

- (int16_t)primitiveRoomsValue;
- (void)setPrimitiveRoomsValue:(int16_t)value_;

- (NSNumber*)primitiveStars;
- (void)setPrimitiveStars:(NSNumber*)value;

- (int16_t)primitiveStarsValue;
- (void)setPrimitiveStarsValue:(int16_t)value_;

@end

@interface BOHotelAttributes: NSObject 
+ (NSString *)address;
+ (NSString *)amenitiesType;
+ (NSString *)areaIdentifier;
+ (NSString *)areaName;
+ (NSString *)details;
+ (NSString *)district;
+ (NSString *)guestType;
+ (NSString *)hotelsRoom;
+ (NSString *)identifier;
+ (NSString *)initialDetailedInfo;
+ (NSString *)latitude;
+ (NSString *)longitude;
+ (NSString *)name;
+ (NSString *)nearbyAttractions;
+ (NSString *)nearbyRestaurants;
+ (NSString *)photos;
+ (NSString *)picture;
+ (NSString *)price;
+ (NSString *)rooms;
+ (NSString *)stars;
@end

NS_ASSUME_NONNULL_END
