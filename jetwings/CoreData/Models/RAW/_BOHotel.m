// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BOHotel.m instead.

#import "_BOHotel.h"

@implementation BOHotelID
@end

@implementation _BOHotel

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BOHotel" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BOHotel";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BOHotel" inManagedObjectContext:moc_];
}

- (BOHotelID*)objectID {
	return (BOHotelID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"areaIdentifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"areaIdentifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"initialDetailedInfoValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"initialDetailedInfo"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"latitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"latitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"longitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"longitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"priceValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"price"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"roomsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"rooms"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"starsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"stars"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic address;

@dynamic amenitiesType;

@dynamic areaIdentifier;

- (int16_t)areaIdentifierValue {
	NSNumber *result = [self areaIdentifier];
	return [result shortValue];
}

- (void)setAreaIdentifierValue:(int16_t)value_ {
	[self setAreaIdentifier:@(value_)];
}

- (int16_t)primitiveAreaIdentifierValue {
	NSNumber *result = [self primitiveAreaIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveAreaIdentifierValue:(int16_t)value_ {
	[self setPrimitiveAreaIdentifier:@(value_)];
}

@dynamic areaName;

@dynamic details;

@dynamic district;

@dynamic guestType;

@dynamic hotelsRoom;

@dynamic identifier;

- (int32_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result intValue];
}

- (void)setIdentifierValue:(int32_t)value_ {
	[self setIdentifier:@(value_)];
}

- (int32_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result intValue];
}

- (void)setPrimitiveIdentifierValue:(int32_t)value_ {
	[self setPrimitiveIdentifier:@(value_)];
}

@dynamic initialDetailedInfo;

- (BOOL)initialDetailedInfoValue {
	NSNumber *result = [self initialDetailedInfo];
	return [result boolValue];
}

- (void)setInitialDetailedInfoValue:(BOOL)value_ {
	[self setInitialDetailedInfo:@(value_)];
}

- (BOOL)primitiveInitialDetailedInfoValue {
	NSNumber *result = [self primitiveInitialDetailedInfo];
	return [result boolValue];
}

- (void)setPrimitiveInitialDetailedInfoValue:(BOOL)value_ {
	[self setPrimitiveInitialDetailedInfo:@(value_)];
}

@dynamic latitude;

- (double)latitudeValue {
	NSNumber *result = [self latitude];
	return [result doubleValue];
}

- (void)setLatitudeValue:(double)value_ {
	[self setLatitude:@(value_)];
}

- (double)primitiveLatitudeValue {
	NSNumber *result = [self primitiveLatitude];
	return [result doubleValue];
}

- (void)setPrimitiveLatitudeValue:(double)value_ {
	[self setPrimitiveLatitude:@(value_)];
}

@dynamic longitude;

- (double)longitudeValue {
	NSNumber *result = [self longitude];
	return [result doubleValue];
}

- (void)setLongitudeValue:(double)value_ {
	[self setLongitude:@(value_)];
}

- (double)primitiveLongitudeValue {
	NSNumber *result = [self primitiveLongitude];
	return [result doubleValue];
}

- (void)setPrimitiveLongitudeValue:(double)value_ {
	[self setPrimitiveLongitude:@(value_)];
}

@dynamic name;

@dynamic nearbyAttractions;

@dynamic nearbyRestaurants;

@dynamic photos;

@dynamic picture;

@dynamic price;

- (double)priceValue {
	NSNumber *result = [self price];
	return [result doubleValue];
}

- (void)setPriceValue:(double)value_ {
	[self setPrice:@(value_)];
}

- (double)primitivePriceValue {
	NSNumber *result = [self primitivePrice];
	return [result doubleValue];
}

- (void)setPrimitivePriceValue:(double)value_ {
	[self setPrimitivePrice:@(value_)];
}

@dynamic rooms;

- (int16_t)roomsValue {
	NSNumber *result = [self rooms];
	return [result shortValue];
}

- (void)setRoomsValue:(int16_t)value_ {
	[self setRooms:@(value_)];
}

- (int16_t)primitiveRoomsValue {
	NSNumber *result = [self primitiveRooms];
	return [result shortValue];
}

- (void)setPrimitiveRoomsValue:(int16_t)value_ {
	[self setPrimitiveRooms:@(value_)];
}

@dynamic stars;

- (int16_t)starsValue {
	NSNumber *result = [self stars];
	return [result shortValue];
}

- (void)setStarsValue:(int16_t)value_ {
	[self setStars:@(value_)];
}

- (int16_t)primitiveStarsValue {
	NSNumber *result = [self primitiveStars];
	return [result shortValue];
}

- (void)setPrimitiveStarsValue:(int16_t)value_ {
	[self setPrimitiveStars:@(value_)];
}

@end

@implementation BOHotelAttributes 
+ (NSString *)address {
	return @"address";
}
+ (NSString *)amenitiesType {
	return @"amenitiesType";
}
+ (NSString *)areaIdentifier {
	return @"areaIdentifier";
}
+ (NSString *)areaName {
	return @"areaName";
}
+ (NSString *)details {
	return @"details";
}
+ (NSString *)district {
	return @"district";
}
+ (NSString *)guestType {
	return @"guestType";
}
+ (NSString *)hotelsRoom {
	return @"hotelsRoom";
}
+ (NSString *)identifier {
	return @"identifier";
}
+ (NSString *)initialDetailedInfo {
	return @"initialDetailedInfo";
}
+ (NSString *)latitude {
	return @"latitude";
}
+ (NSString *)longitude {
	return @"longitude";
}
+ (NSString *)name {
	return @"name";
}
+ (NSString *)nearbyAttractions {
	return @"nearbyAttractions";
}
+ (NSString *)nearbyRestaurants {
	return @"nearbyRestaurants";
}
+ (NSString *)photos {
	return @"photos";
}
+ (NSString *)picture {
	return @"picture";
}
+ (NSString *)price {
	return @"price";
}
+ (NSString *)rooms {
	return @"rooms";
}
+ (NSString *)stars {
	return @"stars";
}
@end

