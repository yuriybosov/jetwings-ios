// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BOOrder.h instead.

@import CoreData;

#import "CTBOBase.h"

NS_ASSUME_NONNULL_BEGIN

@class NSObject;

@class NSObject;

@interface BOOrderID : NSManagedObjectID {}
@end

@interface _BOOrder : CTBOBase
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BOOrderID*objectID;

@property (nonatomic, strong, nullable) NSNumber* areaIdentifier;

@property (atomic) int16_t areaIdentifierValue;
- (int16_t)areaIdentifierValue;
- (void)setAreaIdentifierValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSString* currensy;

@property (nonatomic, strong, nullable) NSDate* dateFrom;

@property (nonatomic, strong, nullable) NSDate* dateTo;

@property (nonatomic, strong, nullable) NSString* descriptionText;

@property (nonatomic, strong, nullable) NSNumber* identifier;

@property (atomic) int16_t identifierValue;
- (int16_t)identifierValue;
- (void)setIdentifierValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSNumber* nights;

@property (atomic) int16_t nightsValue;
- (int16_t)nightsValue;
- (void)setNightsValue:(int16_t)value_;

@property (nonatomic, strong, nullable) id ordersItems;

@property (nonatomic, strong, nullable) NSString* title;

@property (nonatomic, strong, nullable) NSNumber* total;

@property (atomic) double totalValue;
- (double)totalValue;
- (void)setTotalValue:(double)value_;

@property (nonatomic, strong, nullable) id user;

@end

@interface _BOOrder (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAreaIdentifier;
- (void)setPrimitiveAreaIdentifier:(NSNumber*)value;

- (int16_t)primitiveAreaIdentifierValue;
- (void)setPrimitiveAreaIdentifierValue:(int16_t)value_;

- (NSString*)primitiveCurrensy;
- (void)setPrimitiveCurrensy:(NSString*)value;

- (NSDate*)primitiveDateFrom;
- (void)setPrimitiveDateFrom:(NSDate*)value;

- (NSDate*)primitiveDateTo;
- (void)setPrimitiveDateTo:(NSDate*)value;

- (NSString*)primitiveDescriptionText;
- (void)setPrimitiveDescriptionText:(NSString*)value;

- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int16_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int16_t)value_;

- (NSNumber*)primitiveNights;
- (void)setPrimitiveNights:(NSNumber*)value;

- (int16_t)primitiveNightsValue;
- (void)setPrimitiveNightsValue:(int16_t)value_;

- (id)primitiveOrdersItems;
- (void)setPrimitiveOrdersItems:(id)value;

- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;

- (NSNumber*)primitiveTotal;
- (void)setPrimitiveTotal:(NSNumber*)value;

- (double)primitiveTotalValue;
- (void)setPrimitiveTotalValue:(double)value_;

- (id)primitiveUser;
- (void)setPrimitiveUser:(id)value;

@end

@interface BOOrderAttributes: NSObject 
+ (NSString *)areaIdentifier;
+ (NSString *)currensy;
+ (NSString *)dateFrom;
+ (NSString *)dateTo;
+ (NSString *)descriptionText;
+ (NSString *)identifier;
+ (NSString *)nights;
+ (NSString *)ordersItems;
+ (NSString *)title;
+ (NSString *)total;
+ (NSString *)user;
@end

NS_ASSUME_NONNULL_END
