// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BOOrder.m instead.

#import "_BOOrder.h"

@implementation BOOrderID
@end

@implementation _BOOrder

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BOOrder" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BOOrder";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BOOrder" inManagedObjectContext:moc_];
}

- (BOOrderID*)objectID {
	return (BOOrderID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"areaIdentifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"areaIdentifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"nightsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"nights"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"totalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"total"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic areaIdentifier;

- (int16_t)areaIdentifierValue {
	NSNumber *result = [self areaIdentifier];
	return [result shortValue];
}

- (void)setAreaIdentifierValue:(int16_t)value_ {
	[self setAreaIdentifier:@(value_)];
}

- (int16_t)primitiveAreaIdentifierValue {
	NSNumber *result = [self primitiveAreaIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveAreaIdentifierValue:(int16_t)value_ {
	[self setPrimitiveAreaIdentifier:@(value_)];
}

@dynamic currensy;

@dynamic dateFrom;

@dynamic dateTo;

@dynamic descriptionText;

@dynamic identifier;

- (int16_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result shortValue];
}

- (void)setIdentifierValue:(int16_t)value_ {
	[self setIdentifier:@(value_)];
}

- (int16_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveIdentifierValue:(int16_t)value_ {
	[self setPrimitiveIdentifier:@(value_)];
}

@dynamic nights;

- (int16_t)nightsValue {
	NSNumber *result = [self nights];
	return [result shortValue];
}

- (void)setNightsValue:(int16_t)value_ {
	[self setNights:@(value_)];
}

- (int16_t)primitiveNightsValue {
	NSNumber *result = [self primitiveNights];
	return [result shortValue];
}

- (void)setPrimitiveNightsValue:(int16_t)value_ {
	[self setPrimitiveNights:@(value_)];
}

@dynamic ordersItems;

@dynamic title;

@dynamic total;

- (double)totalValue {
	NSNumber *result = [self total];
	return [result doubleValue];
}

- (void)setTotalValue:(double)value_ {
	[self setTotal:@(value_)];
}

- (double)primitiveTotalValue {
	NSNumber *result = [self primitiveTotal];
	return [result doubleValue];
}

- (void)setPrimitiveTotalValue:(double)value_ {
	[self setPrimitiveTotal:@(value_)];
}

@dynamic user;

@end

@implementation BOOrderAttributes 
+ (NSString *)areaIdentifier {
	return @"areaIdentifier";
}
+ (NSString *)currensy {
	return @"currensy";
}
+ (NSString *)dateFrom {
	return @"dateFrom";
}
+ (NSString *)dateTo {
	return @"dateTo";
}
+ (NSString *)descriptionText {
	return @"descriptionText";
}
+ (NSString *)identifier {
	return @"identifier";
}
+ (NSString *)nights {
	return @"nights";
}
+ (NSString *)ordersItems {
	return @"ordersItems";
}
+ (NSString *)title {
	return @"title";
}
+ (NSString *)total {
	return @"total";
}
+ (NSString *)user {
	return @"user";
}
@end

