// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BORestaurant.m instead.

#import "_BORestaurant.h"

@implementation BORestaurantID
@end

@implementation _BORestaurant

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BORestaurant" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BORestaurant";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BORestaurant" inManagedObjectContext:moc_];
}

- (BORestaurantID*)objectID {
	return (BORestaurantID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"areaIdentifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"areaIdentifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"categoryValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"category"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"latitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"latitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"longitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"longitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic address;

@dynamic areaIdentifier;

- (int16_t)areaIdentifierValue {
	NSNumber *result = [self areaIdentifier];
	return [result shortValue];
}

- (void)setAreaIdentifierValue:(int16_t)value_ {
	[self setAreaIdentifier:@(value_)];
}

- (int16_t)primitiveAreaIdentifierValue {
	NSNumber *result = [self primitiveAreaIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveAreaIdentifierValue:(int16_t)value_ {
	[self setPrimitiveAreaIdentifier:@(value_)];
}

@dynamic category;

- (int16_t)categoryValue {
	NSNumber *result = [self category];
	return [result shortValue];
}

- (void)setCategoryValue:(int16_t)value_ {
	[self setCategory:@(value_)];
}

- (int16_t)primitiveCategoryValue {
	NSNumber *result = [self primitiveCategory];
	return [result shortValue];
}

- (void)setPrimitiveCategoryValue:(int16_t)value_ {
	[self setPrimitiveCategory:@(value_)];
}

@dynamic descriptionText;

@dynamic identifier;

- (int32_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result intValue];
}

- (void)setIdentifierValue:(int32_t)value_ {
	[self setIdentifier:@(value_)];
}

- (int32_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result intValue];
}

- (void)setPrimitiveIdentifierValue:(int32_t)value_ {
	[self setPrimitiveIdentifier:@(value_)];
}

@dynamic latitude;

- (double)latitudeValue {
	NSNumber *result = [self latitude];
	return [result doubleValue];
}

- (void)setLatitudeValue:(double)value_ {
	[self setLatitude:@(value_)];
}

- (double)primitiveLatitudeValue {
	NSNumber *result = [self primitiveLatitude];
	return [result doubleValue];
}

- (void)setPrimitiveLatitudeValue:(double)value_ {
	[self setPrimitiveLatitude:@(value_)];
}

@dynamic longitude;

- (double)longitudeValue {
	NSNumber *result = [self longitude];
	return [result doubleValue];
}

- (void)setLongitudeValue:(double)value_ {
	[self setLongitude:@(value_)];
}

- (double)primitiveLongitudeValue {
	NSNumber *result = [self primitiveLongitude];
	return [result doubleValue];
}

- (void)setPrimitiveLongitudeValue:(double)value_ {
	[self setPrimitiveLongitude:@(value_)];
}

@dynamic mealsSet;

@dynamic name;

@dynamic nearbyHotels;

@dynamic photos;

@dynamic picture;

@dynamic work;

@end

@implementation BORestaurantAttributes 
+ (NSString *)address {
	return @"address";
}
+ (NSString *)areaIdentifier {
	return @"areaIdentifier";
}
+ (NSString *)category {
	return @"category";
}
+ (NSString *)descriptionText {
	return @"descriptionText";
}
+ (NSString *)identifier {
	return @"identifier";
}
+ (NSString *)latitude {
	return @"latitude";
}
+ (NSString *)longitude {
	return @"longitude";
}
+ (NSString *)mealsSet {
	return @"mealsSet";
}
+ (NSString *)name {
	return @"name";
}
+ (NSString *)nearbyHotels {
	return @"nearbyHotels";
}
+ (NSString *)photos {
	return @"photos";
}
+ (NSString *)picture {
	return @"picture";
}
+ (NSString *)work {
	return @"work";
}
@end

