// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BOTour.h instead.

@import CoreData;

#import "CTBOBase.h"

NS_ASSUME_NONNULL_BEGIN

@class NSObject;

@class NSObject;

@interface BOTourID : NSManagedObjectID {}
@end

@interface _BOTour : CTBOBase
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BOTourID*objectID;

@property (nonatomic, strong, nullable) NSNumber* areaIdentifier;

@property (atomic) int16_t areaIdentifierValue;
- (int16_t)areaIdentifierValue;
- (void)setAreaIdentifierValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSString* descriptionText;

@property (nonatomic, strong, nullable) NSDate* endDate;

@property (nonatomic, strong, nullable) NSNumber* identifier;

@property (atomic) int16_t identifierValue;
- (int16_t)identifierValue;
- (void)setIdentifierValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) NSString* picture;

@property (nonatomic, strong, nullable) NSDate* startDate;

@property (nonatomic, strong, nullable) id tourPictures;

@property (nonatomic, strong, nullable) id tourTicket;

@end

@interface _BOTour (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAreaIdentifier;
- (void)setPrimitiveAreaIdentifier:(NSNumber*)value;

- (int16_t)primitiveAreaIdentifierValue;
- (void)setPrimitiveAreaIdentifierValue:(int16_t)value_;

- (NSString*)primitiveDescriptionText;
- (void)setPrimitiveDescriptionText:(NSString*)value;

- (NSDate*)primitiveEndDate;
- (void)setPrimitiveEndDate:(NSDate*)value;

- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int16_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int16_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitivePicture;
- (void)setPrimitivePicture:(NSString*)value;

- (NSDate*)primitiveStartDate;
- (void)setPrimitiveStartDate:(NSDate*)value;

- (id)primitiveTourPictures;
- (void)setPrimitiveTourPictures:(id)value;

- (id)primitiveTourTicket;
- (void)setPrimitiveTourTicket:(id)value;

@end

@interface BOTourAttributes: NSObject 
+ (NSString *)areaIdentifier;
+ (NSString *)descriptionText;
+ (NSString *)endDate;
+ (NSString *)identifier;
+ (NSString *)name;
+ (NSString *)picture;
+ (NSString *)startDate;
+ (NSString *)tourPictures;
+ (NSString *)tourTicket;
@end

NS_ASSUME_NONNULL_END
