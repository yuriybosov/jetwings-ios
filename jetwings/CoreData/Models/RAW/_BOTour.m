// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BOTour.m instead.

#import "_BOTour.h"

@implementation BOTourID
@end

@implementation _BOTour

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BOTour" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BOTour";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BOTour" inManagedObjectContext:moc_];
}

- (BOTourID*)objectID {
	return (BOTourID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"areaIdentifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"areaIdentifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic areaIdentifier;

- (int16_t)areaIdentifierValue {
	NSNumber *result = [self areaIdentifier];
	return [result shortValue];
}

- (void)setAreaIdentifierValue:(int16_t)value_ {
	[self setAreaIdentifier:@(value_)];
}

- (int16_t)primitiveAreaIdentifierValue {
	NSNumber *result = [self primitiveAreaIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveAreaIdentifierValue:(int16_t)value_ {
	[self setPrimitiveAreaIdentifier:@(value_)];
}

@dynamic descriptionText;

@dynamic endDate;

@dynamic identifier;

- (int16_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result shortValue];
}

- (void)setIdentifierValue:(int16_t)value_ {
	[self setIdentifier:@(value_)];
}

- (int16_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveIdentifierValue:(int16_t)value_ {
	[self setPrimitiveIdentifier:@(value_)];
}

@dynamic name;

@dynamic picture;

@dynamic startDate;

@dynamic tourPictures;

@dynamic tourTicket;

@end

@implementation BOTourAttributes 
+ (NSString *)areaIdentifier {
	return @"areaIdentifier";
}
+ (NSString *)descriptionText {
	return @"descriptionText";
}
+ (NSString *)endDate {
	return @"endDate";
}
+ (NSString *)identifier {
	return @"identifier";
}
+ (NSString *)name {
	return @"name";
}
+ (NSString *)picture {
	return @"picture";
}
+ (NSString *)startDate {
	return @"startDate";
}
+ (NSString *)tourPictures {
	return @"tourPictures";
}
+ (NSString *)tourTicket {
	return @"tourTicket";
}
@end

