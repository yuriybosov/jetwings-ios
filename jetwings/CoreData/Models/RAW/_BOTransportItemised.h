// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BOTransportItemised.h instead.

@import CoreData;

#import "CTBOBase.h"

NS_ASSUME_NONNULL_BEGIN

@interface BOTransportItemisedID : NSManagedObjectID {}
@end

@interface _BOTransportItemised : CTBOBase
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BOTransportItemisedID*objectID;

@property (nonatomic, strong, nullable) NSNumber* areaIdentifier;

@property (atomic) int16_t areaIdentifierValue;
- (int16_t)areaIdentifierValue;
- (void)setAreaIdentifierValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSString* descriptionText;

@property (nonatomic, strong, nullable) NSNumber* identifier;

@property (atomic) int16_t identifierValue;
- (int16_t)identifierValue;
- (void)setIdentifierValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) NSNumber* priceBig;

@property (atomic) int16_t priceBigValue;
- (int16_t)priceBigValue;
- (void)setPriceBigValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSNumber* priceSmall;

@property (atomic) int16_t priceSmallValue;
- (int16_t)priceSmallValue;
- (void)setPriceSmallValue:(int16_t)value_;

@end

@interface _BOTransportItemised (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAreaIdentifier;
- (void)setPrimitiveAreaIdentifier:(NSNumber*)value;

- (int16_t)primitiveAreaIdentifierValue;
- (void)setPrimitiveAreaIdentifierValue:(int16_t)value_;

- (NSString*)primitiveDescriptionText;
- (void)setPrimitiveDescriptionText:(NSString*)value;

- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int16_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int16_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitivePriceBig;
- (void)setPrimitivePriceBig:(NSNumber*)value;

- (int16_t)primitivePriceBigValue;
- (void)setPrimitivePriceBigValue:(int16_t)value_;

- (NSNumber*)primitivePriceSmall;
- (void)setPrimitivePriceSmall:(NSNumber*)value;

- (int16_t)primitivePriceSmallValue;
- (void)setPrimitivePriceSmallValue:(int16_t)value_;

@end

@interface BOTransportItemisedAttributes: NSObject 
+ (NSString *)areaIdentifier;
+ (NSString *)descriptionText;
+ (NSString *)identifier;
+ (NSString *)name;
+ (NSString *)priceBig;
+ (NSString *)priceSmall;
@end

NS_ASSUME_NONNULL_END
