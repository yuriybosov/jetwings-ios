// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BOTransportItemised.m instead.

#import "_BOTransportItemised.h"

@implementation BOTransportItemisedID
@end

@implementation _BOTransportItemised

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BOTransportItemised" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BOTransportItemised";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BOTransportItemised" inManagedObjectContext:moc_];
}

- (BOTransportItemisedID*)objectID {
	return (BOTransportItemisedID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"areaIdentifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"areaIdentifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"priceBigValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"priceBig"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"priceSmallValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"priceSmall"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic areaIdentifier;

- (int16_t)areaIdentifierValue {
	NSNumber *result = [self areaIdentifier];
	return [result shortValue];
}

- (void)setAreaIdentifierValue:(int16_t)value_ {
	[self setAreaIdentifier:@(value_)];
}

- (int16_t)primitiveAreaIdentifierValue {
	NSNumber *result = [self primitiveAreaIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveAreaIdentifierValue:(int16_t)value_ {
	[self setPrimitiveAreaIdentifier:@(value_)];
}

@dynamic descriptionText;

@dynamic identifier;

- (int16_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result shortValue];
}

- (void)setIdentifierValue:(int16_t)value_ {
	[self setIdentifier:@(value_)];
}

- (int16_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveIdentifierValue:(int16_t)value_ {
	[self setPrimitiveIdentifier:@(value_)];
}

@dynamic name;

@dynamic priceBig;

- (int16_t)priceBigValue {
	NSNumber *result = [self priceBig];
	return [result shortValue];
}

- (void)setPriceBigValue:(int16_t)value_ {
	[self setPriceBig:@(value_)];
}

- (int16_t)primitivePriceBigValue {
	NSNumber *result = [self primitivePriceBig];
	return [result shortValue];
}

- (void)setPrimitivePriceBigValue:(int16_t)value_ {
	[self setPrimitivePriceBig:@(value_)];
}

@dynamic priceSmall;

- (int16_t)priceSmallValue {
	NSNumber *result = [self priceSmall];
	return [result shortValue];
}

- (void)setPriceSmallValue:(int16_t)value_ {
	[self setPriceSmall:@(value_)];
}

- (int16_t)primitivePriceSmallValue {
	NSNumber *result = [self primitivePriceSmall];
	return [result shortValue];
}

- (void)setPrimitivePriceSmallValue:(int16_t)value_ {
	[self setPrimitivePriceSmall:@(value_)];
}

@end

@implementation BOTransportItemisedAttributes 
+ (NSString *)areaIdentifier {
	return @"areaIdentifier";
}
+ (NSString *)descriptionText {
	return @"descriptionText";
}
+ (NSString *)identifier {
	return @"identifier";
}
+ (NSString *)name {
	return @"name";
}
+ (NSString *)priceBig {
	return @"priceBig";
}
+ (NSString *)priceSmall {
	return @"priceSmall";
}
@end

