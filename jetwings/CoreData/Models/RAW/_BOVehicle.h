// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BOVehicle.h instead.

@import CoreData;

#import "CTBOBase.h"

NS_ASSUME_NONNULL_BEGIN

@class NSObject;

@interface BOVehicleID : NSManagedObjectID {}
@end

@interface _BOVehicle : CTBOBase
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BOVehicleID*objectID;

@property (nonatomic, strong, nullable) NSNumber* areaIdentifier;

@property (atomic) int16_t areaIdentifierValue;
- (int16_t)areaIdentifierValue;
- (void)setAreaIdentifierValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSString* brand;

@property (nonatomic, strong, nullable) NSNumber* identifier;

@property (atomic) int16_t identifierValue;
- (int16_t)identifierValue;
- (void)setIdentifierValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSString* model;

@property (nonatomic, strong, nullable) id photos;

@property (nonatomic, strong, nullable) NSString* seats;

@property (nonatomic, strong, nullable) NSString* type;

@property (nonatomic, strong, nullable) NSString* year;

@end

@interface _BOVehicle (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAreaIdentifier;
- (void)setPrimitiveAreaIdentifier:(NSNumber*)value;

- (int16_t)primitiveAreaIdentifierValue;
- (void)setPrimitiveAreaIdentifierValue:(int16_t)value_;

- (NSString*)primitiveBrand;
- (void)setPrimitiveBrand:(NSString*)value;

- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int16_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int16_t)value_;

- (NSString*)primitiveModel;
- (void)setPrimitiveModel:(NSString*)value;

- (id)primitivePhotos;
- (void)setPrimitivePhotos:(id)value;

- (NSString*)primitiveSeats;
- (void)setPrimitiveSeats:(NSString*)value;

- (NSString*)primitiveYear;
- (void)setPrimitiveYear:(NSString*)value;

@end

@interface BOVehicleAttributes: NSObject 
+ (NSString *)areaIdentifier;
+ (NSString *)brand;
+ (NSString *)identifier;
+ (NSString *)model;
+ (NSString *)photos;
+ (NSString *)seats;
+ (NSString *)type;
+ (NSString *)year;
@end

NS_ASSUME_NONNULL_END
