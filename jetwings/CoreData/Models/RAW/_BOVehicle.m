// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BOVehicle.m instead.

#import "_BOVehicle.h"

@implementation BOVehicleID
@end

@implementation _BOVehicle

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BOVehicle" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BOVehicle";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BOVehicle" inManagedObjectContext:moc_];
}

- (BOVehicleID*)objectID {
	return (BOVehicleID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"areaIdentifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"areaIdentifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic areaIdentifier;

- (int16_t)areaIdentifierValue {
	NSNumber *result = [self areaIdentifier];
	return [result shortValue];
}

- (void)setAreaIdentifierValue:(int16_t)value_ {
	[self setAreaIdentifier:@(value_)];
}

- (int16_t)primitiveAreaIdentifierValue {
	NSNumber *result = [self primitiveAreaIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveAreaIdentifierValue:(int16_t)value_ {
	[self setPrimitiveAreaIdentifier:@(value_)];
}

@dynamic brand;

@dynamic identifier;

- (int16_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result shortValue];
}

- (void)setIdentifierValue:(int16_t)value_ {
	[self setIdentifier:@(value_)];
}

- (int16_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveIdentifierValue:(int16_t)value_ {
	[self setPrimitiveIdentifier:@(value_)];
}

@dynamic model;

@dynamic photos;

@dynamic seats;

@dynamic type;

@dynamic year;

@end

@implementation BOVehicleAttributes 
+ (NSString *)areaIdentifier {
	return @"areaIdentifier";
}
+ (NSString *)brand {
	return @"brand";
}
+ (NSString *)identifier {
	return @"identifier";
}
+ (NSString *)model {
	return @"model";
}
+ (NSString *)photos {
	return @"photos";
}
+ (NSString *)seats {
	return @"seats";
}
+ (NSString *)type {
	return @"type";
}
+ (NSString *)year {
	return @"year";
}
@end

