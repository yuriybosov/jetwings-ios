//
//  CTBaseGateway.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/5/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>

@interface CTBaseGateway : CTGateway

- (CTGatewayRequest*)requestWithType:(NSString*)aType
                                path:(NSString*)aPath
                          parameters:(NSDictionary*)aParameters
                        successBlock:(CTGatewayRequestSuccessBlock)aSuccessBlock;

- (id)processData:(id)aResponseObject forResponse:(CTResponse*)aResponse;
- (id)modelFromRawDictionary:(NSDictionary *)aDictionary forModelClass:(Class)aModelClass;
- (NSMutableArray*)modelsFromRawData:(NSArray *)aData forModelClass:(Class)aModelClass;

@end
