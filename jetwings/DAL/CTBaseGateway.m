//
//  CTBaseGateway.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/5/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseGateway.h"
#import "CTMainStorage.h"
#import "CTBOBase.h"

@implementation CTBaseGateway

- (CTGatewayRequest*)requestWithType:(NSString*)aType
                                path:(NSString*)aPath
                          parameters:(NSDictionary*)aParameters
                        successBlock:(CTGatewayRequestSuccessBlock)aSuccessBlock
{
    CTGatewayRequest* request = [self requestWithType:aType
                                                 path:aPath
                                           parameters:aParameters
                                         successBlock:aSuccessBlock
                                        dispatchQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
    
    [request addValue:@"application/json" forHeaderField:@"Accept-Type"];
    [request addValue:@"application/json" forHeaderField:@"Content-Type"];
    
    if(CTAppManagerInstance.currentUser.userToken)
    {
        [request addValue:CTAppManagerInstance.currentUser.userToken forHeaderField:@"X_JETWINGS_TOKEN"];
    }
    
    return request;
}

- (CTGatewayRequestFailureBlock)defaultFailureBlockForRequest:(CTGatewayRequest*)aRequest
{
    CTGatewayRequestFailureBlock block = ^(AFHTTPRequestOperation *operation, NSError *error)
    {
        CTResponse* response = [CTResponse new];
        response.error = error;
        response.requestCancelled = operation.isCancelled;
        response.code = operation.response.statusCode;
        
        NSData *jsonData = [operation.responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSString *textMessage;
        if (jsonData)
        {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
            textMessage = [dict nullProtectedObjectForKey:@"message"];
        }
        if (textMessage.length)
        {
            response.textMessage = textMessage;
        }
        
        if ([[operation response] statusCode] == 403)
        {
            dispatch_async(dispatch_get_main_queue(), ^
            {
                CTShowSimpleAlert(CTLocalizedString(@"Error"), CTLocalizedString(@"Authorization error. Are you logged in another device?"));
                [CTAppManagerInstance logout];
            });
        }
        else
        {
            if(!response.textMessage.length && !response.isRequestCancelled)
            {
                if (error.localizedDescription.length)
                {
                    response.textMessage = error.localizedDescription;
                }
            }
        }
        
        return response;
    };
    
    return block;
}

- (id)processData:(id)aResponseObject forResponse:(CTResponse*)aResponse
{
    id result = nil;
    
    if ([aResponseObject isKindOfClass:[NSData class]])
    {
        NSError *error = nil;
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:aResponseObject options:NSJSONReadingMutableContainers error:&error];
        if (!error)
        {
            if ([dict nullProtectedObjectForKey:@"result"])
                result = [dict nullProtectedObjectForKey:@"result"];
            else
                result = dict;
            aResponse.success = YES;
        }
        else
        {
            aResponse.success = NO;
        }
    }
    return result;
}

- (id)modelFromRawDictionary:(NSDictionary *)aDictionary forModelClass:(Class)aModelClass
{
    id model = [CTMainStorageInstance tempObjectOfClass:aModelClass fromEntityName:[aModelClass entityName]];
    [model setupWithDictionary:aDictionary];
    return model;
}

- (NSMutableArray*)modelsFromRawData:(NSArray *)aData forModelClass:(Class)aModelClass
{
    if(![aData isKindOfClass:[NSArray class]])
        return nil;
    
    NSMutableArray* result = [NSMutableArray array];
    
    CTBOBase *model;
    NSArray* modelsArray = (NSArray*)aData;
    for(NSDictionary* dict in modelsArray)
    {
        model = [self modelFromRawDictionary:dict forModelClass:aModelClass];
        [result addObject:model];
    }
    return result;
}

@end
