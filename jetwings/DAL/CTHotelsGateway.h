//
//  CTHotelsGateway.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseGateway.h"
#import "CTBOHotelFilter.h"


@class CTBOOrdersItem;

@interface CTHotelsGateway : CTBaseGateway

CT_DECLARE_SINGLETON(CTHotelsGateway);

- (CTGatewayRequest *)hotelsListByFilter:(CTBOHotelFilter *)filter;
- (CTGatewayRequest *)hotelInfoById:(NSNumber *)hotelId dateFrom:(NSString *)dateFrom dateTo:(NSString *)dateTo;

- (CTGatewayRequest *)attractionsList;
- (CTGatewayRequest *)attractionInfoById:(NSNumber *)attractionId;

- (CTGatewayRequest *)restaurantsList;
- (CTGatewayRequest *)restaurantInfoById:(NSNumber *)restaurantId;

- (CTGatewayRequest *)vehicleList;
- (CTGatewayRequest *)vehicleInfoById:(NSNumber *)vehicleId;

- (CTGatewayRequest *)transportPackagesList;
- (CTGatewayRequest *)transportItemisedList;

- (CTGatewayRequest *)guidesList;
- (CTGatewayRequest *)guidesPricingList;
- (CTGatewayRequest *)guideInfoById:(NSNumber *)guideId;

- (CTGatewayRequest *)toursList;
- (CTGatewayRequest *)tourInfoById:(NSNumber *)tourId;

- (CTGatewayRequest *)ordersList;
- (CTGatewayRequest *)orderCurrent;
- (CTGatewayRequest *)orderInfoById:(NSNumber *)orderId;
- (CTGatewayRequest *)deleteOrderItem:(CTBOOrdersItem *)ordersItem;

//
- (CTGatewayRequest *)bookTransportWithData:(NSArray *)bookData;
- (CTGatewayRequest *)bookGuide:(NSArray *)bookData;
- (CTGatewayRequest *)bookTourPackage:(NSNumber *)tourPackageID bookData:(NSArray *)bookData;
- (CTGatewayRequest *)bookRestaurants:(NSNumber *)restaurantsID bookData:(NSArray *)bookData;
- (CTGatewayRequest *)bookHotel:(NSNumber *)hotelID
                      startDate:(NSString *)startDate
                        endDate:(NSString *)endDate
                       bookData:(NSArray *)bookData;
- (CTGatewayRequest *)bookAttraction:(NSNumber *)attractionID bookData:(NSArray *)bookData;

- (CTGatewayRequest *)updateCurrentOrderName:(NSString *)orderName
                            orderDescription:(NSString *)orderDescription
                                    dateFrom:(NSString *)dateFrom
                                      dateTo:(NSString *)dateTo;
- (CTGatewayRequest *)closeCurrentOrder;

@end
