//
//  CTHotelsGateway.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTHotelsGateway.h"
#import "BOHotel.h"
#import "BOAttraction.h"
#import "BORestaurant.h"
#import "BOVehicle.h"
#import "BOTransportItemised.h"
#import "BOTransportPackages.h"
#import "BOGuide.h"
#import "BOGuidePricing.h"
#import "BOTour.h"
#import "BOOrder.h"
#import "CTBOOrdersItem.h"

@implementation CTHotelsGateway

CT_IMPLEMENT_SINGLETON(CTHotelsGateway);

#pragma mark - Hotels

- (CTGatewayRequest *)hotelsListByFilter:(CTBOHotelFilter *)filter
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            NSArray *itemsRaw = [data nullProtectedObjectForKey:@"items"];
            
            if ([itemsRaw isKindOfClass:[NSArray class]])
            {
                NSMutableArray *items = [self modelsFromRawData:itemsRaw forModelClass:[BOHotel class]];
                [response.boArray addObjectsFromArray:items];
            }
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/gethotellist/%@/0/0",CTAppManagerInstance.currentArea.ID];
    return [self requestWithType:@"POST" path:path parameters:[filter gatewayRequestParams] successBlock:successBlock];
}

- (CTGatewayRequest *)hotelInfoById:(NSNumber *)hotelId dateFrom:(NSString *)dateFrom dateTo:(NSString *)dateTo
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            [response.dataDictionary setDictionary:data];
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/viewhotels/%@",hotelId];
    NSDictionary *params = nil;
    if (dateFrom && dateTo)
    {
        params = @{@"date_from":dateFrom, @"date_to":dateTo};
    }
    return [self requestWithType:@"POST" path:path parameters:params successBlock:successBlock];
}

#pragma mark - Attractions

- (CTGatewayRequest *)attractionsList
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            NSArray *itemsRaw = [data nullProtectedObjectForKey:@"items"];
            
            if ([itemsRaw isKindOfClass:[NSArray class]])
            {
                NSMutableArray *items = [self modelsFromRawData:itemsRaw forModelClass:[BOAttraction class]];
                [response.boArray addObjectsFromArray:items];
            }
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/getattractionslist/%@/0/0",CTAppManagerInstance.currentArea.ID];
    return [self requestWithType:@"GET" path:path parameters:nil successBlock:successBlock];
}

- (CTGatewayRequest *)attractionInfoById:(NSNumber *)attractionId
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            [response.dataDictionary setDictionary:data];
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/viewattraction/%@", attractionId];
    return [self requestWithType:@"GET" path:path parameters:nil successBlock:successBlock];
}

#pragma mark - Restaurants

- (CTGatewayRequest *)restaurantsList
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            NSArray *itemsRaw = [data nullProtectedObjectForKey:@"items"];
            
            if ([itemsRaw isKindOfClass:[NSArray class]])
            {
                NSMutableArray *items = [self modelsFromRawData:itemsRaw forModelClass:[BORestaurant class]];
                [response.boArray addObjectsFromArray:items];
            }
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/getrestaurantslist/%@/0/0",CTAppManagerInstance.currentArea.ID];
    return [self requestWithType:@"GET" path:path parameters:nil successBlock:successBlock];
}

- (CTGatewayRequest *)restaurantInfoById:(NSNumber *)restaurantId
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            [response.dataDictionary setDictionary:data];
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/viewrestaurants/%@", restaurantId];
    return [self requestWithType:@"GET" path:path parameters:nil successBlock:successBlock];
}

#pragma mark - Transport

- (CTGatewayRequest *)vehicleList
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            NSArray *itemsRaw = [data nullProtectedObjectForKey:@"items"];
            
            if ([itemsRaw isKindOfClass:[NSArray class]])
            {
                NSMutableArray *items = [self modelsFromRawData:itemsRaw forModelClass:[BOVehicle class]];
                [response.boArray addObjectsFromArray:items];
            }
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/getvehiclelist/%@/0/0",CTAppManagerInstance.currentArea.ID];
    return [self requestWithType:@"GET" path:path parameters:nil successBlock:successBlock];
}

- (CTGatewayRequest *)vehicleInfoById:(NSNumber *)vehicleId
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            [response.dataDictionary setDictionary:data];
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/viewvehicle/%@",vehicleId];
    return [self requestWithType:@"GET" path:path parameters:nil successBlock:successBlock];
}

- (CTGatewayRequest *)transportPackagesList
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            NSArray *itemsRaw = [data nullProtectedObjectForKey:@"items"];
            
            if ([itemsRaw isKindOfClass:[NSArray class]])
            {
                NSMutableArray *items = [self modelsFromRawData:itemsRaw forModelClass:[BOTransportPackages class]];
                [response.boArray addObjectsFromArray:items];
            }
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/getpackagelist/%@/0/0",CTAppManagerInstance.currentArea.ID];
    return [self requestWithType:@"GET" path:path parameters:nil successBlock:successBlock];
}

- (CTGatewayRequest *)transportItemisedList
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            NSArray *itemsRaw = [data nullProtectedObjectForKey:@"items"];
            
            if ([itemsRaw isKindOfClass:[NSArray class]])
            {
                NSMutableArray *items = [self modelsFromRawData:itemsRaw forModelClass:[BOTransportItemised class]];
                [response.boArray addObjectsFromArray:items];
            }
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/getitemiselist/%@/0/0",CTAppManagerInstance.currentArea.ID];
    return [self requestWithType:@"GET" path:path parameters:nil successBlock:successBlock];
}

#pragma mark - Guides

- (CTGatewayRequest *)guidesList
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            NSArray *itemsRaw = [data nullProtectedObjectForKey:@"items"];
            
            if ([itemsRaw isKindOfClass:[NSArray class]])
            {
                NSMutableArray *items = [self modelsFromRawData:itemsRaw forModelClass:[BOGuide class]];
                [response.boArray addObjectsFromArray:items];
            }
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/getguidelist/%@/0/0",CTAppManagerInstance.currentArea.ID];
    return [self requestWithType:@"GET" path:path parameters:nil successBlock:successBlock];
}

- (CTGatewayRequest *)guidesPricingList
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            NSArray *itemsRaw = [data nullProtectedObjectForKey:@"items"];
            
            if ([itemsRaw isKindOfClass:[NSArray class]])
            {
                NSMutableArray *items = [self modelsFromRawData:itemsRaw forModelClass:[BOGuidePricing class]];
                [response.boArray addObjectsFromArray:items];
            }
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/getguidepricing/%@",CTAppManagerInstance.currentArea.ID];
    return [self requestWithType:@"GET" path:path parameters:nil successBlock:successBlock];
}

- (CTGatewayRequest *)guideInfoById:(NSNumber *)guideId
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            [response.dataDictionary setDictionary:data];
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/viewguide/%@", guideId];
    return [self requestWithType:@"GET" path:path parameters:nil successBlock:successBlock];
}

#pragma mark - Tours

- (CTGatewayRequest *)toursList
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            NSArray *itemsRaw = [data nullProtectedObjectForKey:@"items"];
            
            if ([itemsRaw isKindOfClass:[NSArray class]])
            {
                NSMutableArray *items = [self modelsFromRawData:itemsRaw forModelClass:[BOTour class]];
                [response.boArray addObjectsFromArray:items];
            }
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/gettourpackagelist/%@/0/0",CTAppManagerInstance.currentArea.ID];
    return [self requestWithType:@"GET" path:path parameters:nil successBlock:successBlock];
}

- (CTGatewayRequest *)tourInfoById:(NSNumber *)tourId
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            [response.dataDictionary setDictionary:data];
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/viewtourpackage/%@", tourId];
    return [self requestWithType:@"GET" path:path parameters:nil successBlock:successBlock];
}

#pragma mark - Orders

- (CTGatewayRequest *)ordersList
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            NSArray *itemsRaw = [data nullProtectedObjectForKey:@"items"];
            
            if ([itemsRaw isKindOfClass:[NSArray class]])
            {
                NSMutableArray *items = [self modelsFromRawData:itemsRaw forModelClass:[BOOrder class]];
                [response.boArray addObjectsFromArray:items];
            }
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/getorderslist/%@/0/0",CTAppManagerInstance.currentArea.ID];
    return [self requestWithType:@"GET" path:path parameters:nil successBlock:successBlock];
}

- (CTGatewayRequest *)orderCurrent
{
    {
        CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
        {
            CTResponse *response = [CTResponse new];
            NSDictionary *data = [self processData:responseObject forResponse:response];
            if ([data isKindOfClass:[NSDictionary class]])
            {
                BOOrder *order = [self modelFromRawDictionary:data forModelClass:[BOOrder class]];
                [order setupDetailedDictionary:data];
                [response.boArray addObject:order];
                response.success = YES;
            }
            else
            {
                response.success = NO;
            }
            
            return response;
        };
        
        return [self requestWithType:@"GET" path:@"api/getorder" parameters:nil successBlock:successBlock];
    }
}

- (CTGatewayRequest *)orderInfoById:(NSNumber *)orderId
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            [response.dataDictionary setDictionary:data];
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/vieworder/%@", orderId];
    return [self requestWithType:@"GET" path:path parameters:nil successBlock:successBlock];
}

- (CTGatewayRequest *)deleteOrderItem:(CTBOOrdersItem *)ordersItem
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            [response.dataDictionary setDictionary:data];
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = @"api/deleteorderitem";
    
    NSDictionary *params = nil;
    if (ordersItem.ID)
        params = @{@"type":ordersItem.type, @"item_id":ordersItem.ID};
    else
         params = @{@"type":ordersItem.type};
    
    return [self requestWithType:@"POST" path:path parameters:params successBlock:successBlock];
}

#pragma mark - Book

- (CTGatewayRequest *)bookTransportWithData:(NSArray *)bookData
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            [response.dataDictionary setDictionary:data];
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = @"api/booktransport";
    NSDictionary *params = @{@"booking":[NSString stringWithFormat:@"[%@]",[bookData componentsJoinedByString:@","]]};
    
    return [self requestWithType:@"POST" path:path parameters:params successBlock:successBlock];
}

- (CTGatewayRequest *)bookGuide:(NSArray *)bookData
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            [response.dataDictionary setDictionary:data];
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = @"api/bookguide";
    NSDictionary *params = @{@"booking":[NSString stringWithFormat:@"[%@]",[bookData componentsJoinedByString:@","]]};
    
    return [self requestWithType:@"POST" path:path parameters:params successBlock:successBlock];
}

- (CTGatewayRequest *)bookTourPackage:(NSNumber *)tourPackageID bookData:(NSArray *)bookData
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            [response.dataDictionary setDictionary:data];
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/booktourpackage/%@", tourPackageID];
    NSDictionary *params = @{@"booking":[NSString stringWithFormat:@"[%@]",[bookData componentsJoinedByString:@","]]};
    
    return [self requestWithType:@"POST" path:path parameters:params successBlock:successBlock];
}

- (CTGatewayRequest *)bookRestaurants:(NSNumber *)restaurantsID bookData:(NSArray *)bookData
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            [response.dataDictionary setDictionary:data];
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/bookrestaurants/%@", restaurantsID];
    NSDictionary *params = @{@"booking":[NSString stringWithFormat:@"[%@]",[bookData componentsJoinedByString:@","]]};
    
    return [self requestWithType:@"POST" path:path parameters:params successBlock:successBlock];
}

- (CTGatewayRequest *)bookHotel:(NSNumber *)hotelID
                      startDate:(NSString *)startDate
                        endDate:(NSString *)endDate
                       bookData:(NSArray *)bookData
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            [response.dataDictionary setDictionary:data];
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/bookhotel/%@", hotelID];
    NSDictionary *params = @{@"booking":[NSString stringWithFormat:@"[%@]",[bookData componentsJoinedByString:@","]],
                             @"date_from":startDate,
                             @"date_to":endDate};
    
    return [self requestWithType:@"POST" path:path parameters:params successBlock:successBlock];
}

- (CTGatewayRequest *)bookAttraction:(NSNumber *)attractionID bookData:(NSArray *)bookData
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            [response.dataDictionary setDictionary:data];
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = [NSString stringWithFormat:@"api/bookattraction/%@", attractionID];
    NSDictionary *params = @{@"booking":[NSString stringWithFormat:@"[%@]",[bookData componentsJoinedByString:@","]]};
    
    return [self requestWithType:@"POST" path:path parameters:params successBlock:successBlock];
}

- (CTGatewayRequest *)updateCurrentOrderName:(NSString *)orderName
                            orderDescription:(NSString *)orderDescription
                                    dateFrom:(NSString *)dateFrom
                                      dateTo:(NSString *)dateTo
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            [response.dataDictionary setDictionary:data];
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = @"api/updateorder";
    
    NSDictionary *params = nil;
    if (dateFrom && dateTo)
    {
        params = @{@"title":orderName ?: @"",
                   @"description":orderDescription ?: @"",
                   @"date_from":dateFrom ,
                   @"date_to":dateTo};
    }
    else
    {
        params = @{@"title":orderName ?: @"",
                   @"description":orderDescription ?: @""};
    }
    
    return [self requestWithType:@"POST" path:path parameters:params successBlock:successBlock];
}

- (CTGatewayRequest *)closeCurrentOrder
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary *data = [self processData:responseObject forResponse:response];
        if ([data isKindOfClass:[NSDictionary class]])
        {
            [response.dataDictionary setDictionary:data];
            response.success = YES;
        }
        else
        {
            response.success = NO;
        }
        
        return response;
    };
    
    NSString *path = @"api/closeorder";
    
    return [self requestWithType:@"GET" path:path parameters:nil successBlock:successBlock];
}

@end
