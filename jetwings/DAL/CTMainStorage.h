//
//  CTMainStorage.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/5/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>

#define CTMainStorageInstance [CTMainStorage sharedInstance]

@interface CTMainStorage : CTStorage

CT_DECLARE_SINGLETON(CTMainStorage);

- (NSManagedObject*)objectOfClass:(Class)aClass;
- (NSManagedObject*)tempObjectOfClass:(Class)aClass;

- (void)removeObject:(id)aObject save:(BOOL)isSave;
- (void)removeObjects:(NSArray *)aObjects save:(BOOL)isSave;

@end
