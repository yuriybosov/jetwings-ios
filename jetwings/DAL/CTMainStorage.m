//
//  CTMainStorage.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/5/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTMainStorage.h"
#import "CTBOBase.h"

@implementation CTMainStorage

CT_IMPLEMENT_SINGLETON(CTMainStorage);

- (NSString *)persistentStoreName
{
    return @"DataModel.sqlite";
}

- (NSManagedObject*)updateIntoContextObjectID:(NSManagedObjectID*)anObjectID
                                    fromModel:(NSManagedObject*)aModel
                                   attributes:(NSSet*)anAttributes
{
    __block NSManagedObject* object = nil;
    [self executeBlock:^
     {
         object = [self.managedObjectContext existingObjectWithID:anObjectID error:nil];
         
         if(object)
         {
             NSMutableSet* attributes = (anAttributes.count) ? ([anAttributes mutableCopy]) :
             ([NSMutableSet setWithArray:[object.entity.attributesByName allKeys]]);
             
             if([[aModel class] respondsToSelector:@selector(notUpdatableAttributes)])
             {
                 NSSet* unupdatableAttributes = [[aModel class] notUpdatableAttributes];
                 [attributes minusSet:unupdatableAttributes];
             }
             
             for(NSString* attribute in attributes)
             {
                 if ([aModel valueForKey:attribute])
                 {
                     [object setValue:[aModel valueForKey:attribute] forKey:attribute];
                 }
             }
         }
     }];
    [((CTBOBase *)object) insertIntoContext];
    
    return object;
}

- (NSManagedObject*)objectOfClass:(Class)aClass
{
    return [self objectOfClass:aClass fromEntityName:/*[aClass entityName]*/NSStringFromClass(aClass)];
}

- (NSManagedObject*)tempObjectOfClass:(Class)aClass
{
    return [self tempObjectOfClass:aClass fromEntityName:/*[aClass entityName]*/NSStringFromClass(aClass)];
}

- (void)removeObject:(id)aObject save:(BOOL)isSave
{
    [self executeBlock:^
     {
         [self.managedObjectContext deleteObject:aObject];
     }];
    if (isSave)
    {
        [self save];
    }
}

- (void)removeObjects:(NSArray *)aObjects save:(BOOL)isSave
{
    for (id obj in aObjects)
    {
        [self executeBlock:^
         {
             [self.managedObjectContext deleteObject:obj];
         }];
    }
    
    if (isSave)
    {
        [self save];
    }
    
}

@end
