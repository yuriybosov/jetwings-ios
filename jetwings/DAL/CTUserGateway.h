//
//  CTUserGateway.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/11/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseGateway.h"

@interface CTUserGateway : CTBaseGateway

CT_DECLARE_SINGLETON(CTUserGateway);

- (CTGatewayRequest *)loginUserWithLogin:(NSString *)login password:(NSString *)password;
- (CTGatewayRequest *)changeCurensy:(NSString *)currency;
- (CTGatewayRequest *)changeLanguare:(NSString *)languare;

@end
