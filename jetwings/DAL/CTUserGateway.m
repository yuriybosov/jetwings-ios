//
//  CTUserGateway.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/11/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTUserGateway.h"

@implementation CTUserGateway

CT_IMPLEMENT_SINGLETON(CTUserGateway);

- (CTGatewayRequest *)loginUserWithLogin:(NSString *)login password:(NSString *)password
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        NSDictionary* data = [self processData:responseObject forResponse:response];
        if([data isKindOfClass:[NSDictionary class]] && response.success)
        {
            CTBOUser *user = [[CTBOUser alloc] init];
            [user setupWithDictionary:data];
            user.userToken = [[operation.response allHeaderFields] nullProtectedObjectForKey:@"X_jetwings_token"];
            
            CTAppManagerInstance.currentUser = user;
            CTLanguageMangerInstance.currentLanguage = user.userLang;
        }
        return response;
    };
    
    NSDictionary* params = @{@"email":login,
                             @"password":password,
                             @"device_id":CTAppManagerInstance.deviceId,
                             @"device_type":@"1",}; //iphone
    
    CTGatewayRequest* request = [self requestWithType:@"POST" path:@"api/login" parameters:params successBlock:successBlock];
    return request;
}

- (CTGatewayRequest *)changeCurensy:(NSString *)currency
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        [self processData:responseObject forResponse:response];
        return response;
    };
    
    NSDictionary* params = @{@"currency":currency};
    
    CTGatewayRequest* request = [self requestWithType:@"POST" path:@"api/setcurrency" parameters:params successBlock:successBlock];
    return request;
}

- (CTGatewayRequest *)changeLanguare:(NSString *)languare
{
    CTGatewayRequestSuccessBlock successBlock = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        CTResponse *response = [CTResponse new];
        [self processData:responseObject forResponse:response];
        return response;
    };
    
    NSDictionary* params = @{@"lang":languare};
    
    CTGatewayRequest* request = [self requestWithType:@"POST" path:@"api/setlang" parameters:params successBlock:successBlock];
    return request;
}

@end
