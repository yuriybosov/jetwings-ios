//
//  Defines.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/5/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#ifndef Defines_h
#define Defines_h

#define CTWeakSelf __weak __typeof(&*self)weakSelf = self;
#define CTStrongSelf __strong __typeof(&*weakSelf)strongSelf = weakSelf;

static NSString *const kFetcherMessageFilter            = @"filter";

static NSString *const kURLBase = @"http://ec2-52-29-87-251.eu-central-1.compute.amazonaws.com";
static NSString *const kAppID = @"JETWINGS";

static NSString *const kLoginScreenStoryBoardId         = @"kLoginScreenStoryBoardId";
static NSString *const kDrawerScreenStoryBoardId        = @"kDrawerScreenStoryBoardId";
static NSString *const kHomeScreenStoryBoardId          = @"kHomeScreenStoryBoardId";
static NSString *const kMenuScreenStoryBoardId          = @"kMenuScreenStoryBoardId";
static NSString *const kWelcomeScreenStoryBoardId       = @"kWelcomeScreenStoryBoardId";

static NSString *const kAttractionsScreenStoryBoardId   = @"kAttractionsScreenStoryBoardId";
static NSString *const kGuidesScreenStoryBoardId        = @"kGuidesScreenStoryBoardId";
static NSString *const kHotelsScreenStoryBoardId        = @"kHotelsScreenStoryBoardId";
static NSString *const kHotelDetailedScreenStoryBoardId = @"kHotelDetailedScreenStoryBoardId";
static NSString *const kMyQouteScreenStoryBoardId       = @"kMyQouteScreenStoryBoardId";
static NSString *const kOrderScreenStoryBoardId         = @"kOrderScreenStoryBoardId";
static NSString *const kRestaurantsScreenStoryBoardId   = @"kRestaurantsScreenStoryBoardId";
static NSString *const kMealsSetScreenStoryBoardId      = @"kMealsSetScreenStoryBoardId";
static NSString *const kSettingsScreenStoryBoardId      = @"kSettingsScreenStoryBoardId";
static NSString *const kToursScreenStoryBoardId         = @"kToursScreenStoryBoardId";
static NSString *const kTransportScreenStoryBoardId     = @"kTransportScreenStoryBoardId";

static NSString *const kDidChangeNumberOfPrice          = @"kDidChangeNumberOfPrice";

#endif /* Defines_h */
