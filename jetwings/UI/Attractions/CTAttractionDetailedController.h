//
//  CTAttractionDetailedController.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseTableController.h"
#import "BOAttraction.h"
#import "SwipeView.h"

@interface CTAttractionDetailedController : CTBaseTableController <SwipeViewDataSource, SwipeViewDelegate>

@property (nonatomic, strong) BOAttraction *attraction;

@end
