//
//  CTAttractionDetailedController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTAttractionDetailedController.h"
#import "CTHotelsGateway.h"
#import "CTTotalPriceView.h"
#import "CTWalkInPriceHeaderView.h"
#import "CTWalkInPriceCell.h"
#import "CTAttractionPriceCell.h"

@interface CTAttractionDetailedController ()
{
    CTTotalPriceView *totalPriceView;
    CTWalkInPriceHeaderView *walkInPricingView;
    
    CTSectionReadonly *pricingSection;
    CTSectionReadonly *walkInPricingSection;
}

@property (nonatomic, weak) IBOutlet SwipeView *swipeView;
@property (nonatomic, weak) IBOutlet UIPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UILabel *lbTitle;

@property (nonatomic, weak) IBOutlet UILabel *lbDescription;
@property (nonatomic, weak) IBOutlet UIView *descriptionView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *descriptionViewHeight;

@property (nonatomic, weak) IBOutlet UILabel *lbAddres;
@property (nonatomic, weak) IBOutlet UIView *addresView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *addresViewHeight;

@property (nonatomic, weak) IBOutlet UILabel *lbNearbyHotels;
@property (nonatomic, weak) IBOutlet UIView *nearbyHotelsView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *nearbyHotelsViewHeight;

@property (nonatomic, weak) IBOutlet UILabel *lbWork;
@property (nonatomic, weak) IBOutlet UIView *workView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *workViewHeight;

@property (nonatomic, weak) IBOutlet UILabel *lbRerformans;
@property (nonatomic, weak) IBOutlet UIView *rerformansView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *rerformansViewHeight;

@end

@implementation CTAttractionDetailedController

- (void)setup
{
    [super setup];
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTAttractionPriceCellData class]
                                                       forModel:[CTBOTicket class]];
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTWalkInPriceCellDate class]
                                                       forModel:[CTBOWalkInPrice class]];
    
    pricingSection = [CTSectionReadonly section];
    [tableDisposer addSection:pricingSection];
    
    walkInPricingSection = [CTSectionReadonly section];
    [tableDisposer addSection:walkInPricingSection];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangePrice:) name:kDidChangeNumberOfPrice object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    tableDisposer.tableView.tableHeaderView = nil;
    
    totalPriceView = [CTTotalPriceView create];
    walkInPricingView = [CTWalkInPriceHeaderView create];
    
    [self calculatePrice];
}

- (void)showAtractionInfo
{
    tableDisposer.tableView.tableHeaderView = self.tableViewHeader;
    
    self.title = _attraction.name;
    
    self.lbTitle.text = _attraction.name;
    self.pageControl.numberOfPages = [self.attraction.photos count];
    [self.swipeView reloadData];
    
    //description
    if (_attraction.descriptionText)
    {
        self.lbDescription.text = _attraction.descriptionText;
        self.descriptionView.hidden = NO;
        self.descriptionViewHeight.active = NO;
    }
    else
    {
        self.descriptionView.hidden = YES;
        self.descriptionViewHeight.active = YES;
        self.descriptionViewHeight.constant = 0;
    }
    
    //address
    if (_attraction.address)
    {
        self.lbAddres.text = _attraction.address;
        self.addresView.hidden = NO;
        self.addresViewHeight.active = NO;
    }
    else
    {
        self.addresView.hidden = YES;
        self.addresViewHeight.active = YES;
        self.addresViewHeight.constant = 0;
    }
    
    //nearbyHotels
    if ([_attraction nearbyHotelsText])
    {
        self.lbNearbyHotels.attributedText = [_attraction nearbyHotelsText];
        self.nearbyHotelsView.hidden = NO;
        self.nearbyHotelsViewHeight.active = NO;
    }
    else
    {
        self.nearbyHotelsView.hidden = YES;
        self.nearbyHotelsViewHeight.active = YES;
        self.nearbyHotelsViewHeight.constant = 0;
    }
    
    //work
    if (_attraction.work)
    {
        self.lbWork.text = _attraction.work;
        self.workView.hidden = NO;
        self.workViewHeight.active = NO;
    }
    else
    {
        self.workView.hidden = YES;
        self.workViewHeight.active = YES;
        self.workViewHeight.constant = 0;
    }
    
    //perfomance
    if ([_attraction perfomanceText])
    {
        self.lbRerformans.attributedText = [_attraction perfomanceText];
        self.rerformansView.hidden = NO;
        self.rerformansViewHeight.active = NO;
    }
    else
    {
        self.lbRerformans.attributedText = nil;
        self.rerformansView.hidden = YES;
        self.rerformansViewHeight.active = YES;
        self.rerformansViewHeight.constant = 0;
    }
    
    [self resizeTableViewHeaderForWidth:self.view.frame.size.width];
    
    [pricingSection removeAllCellData];
    [walkInPricingSection removeAllCellData];
    
    NSArray *pricings = _attraction.tickets;
    NSArray *walkInPricings = [_attraction walkInPricings];
    
    totalPriceView.lbPriceValue.text = [NSString stringWithFormat:@"%@0.00",[CTAppManagerInstance.currentUser currencySymbol]];
    
    if (pricings.count)
    {
        pricingSection.headerView = totalPriceView;
        walkInPricingSection.headerView = walkInPricingView;
        
        [((CTTableDisposerModeled *)tableDisposer) setupModels:pricings forSection:pricingSection];
        [((CTTableDisposerModeled *)tableDisposer) setupModels:walkInPricings forSection:walkInPricingSection];
    }
    else
    {
        pricingSection.headerView = nil;
        walkInPricingSection.headerView = nil;
    }
    
    [tableDisposer reloadData];
    [self calculatePrice];
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance backButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Actions

- (IBAction)addressByttonPresses:(id)sender
{
    if (fabs(_attraction.latitudeValue) > 0.01 &&
        fabs(_attraction.longitudeValue) > 0.01)
    {
        NSString *mapString = [NSString stringWithFormat:@"http://maps.apple.com/?ll=%@,%@",
                               _attraction.latitude,
                               _attraction.longitude];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mapString]];
    }
}

- (IBAction)sendButtonPressd
{
    NSMutableArray *selectedItems = [NSMutableArray array];
    for (NSInteger i = 0; i < pricingSection.cellDataCount; i++)
    {
        CTAttractionPriceCellData *cellData = (CTAttractionPriceCellData *)[pricingSection cellDataAtIndex:i];
        if (cellData.selected && [cellData priceData])
        {
            NSString *strData = [NSString stringWithFormat:@"{'id':%@,'quantity':%lu}",
                                 [[cellData priceData] priceIdentifier],
                                 [cellData count]];
            
            [selectedItems addObject:strData];
        }
    }
    
    if (selectedItems.count > 0)
    {
        CTRequest *request = [[CTHotelsGateway sharedInstance] bookAttraction:_attraction.identifier bookData:selectedItems];
        [self showActivity];
        CTWeakSelf
        [request addResponseBlock:^(CTResponse *aResponse)
         {
             [weakSelf bookDidEndWithResponse:aResponse];
             
         } responseQueue:dispatch_get_main_queue()];
        
        [request start];
    }
}

- (void)bookDidEndWithResponse:(CTResponse *)response
{
    [self hideActivity];
    if (response.success)
    {
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kOrderScreenStoryBoardId];
        [self.mm_drawerController setCenterViewController:vc];
    }
    else if (response.textMessage)
    {
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

#pragma mark - Fetsh Data

- (void)fetchData
{
    [self showActivity];
    CTWeakSelf
    CTRequest *request = [[CTHotelsGateway sharedInstance] attractionInfoById:_attraction.identifier];
    
    [request addResponseBlock:^(CTResponse *aResponse)
     {
         [weakSelf fetchDataDidFinish:aResponse];
     } responseQueue:dispatch_get_main_queue()];
    
    [request start];
}

- (void)fetchDataDidFinish:(CTResponse *)response
{
    [self hideActivity];
    if (response.success)
    {
        [_attraction setupDetailedDictionary:response.dataDictionary];
        
        [CTMainStorageInstance scheduleBlock:^
         {
             [CTMainStorageInstance save];
         }];
        
        [self showAtractionInfo];
    }
    else
    {
        [self showAtractionInfo];
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

#pragma mark - TableView

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CTAttractionPriceCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[CTAttractionPriceCell class]])
    {
        CTAttractionPriceCellData *cellData = (CTAttractionPriceCellData *)cell.cellData;
        cellData.selected = !cellData.selected;
        
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self calculatePrice];
    }
}

#pragma mark - SwipeViewDataSource

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    return [self.attraction.photos count];
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIImageView *)view
{
    self.pageControl.currentPage = swipeView.currentItemIndex;
    if (!view)
    {
        view = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, swipeView.frame.size.width, swipeView.frame.size.height)];
        view.clipsToBounds = YES;
        view.contentMode = UIViewContentModeScaleAspectFill;
        view.userInteractionEnabled = YES;
    }
    
    CTBOPhotoInfo *photoInfo = [self.attraction.photos objectAtIndex:index];
    NSURL *selectedImageURL = [NSURL URLWithString:photoInfo.photoPath];
    [view sd_setImageWithURL:selectedImageURL
            placeholderImage:nil
                     options:SDWebImageRetryFailed];
    
    return view;
}

#pragma mark - SwipeViewDelegate

- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView
{
    self.pageControl.currentPage = swipeView.currentItemIndex;
}

#pragma mark - Calculate Price

- (void)calculatePrice
{
    double price = 0;
    for (NSInteger i = 0; i < pricingSection.cellDataCount; i++)
    {
        CTAttractionPriceCellData *cellData = (CTAttractionPriceCellData *)[pricingSection cellDataAtIndex:i];
        if (cellData.selected)
        {
            price += cellData.count ? cellData.count * [[[cellData priceData] priceValue] floatValue] : [[[cellData priceData] priceValue] floatValue];
        }
    }
    totalPriceView.lbPriceValue.text = [NSString stringWithFormat:@"%@%0.2f", [CTAppManagerInstance.currentUser currencySymbol], price];
}

#pragma mark - Notification
- (void)didChangePrice:(NSNotification *)notification
{
    [self calculatePrice];
}

@end
