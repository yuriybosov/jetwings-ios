//
//  CTAttractionsController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTAttractionsController.h"
#import "CTAttractionCell.h"
#import "CTAttractionFetcher.h"
#import "CTAttractionDetailedController.h"

@implementation CTAttractionsController

#pragma mark - Setup

- (void)setup
{
    [super setup];
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTAttractionCellData class]
                                                       forModel:[BOAttraction class]];
    moduleList.dataFetcher = [[CTAttractionFetcher alloc] initWithStorage:CTMainStorageInstance];
}

- (BOOL)needShowChageAraeButton
{
    return YES;
}

#pragma mark - Languare

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    self.title = CTLocalizedString(@"Attractions");
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance menuButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    if (self.mm_drawerController.openSide != MMDrawerSideLeft)
    {
        [self.mm_drawerController openDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    }
    else
    {
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    }
}

#pragma mark - Storyboard Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(CTAttractionCell *)sender
{
    if ([sender isKindOfClass:[CTAttractionCell class]])
    {
        CTAttractionCellData *cellData = (CTAttractionCellData *)[tableDisposer cellDataByIndexPath:[tableDisposer.tableView indexPathForCell:sender]];
        ((CTAttractionDetailedController *)segue.destinationViewController).attraction = [cellData attraction];
    }
}

@end
