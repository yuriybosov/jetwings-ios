//
//  CTAttractionCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>
#import "BOAttraction.h"

@interface CTAttractionCellData : CTCellDataModeled

- (BOAttraction *)attraction;

@end


@interface CTAttractionCell : CTCell

@property (nonatomic, weak) IBOutlet UIImageView *picture;
@property (nonatomic, weak) IBOutlet UILabel *lbName;
@property (nonatomic, weak) IBOutlet UILabel *lbWork;

@end
