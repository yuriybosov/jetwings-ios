//
//  CTAttractionCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTAttractionCell.h"

@implementation CTAttractionCellData

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return 150;
}

- (BOAttraction *)attraction
{
    return self.model;
}

- (NSString *)cellIdentifier
{
    return @"attractionCell";
}

@end

@implementation CTAttractionCell

- (void)setupCellData:(CTAttractionCellData *)aCellData
{
    [super setupCellData:aCellData];
    
    [self.picture sd_setImageWithURL:[NSURL URLWithString:[aCellData attraction].picture]];
    self.lbName.text = [[aCellData attraction].name uppercaseString];
    self.lbWork.text = [aCellData attraction].work;
}

@end
