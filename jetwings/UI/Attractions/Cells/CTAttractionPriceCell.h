//
//  CTAttractionPriceCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/18/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTPriceCell.h"

@interface CTAttractionPriceCellData : CTPriceCellData

@end


@interface CTAttractionPriceCell : CTPriceCell

@end
