//
//  CTAttractionPriceCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/18/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTAttractionPriceCell.h"

@implementation CTAttractionPriceCellData

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return self.selected ? 50 : 50;
}

- (NSString *)cellIdentifier
{
    return @"attractionPriceCell";
}

@end


@implementation CTAttractionPriceCell

@end
