//
//  CTViewController.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/5/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>

@interface CTBaseController : CTBaseViewController

- (void)setupLanguareValues;

@end
