//
//  CTViewController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/5/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseController.h"

@interface CTBaseController ()

- (void)changeLanguare;

@end


@implementation CTBaseController

- (void)dealloc
{
    CTLog(@"!!! dealloc %@", NSStringFromClass([self class]));
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguare) name:kLanguageDidChangeNotification object:nil];
    
    [self setupLanguareValues];
}

#pragma mark - Rotate

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

#pragma mark - Languare

- (void)changeLanguare
{
    [self setupLanguareValues];
}

- (void)setupLanguareValues
{
    // base method empty
}

@end
