//
//  CTBaseModuleListController.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseTableController.h"
#import "CTModuleList.h"


@interface CTBaseModuleListController : CTBaseTableController <CTModuleListDelegate>
{
    CTModuleList *moduleList;
}

- (Class)classModuleList;

@end
