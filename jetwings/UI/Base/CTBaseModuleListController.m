//
//  CTBaseModuleListController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseModuleListController.h"
#import "CTActivityHUD.h"

@interface CTBaseModuleListController ()

@end

@implementation CTBaseModuleListController

- (void)setup
{
    [super setup];
    moduleList = [[[self classModuleList] alloc] initWithTableDisposer:(CTTableDisposerModeled *)tableDisposer];
    moduleList.activityAdapter = [CTActivityHUD new];
    moduleList.delegate = self;
    CTWeakSelf
    moduleList.fetcherFailedCallback = ^(CTModuleList* aModule, CTResponse* aResponse)
    {
        [weakSelf showAlertViewWithTitle:CTLocalizedString(@"Error") message:aResponse.textMessage];
    };
}

- (Class)classModuleList
{
    return [CTModuleList class];
}

- (void)viewDidLoad
{
    [moduleList configureWithView:self.view];
    
    [super viewDidLoad];
}

- (void)fetchData
{
    [moduleList reloadData];
}

@end
