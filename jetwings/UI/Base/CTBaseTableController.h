//
//  CTBaseTableController.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/5/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseController.h"
#import "CTTableDisposerModeled.h"

@interface CTBaseTableController : CTBaseController <CTTableDisposerDelegate, CTTableDisposerModeledDelegate>
{
    CTTableDisposer* tableDisposer;
    CTPopupSimplePicker* pickerArea;
}

@property(nonatomic,strong) CTTableDisposer *tableDisposer;
@property(nonatomic,weak) IBOutlet UITableView *tableView;
@property(nonatomic,strong) IBOutlet UIView *tableViewHeader;
@property(nonatomic,strong) IBOutlet UIView *tableViewFooter;

- (void)setup;
- (Class)tableDisposerClass;
- (void)configureTableDisposer;
- (void)configureTableView;
- (CGRect)tableViewFrame;

- (UIView*)createTableViewHeader;
- (UIView*)createTableViewFotter;

- (void)resizeTableViewHeaderForWidth:(CGFloat)width;
- (void)resizeTableViewFooterForWidth:(CGFloat)width;

- (void)fetchData;
- (BOOL)needShowChageAraeButton;
- (IBAction)areaButtonPressd:(id)sender;
- (void)didPickerToolbarItemNotification:(NSNotification*)aNotification;

@end