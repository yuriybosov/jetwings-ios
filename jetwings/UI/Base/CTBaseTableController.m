//
//  CTBaseTableController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/5/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseTableController.h"

@interface CTBaseTableController ()
{
    BOOL isSetuped;
}

@end

@implementation CTBaseTableController

@synthesize tableDisposer;

#pragma mark - Init/Dealloc

- (id)init
{
    self = [super init];
    if (self)
    {
        if (!isSetuped)
        {
            [self setup];
            isSetuped = YES;
        }
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        if (!isSetuped)
        {
            [self setup];
            isSetuped = YES;
        }
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        if (!isSetuped)
        {
            [self setup];
            isSetuped = YES;
        }
    }
    return self;
}
- (void)setup
{
    tableDisposer = [[self tableDisposerClass] new];
    tableDisposer.tableClass = [CTKeyboardAvoidingTableView class];
    [self configureTableDisposer];
}

- (BOOL)needShowChageAraeButton
{
    return NO;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureTableView];
    
    if ([self needShowChageAraeButton])
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPickerToolbarItemNotification:) name:CT_POPUPVIEW_TOOLBAR_ITEM_DID_PRESSED object:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    tableDisposer.tableView.frame = [self tableViewFrame];
}

#pragma mark - TableDisposer configuration

- (Class)tableDisposerClass
{
    return [CTTableDisposerModeled class];
}

- (void)configureTableDisposer
{
    tableDisposer.tableStyle = UITableViewStylePlain;
    tableDisposer.delegate = self;
    if ([tableDisposer isKindOfClass:[CTTableDisposerModeled class]])
        ((CTTableDisposerModeled*)tableDisposer).modeledDelegate = self;
}

- (void)configureTableView
{
    if (self.tableView)
    {
        [tableDisposer setupTableView:self.tableView];
    }
    else
    {
        tableDisposer.tableView.frame = [self tableViewFrame];
        tableDisposer.tableView.autoresizingMask = CTViewAutoresizingFlexibleSize;
        [self.view addSubview:tableDisposer.tableView];
    }
    tableDisposer.tableView.backgroundColor = [UIColor clearColor];
    tableDisposer.tableView.backgroundView = nil;
    tableDisposer.tableView.separatorColor = [UIColor clearColor];
    tableDisposer.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableDisposer.tableView.tableHeaderView = [self createTableViewHeader];
    tableDisposer.tableView.tableFooterView = [self createTableViewFotter];
}

- (CGRect)tableViewFrame
{
    CGRect rect = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    return rect;
}

- (UIView*)createTableViewHeader
{
    return self.tableViewHeader;
}

- (UIView*)createTableViewFotter
{
    return self.tableViewFooter;
}

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    [self fetchData];
}

- (void)fetchData
{
    //
}

#pragma mark - Resize

- (void)resizeTableViewHeaderForWidth:(CGFloat)aWidth
{
    self.tableViewHeader.frame = CGRectZero;
    
    CGSize size = [self.tableViewHeader systemLayoutSizeFittingSize:CGSizeMake(aWidth, 0) withHorizontalFittingPriority:UILayoutPriorityRequired verticalFittingPriority:UILayoutPriorityDefaultLow];
    
    CGRect frame = CGRectMake(0, 0, size.width, size.height);
    self.tableViewHeader.frame = frame;
    self.tableView.tableHeaderView = self.tableViewHeader;
}

- (void)resizeTableViewFooterForWidth:(CGFloat)aWidth
{
    self.tableViewFooter.frame = CGRectZero;
    CGSize size = [self.tableViewFooter systemLayoutSizeFittingSize:CGSizeMake(aWidth, 0) withHorizontalFittingPriority:UILayoutPriorityRequired verticalFittingPriority:UILayoutPriorityDefaultLow];
    
    CGRect frame = CGRectMake(0, 0, size.width, size.height);
    self.tableViewFooter.frame = frame;
    self.tableView.tableFooterView = self.tableViewFooter;
}

#pragma mark - Button Actions

- (IBAction)areaButtonPressd:(id)sender
{
    pickerArea = [[CTPopupSimplePicker alloc] init];
    pickerArea.backgroundColor = [UIColor colorWithRedI:246 greenI:247 blueI:250 alphaI:255];
    
    pickerArea.dataSource = CTAppManagerInstance.allAreas;
    pickerArea.toolbar = [UIConfiguratorInstance defaultPickerToolbarWithTitle:CTLocalizedString(@"Location")];
    
    CTTitledID *titleID = [pickerArea.dataSource titledIDByID:CTAppManagerInstance.currentArea.ID];
    
    [pickerArea prepareToShow];
    [pickerArea setSelectedItem: titleID];
    [pickerArea showWithAnimation:YES inView:self.navigationController.view];
}

#pragma mark - Notification

- (void)didPickerToolbarItemNotification:(NSNotification*)aNotification
{
    if (aNotification.object == pickerArea)
    {
        [pickerArea hideWithAnimation:YES];
        CTBOArea *area = (CTBOArea *)pickerArea.selectedItem;
        
        if ([area.ID integerValue] == [CTAppManagerInstance.currentArea.ID integerValue])
            return;
        
        CTAppManagerInstance.currentArea = area;
        [self fetchData];
    }
}


@end
