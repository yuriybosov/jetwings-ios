//
//  CTDrawerController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTDrawerController.h"

@interface CTDrawerController ()

@end

@implementation CTDrawerController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setMaximumLeftDrawerWidth:260.0];
    [self setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

@end
