//
//  CTPriceCellProtocol.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/17/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#ifndef CTPriceCellProtocol_h
#define CTPriceCellProtocol_h


@protocol CTPriceCellProtocol <NSObject>

- (NSString *)priceTitle;
- (NSString *)priceDescriprion;
- (NSNumber *)priceValue;

- (NSNumber *)priceIdentifier;

@end


#endif /* CTPriceCellProtocol_h */
