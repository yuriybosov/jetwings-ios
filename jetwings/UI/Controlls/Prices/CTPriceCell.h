//
//  CTPriceCell.h
//  ;
//
//  Created by Yuriy Bosov on 1/18/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>
#import "CTPriceCellProtocol.h"

@interface CTPriceCellData : CTCellDataModeled

@property (nonatomic, assign) BOOL selected;
@property (nonatomic, assign) NSUInteger count;

- (id<CTPriceCellProtocol>)priceData;


@end


@interface CTPriceCell : CTCell

@property (nonatomic, weak) IBOutlet UIImageView *checkImageView;
@property (nonatomic, weak) IBOutlet UILabel *lbTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbDescription;
@property (nonatomic, weak) IBOutlet UILabel *lbPriveValue;
@property (nonatomic, weak) IBOutlet UIStepper *stepper;

- (IBAction)steppetDidChange:(UIStepper *)sender;
- (NSNumber *)priceOfModel;

@end
