//
//  CTPriceCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/18/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTPriceCell.h"

@implementation CTPriceCellData

- (id)initWithModel:(id)aModel
{
    self = [super initWithModel:aModel];
    if (self)
    {
        self.cellSelectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setSelected:(BOOL)selected
{
    _selected = selected;
    self.count = selected ? 1 : 0;
}

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return self.selected ? 80 : 44;
}

- (NSString *)cellIdentifier
{
    return @"transportCell";
}

- (id<CTPriceCellProtocol>)priceData
{
    if ([self.model conformsToProtocol:@protocol(CTPriceCellProtocol)])
    {
        return self.model;
    }
    return nil;
}

@end

@implementation CTPriceCell

//- (void)setPriceValue
//{
//    CTPriceCellData *aCellData = (CTPriceCellData *)self.cellData;
//    
//    self.lbPriveValue.text = aCellData.count > 1 ?
//    [NSString stringWithFormat:@"%lu x %@%0.2f", (unsigned long)aCellData.count, [CTAppManagerInstance.currentUser currencySymbol], [[[aCellData priceData] priceValue] doubleValue]] :
//    [NSString stringWithFormat:@"%@%0.2f", [CTAppManagerInstance.currentUser currencySymbol], [[[aCellData priceData] priceValue] doubleValue]];
//}

- (NSNumber *)priceOfModel
{
    CTPriceCellData *aCellData = (CTPriceCellData *)self.cellData;
    return [[aCellData priceData] priceValue];
}

- (void)setPriceValue:(NSNumber *)price forCount:(NSUInteger)count
{
    self.lbPriveValue.text = count > 1 ?
    [NSString stringWithFormat:@"%lu x %@%0.2f", count, [CTAppManagerInstance.currentUser currencySymbol], [price doubleValue]] :
    [NSString stringWithFormat:@"%@%0.2f", [CTAppManagerInstance.currentUser currencySymbol], [price doubleValue]];
}

- (void)setupCellData:(CTPriceCellData *)aCellData
{
    [super setupCellData:aCellData];
    
    self.stepper.value = aCellData.count;
    self.lbTitle.text = [[aCellData priceData] priceTitle];
    self.lbDescription.text = [[aCellData priceData] priceDescriprion];
    
    [self setPriceValue:[self priceOfModel] forCount:aCellData.count];
    
    self.checkImageView.image = aCellData.selected ? [UIImage imageNamed:@"checkbox_active"] : [UIImage imageNamed:@"checkbox_inactive"];
    self.stepper.hidden = !aCellData.selected;
}

- (IBAction)steppetDidChange:(UIStepper *)sender
{
    CTPriceCellData *aCellData = (CTPriceCellData *)self.cellData;
    aCellData.count = sender.value;
    [self setPriceValue:[self priceOfModel] forCount:aCellData.count];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidChangeNumberOfPrice object:[aCellData priceData]];
}

@end
