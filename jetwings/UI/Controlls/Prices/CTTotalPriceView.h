//
//  CTAttractionTotalPriceView.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/18/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTTotalPriceView : UIView

@property (nonatomic, weak) IBOutlet UILabel *lbPriceTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbPriceValue;

+ (CTTotalPriceView *)create;

@end
