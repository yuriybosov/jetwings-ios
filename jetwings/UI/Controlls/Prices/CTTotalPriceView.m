//
//  CTAttractionTotalPriceView.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/18/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTTotalPriceView.h"

@implementation CTTotalPriceView

+ (CTTotalPriceView *)create
{
    CTTotalPriceView *view = [[[NSBundle mainBundle] loadNibNamed:@"CTTotalPriceView" owner:nil options:nil] firstObject];
    
    view.lbPriceTitle.text = CTLocalizedString(@"Total price");
    view.lbPriceValue.text = [NSString stringWithFormat:@"%@0.00",[CTAppManagerInstance.currentUser currencySymbol]];
    
    view.frame = CGRectMake(0, 0, 320, 50);
    view.translatesAutoresizingMaskIntoConstraints = YES;
    
    return view;
}

@end
