//
//  CTWalkInPriceCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/18/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>
#import "CTBOWalkInPrice.h"

@interface CTWalkInPriceCellDate : CTCellDataModeled

- (CTBOWalkInPrice*)walkInPrice;

@end


@interface CTWalkInPriceCell : CTCell

@property (nonatomic, weak) IBOutlet UILabel *lbTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbPrice;

@end
