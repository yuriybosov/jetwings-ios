//
//  CTWalkInPriceCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/18/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTWalkInPriceCell.h"

@implementation CTWalkInPriceCellDate

- (id)initWithModel:(id)aModel
{
    self = [super initWithModel:aModel];
    if  (self)
    {
        self.cellSelectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return 50;
}

- (NSString *)cellIdentifier
{
    return @"walkInPriceCell";
}

- (CTBOWalkInPrice*)walkInPrice
{
    return self.model;
}

@end


@implementation CTWalkInPriceCell

- (void)setupCellData:(CTWalkInPriceCellDate *)aCellData
{
    [super setupCellData:aCellData];
    
    self.lbPrice.text = [NSString stringWithFormat:@"%@%0.2f", [CTAppManagerInstance.currentUser currencySymbol], [[aCellData walkInPrice].price doubleValue]];
    self.lbTitle.text = [aCellData walkInPrice].title;
}

@end
