//
//  CTWalkInPriceHeaderView.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/18/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTWalkInPriceHeaderView.h"

@implementation CTWalkInPriceHeaderView

+ (CTWalkInPriceHeaderView *)create
{
    CTWalkInPriceHeaderView *view = [[[NSBundle mainBundle] loadNibNamed:@"CTWalkInPriceHeaderView" owner:nil options:nil] firstObject];
    
    view.lbTitle.text = CTLocalizedString(@"Walk-in price");
    view.frame = CGRectMake(0, 0, 320, 50);
    view.translatesAutoresizingMaskIntoConstraints = YES;
    
    return view;
}

@end
