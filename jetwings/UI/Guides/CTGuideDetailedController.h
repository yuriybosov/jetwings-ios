//
//  CTGuideDetailedController.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/21/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseController.h"
#import "BOGuide.h"

@interface CTGuideDetailedController : CTBaseController

@property (nonatomic, strong) BOGuide* guide;

@end
