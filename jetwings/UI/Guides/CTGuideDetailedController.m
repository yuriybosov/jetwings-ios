//
//  CTGuideDetailedController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/21/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTGuideDetailedController.h"
#import "CTHotelsGateway.h"

@interface CTGuideDetailedController ()

@property (nonatomic, weak) IBOutlet UIImageView *photoView;
@property (nonatomic, weak) IBOutlet UIImageView *photoViewBG;

@property (nonatomic, weak) IBOutlet UILabel *lbName;
@property (nonatomic, weak) IBOutlet UILabel *lbInfo;

// Nationality
@property (nonatomic, weak) IBOutlet UIView *nationalityView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *nationalityViewHeight;
@property (nonatomic, weak) IBOutlet UILabel *nationalityTitle;
@property (nonatomic, weak) IBOutlet UILabel *nationalityValue;

// License
@property (nonatomic, weak) IBOutlet UIView *licenseView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *licenseViewHeight;
@property (nonatomic, weak) IBOutlet UILabel *licenseTitle;
@property (nonatomic, weak) IBOutlet UILabel *licenseValue;

// Languages
@property (nonatomic, weak) IBOutlet UIView *languagesView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *languagesViewHeight;
@property (nonatomic, weak) IBOutlet UILabel *languagesTitle;
@property (nonatomic, weak) IBOutlet UILabel *languagesValue;

// Speciality
@property (nonatomic, weak) IBOutlet UIView *specialityView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *specialityViewHeight;
@property (nonatomic, weak) IBOutlet UILabel *specialityTitle;
@property (nonatomic, weak) IBOutlet UILabel *specialityValue;

// Nationality
@property (nonatomic, weak) IBOutlet UIView *experienceView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *experienceViewHeight;
@property (nonatomic, weak) IBOutlet UILabel *experienceTitle;
@property (nonatomic, weak) IBOutlet UILabel *experienceValue;

@end

@implementation CTGuideDetailedController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.photoView.layer.cornerRadius = self.photoView.frame.size.width/2;
    self.photoViewBG.layer.cornerRadius = self.photoViewBG.frame.size.width/2;
    
    CTRequest *request = [[CTHotelsGateway sharedInstance] guideInfoById:self.guide.identifier];
    [self showActivity];
    
    CTWeakSelf
    [request addResponseBlock:^(CTResponse *aResponse)
     {
         [weakSelf didEndRequestWithResponse:aResponse];
     } responseQueue:dispatch_get_main_queue()];
    
    [request start];
}

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    self.title = _guide.name;
    
    [self.photoView sd_setImageWithURL:[NSURL URLWithString:_guide.avatar]];
    self.lbName.text = _guide.name;
    self.lbInfo.text = [_guide guideInfo];
    
    self.nationalityTitle.text = CTLocalizedString(@"Nationality");
    self.licenseTitle.text = CTLocalizedString(@"License");
    self.languagesTitle.text = CTLocalizedString(@"Languages");
    self.specialityTitle.text = CTLocalizedString(@"Speciality");
    self.experienceTitle.text = CTLocalizedString(@"Experience");
    
    if (_guide.national)
    {
        self.nationalityValue.text = _guide.national;
        self.nationalityView.hidden = NO;
        self.nationalityViewHeight.active = NO;
    }
    else
    {
        self.nationalityView.hidden = YES;
        self.nationalityViewHeight.active = YES;
        self.nationalityViewHeight.constant = 0;
    }
    
    if (_guide.license)
    {
        self.licenseValue.text = _guide.license;
        self.licenseView.hidden = NO;
        self.licenseViewHeight.active = NO;
    }
    else
    {
        self.licenseView.hidden = YES;
        self.licenseViewHeight.active = YES;
        self.licenseViewHeight.constant = 0;
    }
    
    if (_guide.languages)
    {
        self.languagesValue.text = _guide.languages;
        self.languagesView.hidden = NO;
        self.languagesViewHeight.active = NO;
    }
    else
    {
        self.languagesView.hidden = YES;
        self.languagesViewHeight.active = YES;
        self.languagesViewHeight.constant = 0;
    }
    
    if (_guide.speciality)
    {
        self.specialityValue.text = _guide.speciality;
        self.specialityView.hidden = NO;
        self.specialityViewHeight.active = NO;
    }
    else
    {
        self.specialityView.hidden = YES;
        self.specialityViewHeight.active = YES;
        self.specialityViewHeight.constant = 0;
    }
    
    if ([_guide.experience integerValue] > 0)
    {
        self.experienceValue.text = [NSString stringWithFormat:@"%@ %@",_guide.experience, CTLocalizedString(@"years")];
        self.experienceView.hidden = NO;
        self.experienceViewHeight.active = NO;
    }
    else
    {
        self.experienceView.hidden = YES;
        self.experienceViewHeight.active = YES;
        self.experienceViewHeight.constant = 0;
    }
}

- (void)didEndRequestWithResponse:(CTResponse *)response
{
    [self hideActivity];
    if (response.success)
    {
        [self.guide setupDetailedDictionary:response.dataDictionary];
        [CTMainStorageInstance scheduleBlock:^
         {
             [CTMainStorageInstance save];
         }];
        [self setupLanguareValues];
    }
    else if (response.textMessage)
    {
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance backButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
