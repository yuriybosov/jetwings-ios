//
//  CTGuidesController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTGuidesController.h"
#import "CTGuidesCompoundFetcher.h"
#import "CTTotalPriceView.h"
#import "CTGuideDetailedController.h"
#import "CTGuideCell.h"
#import "CTPriceGuideCell.h"
#import "CTHotelsGateway.h"

@interface CTGuidesController ()
{
    CTGuidesCompoundFetcher *fetcher;
    
    NSArray *guiges;
    NSArray *pricings;
    
    BOOL showAllGuiges;
    CTSectionReadonly *guigesSection;
    CTSectionReadonly *pricingSection;
    
    CTTotalPriceView* totalPriceView;
}

@property (nonatomic, weak) IBOutlet UIButton *showAllGuidesButton;

- (void)calculatePrice;

@end

@implementation CTGuidesController

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    self.title = CTLocalizedString(@"Tour Guides");
    
    [self.showAllGuidesButton setTitle:[NSString stringWithFormat:@"  %@  ", CTLocalizedString(@"View all guides")]
                              forState:UIControlStateNormal];

}

- (void)setup
{
    [super setup];
    fetcher = [CTGuidesCompoundFetcher new];
    
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTGuideCellData class]
                                                       forModel:[BOGuide class]];
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTPriceGuideCellData class]
                                                       forModel:[BOGuidePricing class]];
    
    guigesSection = [CTSectionReadonly section];
    pricingSection = [CTSectionReadonly section];
    [tableDisposer addSection:guigesSection];
    [tableDisposer addSection:pricingSection];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangePrice:) name:kDidChangeNumberOfPrice object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    totalPriceView = [CTTotalPriceView create];
    pricingSection.headerView = totalPriceView;
    
    [self calculatePrice];
}

- (BOOL)needShowChageAraeButton
{
    return YES;
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance menuButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    if (self.mm_drawerController.openSide != MMDrawerSideLeft)
    {
        [self.mm_drawerController openDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    }
    else
    {
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    }
}

- (void)fetchData
{
    [self showActivity];
    CTWeakSelf
    [fetcher fetchDataByMessage:[CTFetcherMessage new] withCallback:^(CTResponse *aResponse)
     {
         dispatch_async(dispatch_get_main_queue(), ^
                        {
                            [weakSelf didFetchDataWithResponse:aResponse];
                        });
     }];
}

- (void)didFetchDataWithResponse:(CTResponse *)response
{
    [self hideActivity];
    if (response.success)
    {
        guiges = [response.dataDictionary objectForKey:@"Guides"];
        pricings = [response.dataDictionary objectForKey:@"Pricings"];
        
        [guigesSection removeAllCellData];
        [pricingSection removeAllCellData];
        
        if (showAllGuiges)
        {
            [((CTTableDisposerModeled *)tableDisposer) setupModels:guiges forSection:guigesSection];
        }
        
        [((CTTableDisposerModeled *)tableDisposer) setupModels:pricings forSection:pricingSection];
        
        [tableDisposer reloadData];
        [self calculatePrice];
    }
    else if (response.textMessage)
    {
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

#pragma mark - Button Actions

- (IBAction)sendButtonPressd
{
    NSMutableArray *selectedItems = [NSMutableArray array];
    for (NSInteger i = 0; i < pricingSection.cellDataCount; i++)
    {
        CTPriceGuideCellData *cellData = (CTPriceGuideCellData *)[pricingSection cellDataAtIndex:i];
        if (cellData.selected && [cellData priceData])
        {
            NSString *strData = [NSString stringWithFormat:@"{'id':%@,'quantity':%lu}",
                                 [[cellData priceData] priceIdentifier],
                                 [cellData count]];
            
            [selectedItems addObject:strData];
        }
    }
    
    if (selectedItems.count > 0)
    {
        CTRequest *request = [[CTHotelsGateway sharedInstance] bookGuide:selectedItems];
        [self showActivity];
        CTWeakSelf
        [request addResponseBlock:^(CTResponse *aResponse)
         {
             [weakSelf bookDidEndWithResponse:aResponse];
             
         } responseQueue:dispatch_get_main_queue()];
        
        [request start];
    }
}

- (void)bookDidEndWithResponse:(CTResponse *)response
{
    [self hideActivity];
    if (response.success)
    {
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kOrderScreenStoryBoardId];
        [self.mm_drawerController setCenterViewController:vc];
    }
    else if (response.textMessage)
    {
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

- (IBAction)showAllGuidesButtonPressed
{
    if (showAllGuiges)
    {
        [guigesSection removeAllCellData];
        [guigesSection reloadWithAnimation:UITableViewRowAnimationAutomatic];
        
        [self.showAllGuidesButton setImage:[UIImage imageNamed:@"ic_arrow_down"] forState:UIControlStateNormal];
    }
    else
    {
        [tableDisposer.tableView setContentOffset:CGPointZero animated:YES];
        [((CTTableDisposerModeled *)tableDisposer) setupModels:guiges forSection:guigesSection];
        [guigesSection reloadWithAnimation:UITableViewRowAnimationAutomatic];
        
        [self.showAllGuidesButton setImage:[UIImage imageNamed:@"ic_arrow_up"] forState:UIControlStateNormal];
    }
    
    showAllGuiges = !showAllGuiges;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CTPriceGuideCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[CTPriceGuideCell class]])
    {
        CTPriceGuideCellData *cellData = (CTPriceGuideCellData *)cell.cellData;
        cellData.selected = !cellData.selected;
        
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self calculatePrice];
    }
}

#pragma mark - Storyboard Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(CTGuideCell *)sender
{
    if ([sender isKindOfClass:[CTGuideCell class]])
    {
        CTGuideCellData *cellData = (CTGuideCellData *)sender.cellData;
        ((CTGuideDetailedController *)segue.destinationViewController).guide = [cellData guide];
    }
}

#pragma mark - Calculate Price

- (void)calculatePrice
{
    double price = 0;
    for (NSInteger i = 0; i <pricingSection.cellDataCount; i++)
    {
        CTPriceGuideCellData *cellData = (CTPriceGuideCellData *)[pricingSection cellDataAtIndex:i];
        if (cellData.selected)
        {
            price += cellData.count ? cellData.count * [[[cellData priceData] priceValue] floatValue] : [[[cellData priceData] priceValue] floatValue];
        }
    }
    totalPriceView.lbPriceValue.text = [NSString stringWithFormat:@"%@%0.2f", [CTAppManagerInstance.currentUser currencySymbol], price];
}

#pragma mark - Notification

- (void)didChangePrice:(NSNotification *)notification
{
    [self calculatePrice];
}

@end
