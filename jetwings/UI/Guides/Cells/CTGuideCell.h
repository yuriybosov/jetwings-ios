//
//  CTGuideCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/21/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>
#import "BOGuide.h"

@interface CTGuideCellData : CTCellDataModeled

- (BOGuide *)guide;

@end


@interface CTGuideCell : CTCell

@property (nonatomic, weak) IBOutlet UIImageView *photoView;
@property (nonatomic, weak) IBOutlet UIImageView *photoViewBG;
@property (nonatomic, weak) IBOutlet UILabel *lbName;
@property (nonatomic, weak) IBOutlet UILabel *lbLocation;

@end
