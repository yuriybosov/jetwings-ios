//
//  CTGuideCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/21/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTGuideCell.h"

@implementation CTGuideCellData

- (id)initWithModel:(id)aModel
{
    self = [super initWithModel:aModel];
    if (self)
    {
        self.cellSelectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (BOGuide *)guide
{
    return self.model;
}

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return 72;
}

- (NSString *)cellIdentifier
{
    return @"guideCell";
}

@end


@implementation CTGuideCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.photoView.layer.cornerRadius = self.photoView.frame.size.width/2;
    self.photoViewBG.layer.cornerRadius = self.photoViewBG.frame.size.width/2;
}

- (void)setupCellData:(CTGuideCellData *)aCellData
{
    [super setupCellData:aCellData];
    
    [self.photoView sd_setImageWithURL:[NSURL URLWithString:[aCellData guide].avatar]];
    self.lbName.text = [aCellData guide].name;
    self.lbLocation.text = [aCellData guide].speciality;
}

@end
