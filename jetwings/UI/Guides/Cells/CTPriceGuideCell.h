//
//  CTPriceGuideCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/21/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTPriceCell.h"
#import "BOGuidePricing.h"

@interface CTPriceGuideCellData : CTPriceCellData

@end


@interface CTPriceGuideCell : CTPriceCell

@end
