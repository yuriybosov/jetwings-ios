//
//  CTPriceGuideCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/21/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTPriceGuideCell.h"

@implementation CTPriceGuideCellData

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return self.selected ? 80 : 44;
}

- (NSString *)cellIdentifier
{
    return @"priceGuideCell";
}

- (id<CTPriceCellProtocol>)priceData
{
    if ([self.model conformsToProtocol:@protocol(CTPriceCellProtocol)])
    {
        return self.model;
    }
    return nil;
}

@end


@implementation CTPriceGuideCell

@end
