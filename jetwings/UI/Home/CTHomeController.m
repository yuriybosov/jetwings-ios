//
//  CTHomeController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/12/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTHomeController.h"

@interface CTHomeController ()

@property (nonatomic, weak) IBOutlet UIButton *btnMyQuote;
@property (nonatomic, weak) IBOutlet UIButton *btnHotels;
@property (nonatomic, weak) IBOutlet UIButton *btnRestaurants;
@property (nonatomic, weak) IBOutlet UIButton *btnAttractions;
@property (nonatomic, weak) IBOutlet UIButton *btnTourPackages;
@property (nonatomic, weak) IBOutlet UIButton *btnTransport;
@property (nonatomic, weak) IBOutlet UIButton *btnTourGuides;
@property (nonatomic, weak) IBOutlet UIButton *btnSettings;

@end

@implementation CTHomeController

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    
    self.title = CTLocalizedString(@"Jetwings");
    
    [_btnMyQuote setTitle:CTLocalizedString(@"My Quote") forState:UIControlStateNormal];
    [_btnHotels setTitle:CTLocalizedString(@"Hotels") forState:UIControlStateNormal];
    [_btnRestaurants setTitle:CTLocalizedString(@"Restaurants") forState:UIControlStateNormal];
    [_btnAttractions setTitle:CTLocalizedString(@"Attractions") forState:UIControlStateNormal];
    [_btnTourPackages setTitle:CTLocalizedString(@"Tour Packages") forState:UIControlStateNormal];
    [_btnTransport setTitle:CTLocalizedString(@"Transport") forState:UIControlStateNormal];
    [_btnTourGuides setTitle:CTLocalizedString(@"Tour Guides") forState:UIControlStateNormal];
    [_btnSettings setTitle:CTLocalizedString(@"Settings") forState:UIControlStateNormal];
    
    [UIConfiguratorInstance customizeHomeButton:_btnAttractions];
    [UIConfiguratorInstance customizeHomeButton:_btnHotels];
    [UIConfiguratorInstance customizeHomeButton:_btnMyQuote];
    [UIConfiguratorInstance customizeHomeButton:_btnRestaurants];
    [UIConfiguratorInstance customizeHomeButton:_btnSettings];
    [UIConfiguratorInstance customizeHomeButton:_btnTourGuides];
    [UIConfiguratorInstance customizeHomeButton:_btnTourPackages];
    [UIConfiguratorInstance customizeHomeButton:_btnTransport];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (CTAppManagerInstance.currentUser.firstLogin)
    {
        [self showControllerByStoryBoardId:kWelcomeScreenStoryBoardId];
    }
}

#pragma mark - Actions

- (void)showControllerByStoryBoardId:(NSString *)storyBoardId
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:storyBoardId];
    [self.mm_drawerController setCenterViewController:vc withCloseAnimation:YES completion:nil];
}

- (IBAction)btnMyQuotePressed
{
    [self showControllerByStoryBoardId:kMyQouteScreenStoryBoardId];
}
- (IBAction)btnHotelsPressed
{
    [self showControllerByStoryBoardId:kHotelsScreenStoryBoardId];
}
- (IBAction)btnRestaurantsPressed
{
    [self showControllerByStoryBoardId:kRestaurantsScreenStoryBoardId];
}
- (IBAction)btnAttractionsPressed
{
    [self showControllerByStoryBoardId:kAttractionsScreenStoryBoardId];
}
- (IBAction)btnTourPackagesPressed
{
    [self showControllerByStoryBoardId:kToursScreenStoryBoardId];
}
- (IBAction)btnTransportPressed
{
    [self showControllerByStoryBoardId:kTransportScreenStoryBoardId];
}
- (IBAction)btnTourGuidesPressed
{
    [self showControllerByStoryBoardId:kGuidesScreenStoryBoardId];
}
- (IBAction)btnSettingsPressed
{
    [self showControllerByStoryBoardId:kSettingsScreenStoryBoardId];
}

@end
