//
//  CTWelcomeController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/12/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTWelcomeController.h"

@interface CTWelcomeController ()

@property (nonatomic, weak) IBOutlet UILabel *welcomeTitle;
@property (nonatomic, weak) IBOutlet UILabel *welcomeText;
@property (nonatomic, weak) IBOutlet UIButton *btProfile;
@property (nonatomic, weak) IBOutlet UIButton *btAdd;

@end

@implementation CTWelcomeController

- (void)setupLanguareValues
{
    [super setupLanguareValues];

    self.title = [CTAppManagerInstance.currentUser fullName];
    self.welcomeTitle.text = CTLocalizedString(@"Welcome");
    self.welcomeText.text = CTLocalizedString(@"Hi there, you are a newest member of the Jetwings app, a system to help you create the best travel plans for our customers and increase your sales. You\'ll always be able to share the latest discounts and packages with this new technology.");
    
    [self.btProfile setTitle:CTLocalizedString(@"View Your Profile") forState:UIControlStateNormal];
    [self.btAdd setTitle:[NSString stringWithFormat:@"+  %@", CTLocalizedString(@"New Oder")] forState:UIControlStateNormal];
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance menuButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    if (self.mm_drawerController.openSide != MMDrawerSideLeft)
    {
        [self.mm_drawerController openDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    }
    else
    {
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    }
}

#pragma mark - Buttons Actions

- (IBAction)profileButtonPressed:(id)sender
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kSettingsScreenStoryBoardId];
    [self.mm_drawerController setCenterViewController:vc withCloseAnimation:NO completion:nil];
}

- (IBAction)addButtonPressed:(id)sender
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kOrderScreenStoryBoardId];
    [self.mm_drawerController setCenterViewController:vc withCloseAnimation:NO completion:nil];
}

@end
