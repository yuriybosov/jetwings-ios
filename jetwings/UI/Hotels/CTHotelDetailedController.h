//
//  CTHotelDetailedController.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/14/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseTableController.h"
#import "BOHotel.h"

@interface CTHotelDetailedController : CTBaseTableController

@property (nonatomic, strong) BOHotel *hotel;

@end
