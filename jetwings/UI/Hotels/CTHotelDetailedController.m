//
//  CTHotelDetailedController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/14/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTHotelDetailedController.h"
#import "CTHotelsGateway.h"
#import "CTHotelDetailedPhoroView.h"
#import "NSDate-Utilities.h"
#import "CTHotelInfoCell.h"
#import "CTTotalPriceView.h"
#import "CTHotelPriceCell.h"

@interface CTHotelDetailedController ()
{
    NSDate *fromDate;
    NSDate *toDate;
    NSDateFormatter *dateFormaterShow;
    NSDateFormatter *dateFormaterRequest;
    
    CTPopupDatePicker* pickerDateFrom;
    CTPopupDatePicker* pickerDateTo;
    
    CTSectionReadonly *hotelInfoSection;
    CTSectionReadonly *pricingSection;
    CTTotalPriceView *totalPriceView;
}

@property (nonatomic, weak) IBOutlet CTHotelDetailedPhoroView* phoroView;

@property (nonatomic, weak) IBOutlet UILabel* lbDateFromTitle;
@property (nonatomic, weak) IBOutlet UILabel* lbDateToTitle;
@property (nonatomic, weak) IBOutlet UIButton* btnDateFrom;
@property (nonatomic, weak) IBOutlet UIButton* btnDateTo;

@property (nonatomic, weak) IBOutlet UILabel* lbDetails;

@end

@implementation CTHotelDetailedController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        fromDate = [NSDate date];
        toDate = [NSDate dateTomorrow];
        
        dateFormaterShow = [NSDateFormatter new];
        dateFormaterShow.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        dateFormaterShow.dateFormat = @"dd.MM.yyyy";
        
        dateFormaterRequest = [NSDateFormatter new];
        dateFormaterRequest.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        dateFormaterRequest.dateFormat = @"yyyy-MM-dd";
    }
    return self;
}

- (void)setup
{
    [super setup];
    
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTHotelPriceCellData class]
                                                       forModel:[CTBOHotelsRoom class]];
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTHotelInfoCellData class]
                                                       forModel:[BOHotel class]];
    hotelInfoSection = [CTSectionReadonly section];
    [tableDisposer addSection:hotelInfoSection];
    
    pricingSection = [CTSectionReadonly section];
    [tableDisposer addSection:pricingSection];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangePrice:)
                                                 name:kDidChangeNumberOfPrice object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPickerToolbarItemNotification:)
                                                 name:CT_POPUPVIEW_TOOLBAR_ITEM_DID_PRESSED object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    totalPriceView = [CTTotalPriceView create];
    [self fetchData];
}

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    
    self.title = _hotel.name;
    [self showHotelInfo];
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance backButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Fetch Data

- (void)fetchData
{
    [self showActivity];
    __weak CTHotelDetailedController *weakSelf = self;
 
    CTRequest *request = [[CTHotelsGateway sharedInstance] hotelInfoById:_hotel.identifier
                                                                dateFrom:[dateFormaterRequest stringFromDate:fromDate]
                                                                  dateTo:[dateFormaterRequest stringFromDate:toDate]];
    
    [request addResponseBlock:^(CTResponse *aResponse)
    {
        [weakSelf fetchDataDidFinish:aResponse];
    } responseQueue:dispatch_get_main_queue()];
    
    [request start];
}

- (void)fetchDataDidFinish:(CTResponse *)response
{
    [self hideActivity];
    if (response.success)
    {
        [_hotel setupDetailedDictionary:response.dataDictionary];
        
        [CTMainStorageInstance scheduleBlock:^
        {
            [CTMainStorageInstance save];
        }];
        
        [self showHotelInfo];
    }
    else
    {
        [self showHotelInfo];
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

- (void)showHotelInfo
{
    [self.phoroView setupHotel:_hotel];
    
    self.lbDateFromTitle.text = CTLocalizedString(@"Arrival Day");
    self.lbDateToTitle.text = CTLocalizedString(@"Departure");
    self.lbDetails.attributedText = [_hotel descriptionText];
    
    [self.btnDateFrom setTitle:[dateFormaterShow stringFromDate:fromDate] forState:UIControlStateNormal];
    [self.btnDateTo setTitle:[dateFormaterShow stringFromDate:toDate] forState:UIControlStateNormal];
    
    [self resizeTableViewHeaderForWidth:self.view.frame.size.width];

    [hotelInfoSection removeAllCellData];
    [pricingSection removeAllCellData];
    
    if (_hotel.initialDetailedInfoValue)
        [((CTTableDisposerModeled *)tableDisposer) setupModels:@[_hotel] forSection:hotelInfoSection];
    
    NSArray *hotelsRooms = _hotel.hotelsRoom;
    totalPriceView.lbPriceValue.text = [NSString stringWithFormat:@"%@0.00",[CTAppManagerInstance.currentUser currencySymbol]];
    
    if (hotelsRooms.count)
    {
        pricingSection.headerView = totalPriceView;
        [((CTTableDisposerModeled *)tableDisposer) setupModels:hotelsRooms forSection:pricingSection];
    }
    else
    {
        pricingSection.headerView = nil;
    }
    
    [tableDisposer reloadData];
    [self calculatePrice];

}

#pragma mark - TableView

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CTHotelPriceCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[CTHotelPriceCell class]])
    {
        CTHotelPriceCellData *cellData = (CTHotelPriceCellData *)cell.cellData;
        cellData.selected = !cellData.selected;
        
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self calculatePrice];
    }
}

#pragma mark - Calculate Price

- (void)calculatePrice
{
    double price = 0;
    for (NSInteger i = 0; i < pricingSection.cellDataCount; i++)
    {
        CTHotelPriceCellData *cellData = (CTHotelPriceCellData *)[pricingSection cellDataAtIndex:i];
        if (cellData.selected)
        {
            price += cellData.count ? cellData.count * [[[cellData priceData] priceValue] floatValue] : [[[cellData priceData] priceValue] floatValue];
        }
    }
    totalPriceView.lbPriceValue.text = [NSString stringWithFormat:@"%@%0.2f", [CTAppManagerInstance.currentUser currencySymbol], price];
}

#pragma mark - Notification
- (void)didChangePrice:(NSNotification *)notification
{
    [self calculatePrice];
}

- (void)didPickerToolbarItemNotification:(NSNotification*)aNotification
{
    if (aNotification.object == pickerDateFrom)
    {
        fromDate = pickerDateFrom.popupedPicker.date;
        if (fromDate.timeIntervalSinceNow > toDate.timeIntervalSinceNow )
        {
            toDate = fromDate;
        }
        
        [self.btnDateFrom setTitle:[dateFormaterShow stringFromDate:fromDate] forState:UIControlStateNormal];
        [self.btnDateTo setTitle:[dateFormaterShow stringFromDate:toDate] forState:UIControlStateNormal];
        
        [pickerDateFrom hideWithAnimation:YES];
        [self fetchData];
    }
    else if (aNotification.object == pickerDateTo)
    {
        toDate = pickerDateTo.popupedPicker.date;
        
        [self.btnDateFrom setTitle:[dateFormaterShow stringFromDate:fromDate] forState:UIControlStateNormal];
        [self.btnDateTo setTitle:[dateFormaterShow stringFromDate:toDate] forState:UIControlStateNormal];
        
        [pickerDateTo hideWithAnimation:YES];
        [self fetchData];
    }
}

#pragma mark - Buttons Actions

- (IBAction)fromDateButtonPressed:(id)sender
{
    pickerDateFrom = [[CTPopupDatePicker alloc] init];
    pickerDateFrom.backgroundColor = [UIColor colorWithRedI:246 greenI:247 blueI:250 alphaI:255];
    
    pickerDateFrom.toolbar = [UIConfiguratorInstance defaultPickerToolbarWithTitle:CTLocalizedString(@"Start date")];
    pickerDateFrom.popupedPicker.date = fromDate;
    pickerDateFrom.popupedPicker.minimumDate = [NSDate date];
    pickerDateFrom.popupedPicker.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    
    [pickerDateFrom prepareToShow];
    [pickerDateFrom showWithAnimation:YES inView:self.navigationController.view];
}

- (IBAction)toDateButtonPressed:(id)sender
{
    pickerDateTo = [[CTPopupDatePicker alloc] init];
    pickerDateTo.backgroundColor = [UIColor colorWithRedI:246 greenI:247 blueI:250 alphaI:255];
    
    pickerDateTo.toolbar = [UIConfiguratorInstance defaultPickerToolbarWithTitle:CTLocalizedString(@"End date")];
    pickerDateTo.popupedPicker.date = toDate;
    pickerDateTo.popupedPicker.minimumDate = fromDate;
    pickerDateTo.popupedPicker.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    
    [pickerDateTo prepareToShow];
    [pickerDateTo showWithAnimation:YES inView:self.navigationController.view];
}

- (IBAction)sendButtonPressd
{
    NSMutableArray *selectedItems = [NSMutableArray array];
    for (NSInteger i = 0; i < pricingSection.cellDataCount; i++)
    {
        CTHotelPriceCellData *cellData = (CTHotelPriceCellData *)[pricingSection cellDataAtIndex:i];
        if (cellData.selected && [cellData priceData])
        {
            NSString *strData = [NSString stringWithFormat:@"{'id':%@,'quantity':%lu}",
                                 [[cellData priceData] priceIdentifier],
                                 [cellData count]];
            
            [selectedItems addObject:strData];
        }
    }
    
    if (selectedItems.count > 0)
    {
        CTRequest *request = [[CTHotelsGateway sharedInstance] bookHotel:_hotel.identifier
                                                               startDate:[dateFormaterRequest stringFromDate:fromDate]
                                                                 endDate:[dateFormaterRequest stringFromDate:toDate]
                                                                bookData:selectedItems];
        [self showActivity];
        CTWeakSelf
        [request addResponseBlock:^(CTResponse *aResponse)
         {
             [weakSelf bookDidEndWithResponse:aResponse];
             
         } responseQueue:dispatch_get_main_queue()];
        
        [request start];
    }
}

- (void)bookDidEndWithResponse:(CTResponse *)response
{
    [self hideActivity];
    if (response.success)
    {
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kOrderScreenStoryBoardId];
        [self.mm_drawerController setCenterViewController:vc];
    }
    else if (response.textMessage)
    {
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

@end
