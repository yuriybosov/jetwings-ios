//
//  CTHotelsController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTHotelsController.h"
#import "CTHotelListFetcher.h"
#import "CTBOHotelFilter.h"
#import "CTHotelCell.h"
#import "CTHotelDetailedController.h"

@interface CTHotelsController ()
{
    CTBOHotelFilter *filter;
    CTPopupSimplePicker* pickerFilter;
}

@end

@implementation CTHotelsController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        filter = [CTBOHotelFilter new];
    }
    return self;
}

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    self.title = CTLocalizedString(@"Hotels");
}

- (BOOL)needShowChageAraeButton
{
    return YES;
}

#pragma mark - Configure Table Disposer

- (void)setup
{
    [super setup];
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTHotelCellData class]
                                                       forModel:[BOHotel class]];
    moduleList.dataFetcher = [[CTHotelListFetcher alloc] initWithStorage:CTMainStorageInstance];
}

#pragma mark - CTModuleListDelegate

- (CTFetcherMessage*)fetcherMessageForModuleList:(CTModuleList*)aModule
{
    CTFetcherMessage *message = [CTFetcherMessage new];
    [message.defaultParameters setObject:filter forKey:kFetcherMessageFilter];
    return message;
}

#pragma mark - Right Navbar Button

- (IBAction)filterButtonPressd:(id)sender
{
    pickerFilter = [[CTPopupSimplePicker alloc] init];
    pickerFilter.backgroundColor = [UIColor colorWithRedI:246 greenI:247 blueI:250 alphaI:255];
    
    pickerFilter.dataSource = @[[[CTTitledID alloc] initWithID:@(CTHotelSortedTypePriceAsc) title:CTLocalizedString(@"Price: from Low to High")],
                                [[CTTitledID alloc] initWithID:@(CTHotelSortedTypePriceDesc) title:CTLocalizedString(@"Price: from High to Low")],
                                [[CTTitledID alloc] initWithID:@(CTHotelSortedTypeStarAsc) title:CTLocalizedString(@"Stars: from Low to High")],
                                [[CTTitledID alloc] initWithID:@(CTHotelSortedTypeStarDesc) title:CTLocalizedString(@"Stars: from High to Low")]];
    
    pickerFilter.toolbar = [UIConfiguratorInstance defaultPickerToolbarWithTitle:CTLocalizedString(@"Sort")];
    
    CTTitledID *titleID = [pickerFilter.dataSource titledIDByID:@(filter.sortedType)];
    
    [pickerFilter prepareToShow];
    [pickerFilter setSelectedItem: titleID];
    [pickerFilter showWithAnimation:YES inView:self.navigationController.view];
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance menuButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    if (self.mm_drawerController.openSide != MMDrawerSideLeft)
    {
        [self.mm_drawerController openDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    }
    else
    {
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    }
}

#pragma mark - Notification

- (void)didPickerToolbarItemNotification:(NSNotification*)aNotification
{
    if (aNotification.object == pickerFilter)
    {
        [pickerFilter hideWithAnimation:YES];
        CTTitledID *filterValue = pickerFilter.selectedItem;
        
        if ([filterValue.ID integerValue] == filter.sortedType)
            return;
        
        filter.sortedType = [filterValue.ID integerValue];
        [self fetchData];
    }
    else if (aNotification.object == pickerArea)
    {
        [super didPickerToolbarItemNotification:aNotification];
    }
}

#pragma mark - Storyboard Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(CTHotelCell *)sender
{
    if  ([sender isKindOfClass:[CTHotelCell class]])
    {
        CTHotelCellData *cellData = (CTHotelCellData *)[tableDisposer cellDataByIndexPath:[tableDisposer.tableView indexPathForCell:sender]];
        ((CTHotelDetailedController *)segue.destinationViewController).hotel = [cellData hotel];
    }
}

@end
