//
//  CTHotelCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>
#import "BOHotel.h"

@interface CTHotelCellData : CTCellDataModeled

- (BOHotel *)hotel;

@end


@interface CTHotelCell : CTCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@property (nonatomic, weak) IBOutlet UIImageView *hotelPicture;
@property (nonatomic, weak) IBOutlet UILabel *hotelName;
@property (nonatomic, weak) IBOutlet UILabel *hotelPrice;

@end
