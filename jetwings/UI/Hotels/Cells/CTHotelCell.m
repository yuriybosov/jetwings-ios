//
//  CTHotelCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTHotelCell.h"
#import "UIImageView+WebCache.h"

@implementation CTHotelCellData

- (BOHotel *)hotel
{
    return self.model;
}

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return 160;
}

- (NSString *)cellIdentifier
{
    return @"hotelCell";
}

@end


@implementation CTHotelCell

- (void)setupCellData:(CTHotelCellData *)aCellData
{
    [super setupCellData:aCellData];
    
    [self.hotelPicture sd_setImageWithURL:[NSURL URLWithString:aCellData.hotel.picture]];
    self.hotelName.text = [aCellData.hotel.name uppercaseString];
    if ([aCellData.hotel.price integerValue] > 0)
    {
        self.hotelPrice.text = [NSString stringWithFormat:@"1 %@ %@%.2f",
                                CTLocalizedString(@"room"),
                                CTAppManagerInstance.currentUser.currencySymbol,
                                [aCellData.hotel.price floatValue]];
    }
    else
    {
        self.hotelPrice.text = nil;
    }
    
    [self.collectionView reloadData];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [((CTHotelCellData *)self.cellData).hotel.stars integerValue];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellStar" forIndexPath:indexPath];
    UIImageView *imageView = [cell viewWithTag:10];
    imageView.image = [UIImage imageNamed:@"ic_star_fill"];
    
    return cell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    // Add inset to the collection view if there are not enough cells to fill the width.
    CGFloat cellSpacing = ((UICollectionViewFlowLayout *) collectionViewLayout).minimumLineSpacing;
    CGFloat cellWidth = ((UICollectionViewFlowLayout *) collectionViewLayout).itemSize.width;
    NSInteger cellCount = [collectionView numberOfItemsInSection:section];
    CGFloat inset = (collectionView.bounds.size.width - (cellCount * (cellWidth + cellSpacing))) * 0.5;
    inset = MAX(inset, 0.0);
    return UIEdgeInsetsMake(0.0, inset, 0.0, 0.0);
}

@end
