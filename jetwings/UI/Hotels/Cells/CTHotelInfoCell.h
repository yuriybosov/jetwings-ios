//
//  CTHotelAmenitiesCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>
#import "BOHotel.h"

@interface CTHotelInfoCellData : CTCellDataModeled

- (BOHotel *)hotel;

@end


@interface CTHotelInfoCell : CTCell <UICollectionViewDataSource>

@property (nonatomic,weak) IBOutlet UICollectionView *amenitiesCollectionView;
@property (nonatomic,weak) IBOutlet UIView *amenitiesView;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *amenitiesCollectionViewHeight;

@property (nonatomic,weak) IBOutlet UILabel *lbDistrict;
@property (nonatomic,weak) IBOutlet UIView *districtView;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *lbDistrictHeight;

@property (nonatomic,weak) IBOutlet UILabel *lbAddress;
@property (nonatomic,weak) IBOutlet UIView *addressView;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *lbAddressHeight;

@property (nonatomic,weak) IBOutlet UILabel *lbNumberOfRooms;
@property (nonatomic,weak) IBOutlet UIView *numberOfRoomsView;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *lbNumberOfRoomsHeight;


@end
