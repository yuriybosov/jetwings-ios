//
//  CTHotelAmenitiesCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTHotelInfoCell.h"

@implementation CTHotelInfoCellData

- (id)initWithModel:(id)aModel
{
    self = [super initWithModel:aModel];
    if (self)
    {
        self.cellSelectionStyle = UITableViewCellSelectionStyleNone;
        self.cellHeightAutomaticDimension = YES;
    }
    return self;
}

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    CGFloat result = 0;
    
    if ([[self hotel].amenitiesType count])
        result += 50;
    
    if ([self hotel].district)
        result += 50;
    
    if ([self hotel].address)
        result += 50;
    
    if ([[self hotel].rooms integerValue] > 0)
        result += 50;
    
    return result + 1;
}

- (NSString *)cellIdentifier
{
    return @"infoCell";
}

- (BOHotel *)hotel
{
    return self.model;
}

@end


@implementation CTHotelInfoCell

- (IBAction)addressButtonPressed:(id)sender
{
    if (fabs(((CTHotelInfoCellData *)self.cellData).hotel.latitudeValue) > 0.01 &&
        fabs(((CTHotelInfoCellData *)self.cellData).hotel.longitudeValue) > 0.01)
    {
        NSString *mapString = [NSString stringWithFormat:@"http://maps.apple.com/?ll=%@,%@",
                               ((CTHotelInfoCellData *)self.cellData).hotel.latitude,
                               ((CTHotelInfoCellData *)self.cellData).hotel.longitude];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mapString]];
    }
}

- (void)setupCellData:(CTHotelInfoCellData *)aCellData
{
    [super setupCellData:aCellData];
    
    if ([[aCellData hotel].amenitiesType count] == 0)
    {
        self.amenitiesCollectionViewHeight.constant = 0;
        self.amenitiesView.hidden = YES;
    }
    else
    {
        self.amenitiesCollectionViewHeight.constant = 50;
        self.amenitiesView.hidden = NO;
    }
    
    if (![aCellData hotel].district)
    {
        self.lbDistrictHeight.constant = 0;
        self.districtView.hidden = YES;
    }
    else
    {
        self.lbDistrictHeight.constant = 50;
        self.districtView.hidden = NO;
    }
    
    if (![aCellData hotel].address)
    {
        self.lbAddressHeight.constant = 0;
        self.addressView.hidden = YES;
    }
    else
    {
        self.lbAddressHeight.constant = 50;
        self.addressView.hidden = NO;
    }
    
    if ([[aCellData hotel].rooms integerValue] == 0)
    {
        self.lbNumberOfRoomsHeight.constant = 0;
        self.numberOfRoomsView.hidden = YES;
    }
    else
    {
        self.lbNumberOfRoomsHeight.constant = 50;
        self.numberOfRoomsView.hidden = NO;
    }

    [self.amenitiesCollectionView reloadData];
    self.lbDistrict.text = [aCellData hotel].district;
    self.lbAddress.text = [aCellData hotel].address;
    self.lbNumberOfRooms.text = [NSString stringWithFormat:@"%@: %@", CTLocalizedString(@"Number of rooms"), [aCellData hotel].rooms];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [((CTHotelInfoCellData *)self.cellData).hotel.amenitiesType count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CTBOAmenitiesType *type = [((CTHotelInfoCellData *)self.cellData).hotel.amenitiesType objectAtIndex:indexPath.row];
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collectionViewCell" forIndexPath:indexPath];
    
    UIImageView *imageView = [cell viewWithTag:10];
    imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_hotel_amenitieslist_%@",type.ID]];
    
    return cell;
}

@end
