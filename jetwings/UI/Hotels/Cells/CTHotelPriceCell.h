//
//  CTHotelPriceCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/20/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTPriceCell.h"
#import "CTBOHotelsRoom.h"

@interface CTHotelPriceCellData : CTPriceCellData

- (CTBOHotelsRoom *)hotelsRoom;

@end


@interface CTHotelPriceCell : CTPriceCell

@property (nonatomic, weak) IBOutlet UILabel *lbAllDates;
@property (nonatomic, weak) IBOutlet UILabel *lbAllWeekDays;
@property (nonatomic, weak) IBOutlet UILabel *lbAllPrices;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *stepperHeight;

@end
