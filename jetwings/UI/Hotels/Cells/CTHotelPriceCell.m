//
//  CTHotelPriceCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/20/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTHotelPriceCell.h"

@implementation CTHotelPriceCellData

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    static CTHotelPriceCell *cell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      cell = (CTHotelPriceCell *)[self.section cellForIndex:[self.section indexByCellData:self]];
                  });

    [cell setupCellData:self];
    
    CGSize size = [cell systemLayoutSizeFittingSize:CGSizeMake(aWidth, 0) withHorizontalFittingPriority:UILayoutPriorityRequired verticalFittingPriority:UILayoutPriorityDefaultLow];
    
    return size.height;
}

- (NSString *)cellIdentifier
{
    return @"hotelPriceCell";
}

- (CTBOHotelsRoom *)hotelsRoom
{
    return self.model;
}

@end


@implementation CTHotelPriceCell

- (void)setupCellData:(CTHotelPriceCellData *)aCellData
{
    [super setupCellData:aCellData];
    
    self.lbAllDates.text = [[aCellData hotelsRoom] allDates];
    self.lbAllWeekDays.text = [[aCellData hotelsRoom] allWeekDays];
    self.lbAllPrices.text = [[aCellData hotelsRoom] allPrices];
    
    self.stepperHeight.constant = !aCellData.selected ? 0 : 30;
    self.lbPriveValue.hidden = !aCellData.selected;
}

@end
