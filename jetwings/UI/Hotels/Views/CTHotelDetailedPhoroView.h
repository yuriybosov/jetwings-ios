//
//  CTHotelDetailedPhoroView.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/14/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeView.h"
#import "BOHotel.h"

@interface CTHotelDetailedPhoroView : UIView <UICollectionViewDataSource, UICollectionViewDelegate, SwipeViewDataSource, SwipeViewDelegate>
{
    BOHotel *hotel;
}

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@property (nonatomic, weak) IBOutlet SwipeView *swipeView;
@property (nonatomic, weak) IBOutlet UILabel *hotelName;
@property (nonatomic, weak) IBOutlet UIPageControl *pageControl;

- (void)setupHotel:(BOHotel *)hotel;

@end
