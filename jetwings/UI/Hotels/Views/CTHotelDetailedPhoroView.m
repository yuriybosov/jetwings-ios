//
//  CTHotelDetailedPhoroView.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/14/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTHotelDetailedPhoroView.h"
#import "UIImageView+WebCache.h"

@implementation CTHotelDetailedPhoroView

- (void)setupHotel:(BOHotel *)aHotel
{
    hotel = aHotel;
    self.hotelName.text = [hotel.name uppercaseString];
    [self.collectionView reloadData];
    
    self.pageControl.numberOfPages = [hotel.photos count];
    
    [self.swipeView reloadData];
    [self.collectionView reloadData];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.pageControl.userInteractionEnabled = NO;
}

#pragma mark - SwipeViewDataSource

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    return [hotel.photos count];
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIImageView *)view
{
    self.pageControl.currentPage = swipeView.currentItemIndex;
    if (!view)
    {
        view = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, swipeView.frame.size.width, swipeView.frame.size.height)];
        view.clipsToBounds = YES;
        view.contentMode = UIViewContentModeScaleAspectFill;
        view.userInteractionEnabled = YES;
    }
    
    CTBOPhotoInfo *photoInfo = [hotel.photos objectAtIndex:index];
    NSURL *selectedImageURL = [NSURL URLWithString:photoInfo.photoPath];
    [view sd_setImageWithURL:selectedImageURL
            placeholderImage:nil
                     options:SDWebImageRetryFailed];

    return view;
}

#pragma mark - SwipeViewDelegate

- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView
{
    self.pageControl.currentPage = swipeView.currentItemIndex;
}

#pragma mark - UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [hotel.stars integerValue];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellStar" forIndexPath:indexPath];
    UIImageView *imageView = [cell viewWithTag:10];
    imageView.image = [UIImage imageNamed:@"ic_star_fill"];
    
    return cell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    // Add inset to the collection view if there are not enough cells to fill the width.
    CGFloat cellSpacing = ((UICollectionViewFlowLayout *) collectionViewLayout).minimumLineSpacing;
    CGFloat cellWidth = ((UICollectionViewFlowLayout *) collectionViewLayout).itemSize.width;
    NSInteger cellCount = [collectionView numberOfItemsInSection:section];
    CGFloat inset = (collectionView.bounds.size.width - (cellCount * (cellWidth + cellSpacing))) * 0.5;
    inset = MAX(inset, 0.0);
    return UIEdgeInsetsMake(0.0, inset, 0.0, 0.0);
}

@end
