//
//  CTLoginController.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/5/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CTBaseController.h"

@interface CTLoginController : CTBaseController <UITextFieldDelegate>
{
    CTValidationGroup *validationGroup;
}

@end
