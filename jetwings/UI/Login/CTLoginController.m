//
//  CTLoginController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/5/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTLoginController.h"
#import "CTLoginTextField.h"
#import "CTUserGateway.h"


static NSString *const mail = @"jervine@jetwingsgroup.com";

@interface CTLoginController ()

@property (nonatomic, weak) IBOutlet CTKeyboardAvoidingScrollView *scrollView;

@property (nonatomic, weak) IBOutlet CTLoginTextField *tfLogin;
@property (nonatomic, weak) IBOutlet CTLoginTextField *tfPassword;

@property (nonatomic, weak) IBOutlet UIButton *btnLogin;
@property (nonatomic, weak) IBOutlet UIButton *btnAgree;

@end


@implementation CTLoginController

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    
    self.tfLogin.placeholder = CTLocalizedString(@"User Name");
    self.tfPassword.placeholder = CTLocalizedString(@"Password");
    
    NSString *agreeText = [NSString stringWithFormat:CTLocalizedString(@"No account? Request access by emailing"),mail];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:agreeText attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [attributedString setAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRedI:57 greenI:169 blueI:255 alphaI:255],
                                      NSUnderlineStyleAttributeName:@(1),
                                      NSUnderlineColorAttributeName:[UIColor colorWithRedI:57 greenI:169 blueI:255 alphaI:255]}
                              range:[agreeText rangeOfString:mail]];
    
    [self.btnLogin setTitle:CTLocalizedString(@"Login") forState:UIControlStateNormal];
    [self.btnAgree setAttributedTitle:attributedString forState:UIControlStateNormal];
    
    self.btnAgree.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnAgree.titleLabel.textAlignment = NSTextAlignmentCenter; // if you want to
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    
    [self.btnLogin setBackgroundImage:[[UIImage imageNamed:@"btnLogin"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)] forState:UIControlStateNormal];
    self.btnLogin.enabled = NO;
    
    self.tfLogin.ctdelegate = self;
    self.tfPassword.ctdelegate = self;
    
    self.tfLogin.validator = [[CTValidatorEmail alloc] init];
    self.tfLogin.validator.errorMessage = CTLocalizedString(@"Email is required");
    
    self.tfPassword.validator = [[CTValidatorNotEmpty alloc] init];
    self.tfPassword.validator.errorMessage = CTLocalizedString(@"Password is required");
    
    validationGroup = [[CTValidationGroup alloc] initWithValidators:@[self.tfLogin.validator,self.tfPassword.validator]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChangeValue) name:UITextFieldTextDidChangeNotification object:self.tfLogin];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChangeValue) name:UITextFieldTextDidChangeNotification object:self.tfPassword];
    
    [self.scrollView addObjectForKeyboard:self.tfLogin];
    [self.scrollView addObjectForKeyboard:self.tfPassword];
    
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.btnAgree.frame.origin.y + self.btnAgree.frame.size.height);
}

- (IBAction)loginButtonPressed:(id)sender
{
    [self.scrollView hideKeyBoard];
    
    CTRequest *request = [[CTUserGateway sharedInstance] loginUserWithLogin:self.tfLogin.text password:self.tfPassword.text];
    [self showActivity];
    
    __weak CTLoginController *weakSelf = self;
    [request addResponseBlock:^(CTResponse *aResponse)
    {
        [weakSelf hideActivity];
        if (aResponse.success)
        {
            [CTAppManagerInstance switchToHomeScreen];
        }
        else
        {
            CTShowSimpleAlert(CTLocalizedString(@"Error"), aResponse.textMessage);
        }
    } responseQueue:dispatch_get_main_queue()];
    
    [request start];
}

- (IBAction)agreeButtonPressed:(id)sender
{
    [self.scrollView hideKeyBoard];
    
    UIActivityViewController* avc = [[UIActivityViewController alloc] initWithActivityItems:@[mail]
                                                                      applicationActivities:nil];
    [self presentViewController:avc animated:YES completion:nil];
}

#pragma mark - Notification

- (void)textFieldDidChangeValue
{
    self.btnLogin.enabled = ([validationGroup validate].count == 0);
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.btnLogin.enabled = ([validationGroup validate].count == 0);
}

@end
