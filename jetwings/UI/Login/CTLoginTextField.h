//
//  CTLoginTextField.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/5/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>


typedef enum
{
    CTLoginTextFieldNormal,
    CTLoginTextFieldEditing,
    CTLoginTextFieldError

}CTLoginTextFieldState;

@interface CTLoginTextField : CTTextField

@property (nonatomic, strong) UIView *separator;
@property (nonatomic, assign) CTLoginTextFieldState textFieldState;

@property (nonatomic, strong) UIColor *normalColor;
@property (nonatomic, strong) UIColor *editingColor;
@property (nonatomic, strong) UIColor *errorColor;

@property (nonatomic, assign) BOOL hideRightButton;

@end
