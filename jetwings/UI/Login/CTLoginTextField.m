//
//  CTLoginTextField.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/5/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTLoginTextField.h"

@implementation CTLoginTextField

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:@"ic_error"] forState:UIControlStateNormal];
        [button setFrame:CGRectMake(0.0f, 0.0f, 15.0f, 15.0f)]; // Required for iOS7
        [button addTarget:self action:@selector(clearButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        self.rightView = button;
        self.rightViewMode = UITextFieldViewModeNever;
        
        self.separator = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                  self.frame.size.height - 1,
                                                                  self.frame.size.width,
                                                                  1)];
        self.separator.translatesAutoresizingMaskIntoConstraints = YES;
        self.separator.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self addSubview: self.separator];
        
        self.textFieldState = CTLoginTextFieldNormal;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChangeValue) name:UITextFieldTextDidChangeNotification object:self];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidBeginEditing) name:UITextFieldTextDidBeginEditingNotification object:self];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidEndEditing) name:UITextFieldTextDidEndEditingNotification object:self];
        
        
        self.errorColor = [UIColor colorWithRedI:228 greenI:89 blueI:89 alphaI:255];
        self.editingColor = [UIColor colorWithRedI:247 greenI:186 blueI:57 alphaI:255];
        self.normalColor = [UIColor colorWithRedI:177 greenI:181 blueI:195 alphaI:255];
    }
    return self;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setPlaceholder:(NSString *)placeholder
{
    if (placeholder)
    {
        NSAttributedString *str = [[NSAttributedString alloc] initWithString:placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRedI:177 greenI:181 blueI:195 alphaI:255] }];
        self.attributedPlaceholder = str;
    }
    else
    {
        [super setPlaceholder:placeholder];
    }
}

- (void)clearButtonPressed
{
    self.text = nil;
}

#pragma mark - Change State

- (void)setTextFieldState:(CTLoginTextFieldState)textFieldState
{
    _textFieldState = textFieldState;
    if (_textFieldState == CTLoginTextFieldError)
    {
        self.separator.backgroundColor = self.errorColor;//[UIColor colorWithRedI:228 greenI:89 blueI:89 alphaI:255];
        self.rightViewMode = UITextFieldViewModeAlways;
    }
    else if (_textFieldState == CTLoginTextFieldEditing)
    {
        self.separator.backgroundColor = self.editingColor;//[UIColor colorWithRedI:247 greenI:186 blueI:57 alphaI:255];
        self.rightViewMode = UITextFieldViewModeNever;
    }
    else
    {
        self.separator.backgroundColor = self.normalColor;//[UIColor colorWithRedI:177 greenI:181 blueI:195 alphaI:255];
        self.rightViewMode = UITextFieldViewModeNever;
    }

    if (self.hideRightButton)
    {
        self.rightViewMode = UITextFieldViewModeNever;
    }
}

#pragma mark - Notification

- (void)textFieldDidChangeValue
{
    if ([self.validator validate])
    {
        self.textFieldState = CTLoginTextFieldEditing;
    }
    else
    {
        self.textFieldState = CTLoginTextFieldError;
    }
}

- (void)textFieldDidBeginEditing
{
    if  (self.text)
    {
        if ([self.validator validate])
        {
            self.textFieldState = CTLoginTextFieldEditing;
        }
        else
        {
            self.textFieldState = CTLoginTextFieldError;
        }
    }
    else
    {
         self.textFieldState = CTLoginTextFieldEditing;
    }
}

- (void)textFieldDidEndEditing
{
    if ([self.validator validate])
    {
        self.textFieldState = CTLoginTextFieldNormal;
    }
    else
    {
        self.textFieldState = CTLoginTextFieldError;
    }
}

@end
