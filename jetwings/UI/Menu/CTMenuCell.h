//
//  CTMenuCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>
#import "CTCellDataMaped.h"

@interface CTMenuCellData : CTCellDataMaped

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, strong) NSString *storyBoardId;

- (id)initWithTitle:(NSString *)title imageName:(NSString *)imageName storyBoardId:(NSString *)storyBoardId;

@end


@interface CTMenuCell : CTCell

@property (nonatomic, weak) IBOutlet UIImageView *menuIcon;
@property (nonatomic, weak) IBOutlet UILabel *menuTitle;

@end


@interface CTMenuIndentCellData : CTCellDataMaped

- (id)init;

@end