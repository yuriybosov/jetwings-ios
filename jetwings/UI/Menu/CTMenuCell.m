//
//  CTMenuCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTMenuCell.h"

@implementation CTMenuCellData

- (id)initWithTitle:(NSString *)title imageName:(NSString *)imageName storyBoardId:(NSString *)storyBoardId
{
    self = [super initWithObject:nil key:nil];
    if (self)
    {
        self.title = title;
        self.imageName = imageName;
        self.storyBoardId = storyBoardId;
    }
    return self;
}

- (NSString *)cellIdentifier
{
    return @"menuCell";
}

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return 52;
}

@end


@implementation CTMenuCell

- (void)setupCellData:(CTMenuCellData *)aCellData
{
    [super setupCellData:aCellData];
    self.menuIcon.image = [UIImage imageNamed:aCellData.imageName];
    self.menuTitle.text = aCellData.title;
}

@end



@implementation CTMenuIndentCellData

- (instancetype)init
{
    self = [super initWithObject:nil key:nil];
    if (self)
    {
        self.cellSelectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (NSString *)cellIdentifier
{
    return @"indentSpase";
}

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return 44;
}

@end
