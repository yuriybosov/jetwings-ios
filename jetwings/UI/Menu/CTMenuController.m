//
//  CTMenuController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/12/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTMenuController.h"
#import "CTMenuCell.h"

@interface CTMenuController ()

- (void)reloadData;

@end


@implementation CTMenuController

- (Class)tableDisposerClass
{
    return [CTTableDisposerMapped class];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self reloadData];
}

- (void)reloadData
{
    [self.tableDisposer removeAllSections];
    
    CTSectionReadonly *section = [CTSectionReadonly section];
    [((CTTableDisposerMapped *)self.tableDisposer) addSection:section];
    
//    CTMenuCellData *homeCellData = [[CTMenuCellData alloc] initWithTitle:CTLocalizedString(@"Home")
//                                                               imageName:nil
//                                                            storyBoardId:kHomeScreenStoryBoardId];
//    [section addCellData:homeCellData];
    
    
    CTMenuCellData *cellData = [[CTMenuCellData alloc] initWithTitle:CTLocalizedString(@"My Quote")
                                                           imageName:@"ic_myquote"
                                                        storyBoardId:kMyQouteScreenStoryBoardId];
    [section addCellData:cellData];
    

    cellData = [[CTMenuCellData alloc] initWithTitle:CTLocalizedString(@"Hotels")
                                           imageName:@"ic_hotel"
                                        storyBoardId:kHotelsScreenStoryBoardId];
    [section addCellData:cellData];
    
    
    cellData = [[CTMenuCellData alloc] initWithTitle:CTLocalizedString(@"Attractions")
                                           imageName:@"ic_attraction"
                                        storyBoardId:kAttractionsScreenStoryBoardId];
    [section addCellData:cellData];
    
    
    cellData = [[CTMenuCellData alloc] initWithTitle:CTLocalizedString(@"Tour Packages")
                                           imageName:@"ic_package"
                                        storyBoardId:kToursScreenStoryBoardId];
    [section addCellData:cellData];
    
    
    cellData = [[CTMenuCellData alloc] initWithTitle:CTLocalizedString(@"Restaurants")
                                           imageName:@"ic_rest"
                                        storyBoardId:kRestaurantsScreenStoryBoardId];
    [section addCellData:cellData];
    
    
    cellData = [[CTMenuCellData alloc] initWithTitle:CTLocalizedString(@"Transport")
                                           imageName:@"ic_transport"
                                        storyBoardId:kTransportScreenStoryBoardId];
    [section addCellData:cellData];
    
    cellData = [[CTMenuCellData alloc] initWithTitle:CTLocalizedString(@"Tour Guides")
                                           imageName:@"ic_guide"
                                        storyBoardId:kGuidesScreenStoryBoardId];
    [section addCellData:cellData];
    
    CTMenuIndentCellData *indentCellData = [CTMenuIndentCellData new];
    [section addCellData:indentCellData];
    
    cellData = [[CTMenuCellData alloc] initWithTitle:CTLocalizedString(@"Settings")
                                           imageName:@"ic_settings"
                                        storyBoardId:kSettingsScreenStoryBoardId];
    [section addCellData:cellData];
    
    
    [self.tableDisposer reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CTMenuCellData *celldData = (CTMenuCellData *)[self.tableDisposer cellDataByIndexPath:indexPath];
    if ([celldData isKindOfClass:[CTMenuCellData class]])
    {
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:celldData.storyBoardId];
        [self.mm_drawerController setCenterViewController:vc withCloseAnimation:YES completion:nil];
    }
}

@end
