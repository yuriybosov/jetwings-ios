
//
//  CTOrderDetailedController.h
//  jetwings
//
//  Created by Yuriy Bosov on 2/4/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseTableController.h"
#import "BOOrder.h"

@interface CTMyOrderDetailedController : CTBaseTableController

@property (nonatomic, strong) BOOrder *order;

@end
