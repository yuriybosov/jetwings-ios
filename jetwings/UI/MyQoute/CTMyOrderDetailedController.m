//
//  CTOrderDetailedController.m
//  jetwings
//
//  Created by Yuriy Bosov on 2/4/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTMyOrderDetailedController.h"
#import "CTHotelsGateway.h"
#import "CTOrdersItemsCell.h"
#import "CTTotalPriceView.h"

@interface CTMyOrderDetailedController ()
{
    CTTotalPriceView* totalPriceView;
}

@property (nonatomic, weak) IBOutlet UILabel *lbTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbDescription;



@end


@implementation CTMyOrderDetailedController

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    self.title = [NSString stringWithFormat:@"#%@", _order.identifier];
    
    // todo - show order info
    self.lbTitle.text = _order.title;
    self.lbDescription.text = [_order.descriptionText stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    [self resizeTableViewHeaderForWidth:self.view.frame.size.width];
}

- (void)setup
{
    [super setup];
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTOrdersItemsCellData class]
                                                       forModel:[CTBOOrdersItem class]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    totalPriceView = [CTTotalPriceView create];
    
    CTRequest *request = [[CTHotelsGateway sharedInstance] orderInfoById:_order.identifier];
    [self showActivity];
    CTWeakSelf
    [request addResponseBlock:^(CTResponse *aResponse)
     {
         [weakSelf didEndRequestWithResponse:aResponse];
     } responseQueue:dispatch_get_main_queue()];
    
    [request start];
}

- (void)didEndRequestWithResponse:(CTResponse *)response
{
    [self hideActivity];
    if (response.success)
    {
        [_order setupDetailedDictionary:response.dataDictionary];
        [CTMainStorageInstance scheduleBlock:^
         {
             [CTMainStorageInstance save];
         }];
        [self setupLanguareValues];
        
        [tableDisposer removeAllSections];
        [tableDisposer addSection:[CTSectionReadonly section]];
        [((CTTableDisposerModeled *)tableDisposer) setupModels:_order.ordersItems forSectionAtIndex:0];
        [tableDisposer reloadData];
        
        tableDisposer.tableView.tableFooterView = totalPriceView;
        totalPriceView.lbPriceValue.text = [NSString stringWithFormat:@"%@%0.2f", [_order currencySymbol], [_order.total floatValue]];
    }
    else if (response.textMessage)
    {
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance backButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    [self.navigationController popViewControllerAnimated:YES];
}



@end
