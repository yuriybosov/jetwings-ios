//
//  CTMyQouteController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTMyOrdersController.h"
#import "CTOrdersFetcher.h"
#import "CTMyOrderCell.h"
#import "CTMyOrderDetailedController.h"

@interface CTMyOrdersController ()

@property (nonatomic, weak) IBOutlet UIView *empryView;
@property (nonatomic, weak) IBOutlet UILabel *empryText;
@property (nonatomic, weak) IBOutlet UIButton *btAdd;

@end

@implementation CTMyOrdersController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.empryView.hidden = YES;
}

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    self.title = CTLocalizedString(@"My Quote");
    self.empryText.text = CTLocalizedString(@"You have not created orders. Press the \"+\" button to create order.");

    [self.btAdd setTitle:[NSString stringWithFormat:@"+  %@", CTLocalizedString(@"New Oder")] forState:UIControlStateNormal];
}

#pragma mark - Setup

- (void)setup
{
    [super setup];
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTMyOrderCellData class]
                                                       forModel:[BOOrder class]];
    moduleList.dataFetcher = [[CTOrdersFetcher alloc] initWithStorage:CTMainStorageInstance];
}

#pragma mark - ModuleList

- (void)moduleList:(CTModuleList *)aModule didReloadDataWithModels:(NSArray *)aModels
{
    self.empryView.hidden = aModels.count > 0;
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance menuButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    if (self.mm_drawerController.openSide != MMDrawerSideLeft)
    {
        [self.mm_drawerController openDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    }
    else
    {
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    }
}

#pragma mark - Buttons Actions

- (IBAction)addButtonPressed:(id)sender
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kOrderScreenStoryBoardId];
    [self.mm_drawerController setCenterViewController:vc withCloseAnimation:NO completion:nil];
}

#pragma mark - Storyboard Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(CTMyOrderCell *)sender
{
    if ([sender isKindOfClass:[CTMyOrderCell class]])
    {
        CTMyOrderCellData *cellData = (CTMyOrderCellData *)[tableDisposer cellDataByIndexPath:[tableDisposer.tableView indexPathForCell:sender]];
        ((CTMyOrderDetailedController *)segue.destinationViewController).order= [cellData order];
    }
}

@end
