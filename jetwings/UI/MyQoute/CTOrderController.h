//
//  CTOrderController.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseTableController.h"
#import "BOOrder.h"
#import "CTOrdersItemsCell.h"


@interface CTOrderController : CTBaseTableController <UITextFieldDelegate, CTOrdersItemsCellProtocol>

@end
