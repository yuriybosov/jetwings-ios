//
//  CTOrderController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTOrderController.h"
#import "CTOrdersItemsCell.h"
#import "CTOrderHeaderView.h"
#import "CTOrderCell.h"
#import "CTHotelsGateway.h"
#import "CTTotalPriceView.h"
#import "CTLoginTextField.h"

@interface CTOrderController ()
{
    BOOrder *currentOrder;
    CTValidationGroup *validationGroup;
}

@property (nonatomic, weak) IBOutlet CTLoginTextField *tfName;
@property (nonatomic, weak) IBOutlet CTTextField *tfDescription;
@property (nonatomic, weak) IBOutlet CTTotalPriceView *totalPriceView;

@end

@implementation CTOrderController

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    self.title = CTLocalizedString(@"New Order");
    
    self.totalPriceView.lbPriceTitle.text = CTLocalizedString(@"Total price");
    self.totalPriceView.lbPriceValue.text = [NSString stringWithFormat:@"%@0.00",[CTAppManagerInstance.currentUser currencySymbol]];
}

- (void)setup
{
    [super setup];
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTOrdersItemsCellData class]
                                                       forModel:[CTBOOrdersItem class]];
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTOrderCellData class]
                                                       forModel:[CTOrder class]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CTWeakSelf
    [self showActivity];
    
    self.tfName.ctdelegate = self;
    self.tfDescription.ctdelegate = self;
    
    self.tfName.editingColor = [UIColor clearColor];
    self.tfName.normalColor = [UIColor clearColor];
    self.tfName.validator = [[CTValidatorNotEmpty alloc] init];
    self.tfName.hideRightButton = YES;
    
    validationGroup = [[CTValidationGroup alloc] initWithValidators:@[self.tfName.validator]];
    
    tableDisposer.tableView.tableHeaderView = nil;
    
    [BOOrder currentOrder:^(NSString *errorMessage, BOOrder *order)
    {
        [weakSelf setCurrentOrder:order errorMessage:errorMessage];
    }];
}

- (void)setCurrentOrder:(BOOrder *)order errorMessage:(NSString *)errorMessage
{
    [self hideActivity];
    if (errorMessage)
    {
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:errorMessage];
    }
    else
    {
        currentOrder = order;
        
        self.tfName.text = currentOrder.title;
        self.tfDescription.text = currentOrder.descriptionText;
        
        self.totalPriceView.lbPriceValue.text = [NSString stringWithFormat:@"%@%0.2f",[CTAppManagerInstance.currentUser currencySymbol], [order.total floatValue]];
        
        [tableDisposer removeAllSections];
        
        if ([order.ordersItems count])
        {
            for (CTBOOrdersItem *item in order.ordersItems)
            {
                item.currencySymbol = [CTAppManagerInstance.currentUser currencySymbol];
            }
            
            CTSectionReadonly *section = [CTSectionReadonly section];
            [tableDisposer addSection:section];
            [((CTTableDisposerModeled *)tableDisposer) setupModels:order.ordersItems forSection:section];
        }
        
        CTSectionReadonly *section = [CTSectionReadonly section];
        [tableDisposer addSection:section];
        
        if (tableDisposer.sectionsCount > 1)
        {
            section.headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 15)];
            section.headerView.backgroundColor = [UIColor colorWithRedI:237 greenI:237 blueI:237 alphaI:255];
        }
        
        CTOrder *orderService = nil;
        
        // hotel
        if (![currentOrder itHasBookingItemByType:OrdetTypeHotel])
        {
            orderService = [[CTOrder alloc] init];
            orderService.storyBoardId = kHotelsScreenStoryBoardId;
            orderService.title = CTLocalizedString(@"Book a hotel");
            orderService.logoName = @"ic_hotel";
            [((CTTableDisposerModeled *)tableDisposer) setupModels:@[orderService] forSection:section];
        }
        
        // attractions
        orderService = [[CTOrder alloc] init];
        orderService.storyBoardId = kAttractionsScreenStoryBoardId;
        orderService.title = CTLocalizedString(@"Book an attraction");
        orderService.logoName = @"ic_attraction";
        [((CTTableDisposerModeled *)tableDisposer) setupModels:@[orderService] forSection:section];
        
        // pack
        if (![currentOrder itHasBookingItemByType:OrdetTypeTourPack])
        {
            orderService = [[CTOrder alloc] init];
            orderService.storyBoardId = kToursScreenStoryBoardId;
            orderService.title = CTLocalizedString(@"Book tour package");
            orderService.logoName = @"ic_package";
            [((CTTableDisposerModeled *)tableDisposer) setupModels:@[orderService] forSection:section];
        }
        
        // rest
        orderService = [[CTOrder alloc] init];
        orderService.storyBoardId = kRestaurantsScreenStoryBoardId;
        orderService.title = CTLocalizedString(@"Book a restaurant");
        orderService.logoName = @"ic_rest";
        [((CTTableDisposerModeled *)tableDisposer) setupModels:@[orderService] forSection:section];
        
        // transport
        if (![currentOrder itHasBookingItemByType:OrdetTypeTransport])
        {
            orderService = [[CTOrder alloc] init];
            orderService.storyBoardId = kTransportScreenStoryBoardId;
            orderService.title = CTLocalizedString(@"Book a transport");
            orderService.logoName = @"ic_transport";
            [((CTTableDisposerModeled *)tableDisposer) setupModels:@[orderService] forSection:section];
        }
        
        // guide
        if (![currentOrder itHasBookingItemByType:OrdetTypeGuide])
        {
            orderService = [[CTOrder alloc] init];
            orderService.storyBoardId = kGuidesScreenStoryBoardId;
            orderService.title = CTLocalizedString(@"Book tour guide");
            orderService.logoName = @"ic_guide";
            [((CTTableDisposerModeled *)tableDisposer) setupModels:@[orderService] forSection:section];
        }
        
        [tableDisposer reloadData];
        
        tableDisposer.tableView.tableHeaderView = self.tableViewHeader;
        [self resizeTableViewHeaderForWidth:self.view.frame.size.width];
    }
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance menuButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    if (self.mm_drawerController.openSide != MMDrawerSideLeft)
    {
        [self.mm_drawerController openDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    }
    else
    {
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    }
}

#pragma mark - Actions

- (IBAction)sendButtonPressed:(id)sender
{
    [self.view endEditing:YES];
    
    if ([validationGroup validate].count != 0)
    {
        if (![self.tfName validate])
        {
            self.tfName.textFieldState = CTLoginTextFieldError;
        }
        return;
    }
    
    if ([[currentOrder ordersItems] count] == 0)
    {
        return;
    }

    [self showActivity];
    CTWeakSelf;
    CTRequest *request = [[CTHotelsGateway sharedInstance] updateCurrentOrderName:self.tfName.text
                                                                 orderDescription:self.tfDescription.text
                                                                         dateFrom:[currentOrder bookingHotel].startDate
                                                                           dateTo:[currentOrder bookingHotel].endDate];
    
    [request addResponseBlock:^(CTResponse *aResponse)
    {
        [weakSelf updateCurrentOrderDidFinish:aResponse];
    } responseQueue:dispatch_get_main_queue()];
    
    [request start];
}

- (void)updateCurrentOrderDidFinish:(CTResponse *)response
{
    if (response.success)
    {
        CTRequest *request = [[CTHotelsGateway sharedInstance] closeCurrentOrder];
        CTWeakSelf
        [request addResponseBlock:^(CTResponse *aResponse)
         {
             [weakSelf closeCurrentOrderDidFinish:aResponse];
         } responseQueue:dispatch_get_main_queue()];
        
        [request start];
    }
    else
    {
        [self hideActivity];
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

- (void)closeCurrentOrderDidFinish:(CTResponse *)response
{
    [self hideActivity];
    if (response.success)
    {
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kMyQouteScreenStoryBoardId];
        [self.mm_drawerController setCenterViewController:vc];
        CTShowSimpleAlert(@"",CTLocalizedString(@"Order Successful"));
    }
    else
    {
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

#pragma mark - TableView

- (void)tableView:(UITableView *)tableView willDisplayCell:(CTOrdersItemsCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[CTOrdersItemsCell class]])
    {
        cell.delegate = self;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CTOrderCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[CTOrderCell class]])
    {
        CTOrderCellData *cellData = (CTOrderCellData *)cell.cellData;
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[cellData order].storyBoardId];
        [self.mm_drawerController setCenterViewController:vc];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.tfName)
    {
        [self.tfDescription becomeFirstResponder];
    }
    else if (textField == self.tfDescription)
    {
        [self.tfDescription resignFirstResponder];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CTRequest *request = [[CTHotelsGateway sharedInstance] updateCurrentOrderName:self.tfName.text
                                                                 orderDescription:self.tfDescription.text
                                                                         dateFrom:[currentOrder bookingHotel].startDate
                                                                           dateTo:[currentOrder bookingHotel].endDate];
    [request addResponseBlock:^(CTResponse *aResponse)
    {
        
    } responseQueue:dispatch_get_main_queue()];
    
    [request start];
}

#pragma mark - CTOrdersItemsCellProtocol

- (void)deleteOrderItem:(CTOrdersItemsCell *)cell item:(CTBOOrdersItem *)item
{
    CTRequest *request = [[CTHotelsGateway sharedInstance] deleteOrderItem:item];
    [self showActivity];
    CTWeakSelf
    [request addResponseBlock:^(CTResponse *aResponse)
     {
         [weakSelf didDeleteOrderItemWithResponse:aResponse];
     } responseQueue:dispatch_get_main_queue()];
    
    [request start];
}

- (void)didDeleteOrderItemWithResponse:(CTResponse *)response
{
    [self hideActivity];
    
    if (response.success)
    {
        [currentOrder setupDetailedDictionary:response.dataDictionary];
        [self setCurrentOrder:currentOrder errorMessage:nil];
    }
    else if (response.textMessage)
    {
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

@end
