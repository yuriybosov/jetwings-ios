//
//  CTOrderCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 2/4/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>
#import "BOOrder.h"

@interface CTMyOrderCellData : CTCellDataModeled

- (BOOrder *)order;

@end


@interface CTMyOrderCell : CTCell

@property (nonatomic, weak) IBOutlet UILabel *lbTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbDescription;
@property (nonatomic, weak) IBOutlet UILabel *lbNumber;

@end
