//
//  CTOrderCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 2/4/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTMyOrderCell.h"

@implementation CTMyOrderCellData

- (BOOrder *)order
{
    return self.model;
}

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return 70;
}

- (NSString *)cellIdentifier
{
    return @"myOrderCell";
}

@end


@implementation CTMyOrderCell

- (void)setupCellData:(CTMyOrderCellData *)aCellData
{
    [super setupCellData:aCellData];
    
    self.lbTitle.text = [aCellData order].title;
    self.lbDescription.text = [aCellData order].descriptionText;
    self.lbNumber.text = [NSString stringWithFormat:@"#%@",[aCellData order].identifier];
}

@end
