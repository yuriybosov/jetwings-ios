//
//  CTOrderCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 2/5/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>

@interface CTOrder : NSObject

@property (nonatomic, strong) NSString *storyBoardId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *logoName;

@end

@interface CTOrderCellData : CTCellDataModeled

- (CTOrder *)order;

@end


@interface CTOrderCell : CTCell

@property (nonatomic, weak) IBOutlet UILabel *lbTitle;
@property (nonatomic, weak) IBOutlet UIImageView *ivLogo;

@end
