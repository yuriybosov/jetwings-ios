//
//  CTOrderCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 2/5/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTOrderCell.h"

@implementation CTOrder

@end


@implementation CTOrderCellData

//- (id)initWithModel:(id)aModel
//{
//    self = [super initWithModel:aModel];
//    if (self)
//    {
////        self.cellAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
//    }
//    return self;
//}

- (CTOrder *)order
{
    return self.model;
}

- (NSString *)cellIdentifier
{
    return @"orderCell";
}

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return 60;
}

@end


@implementation CTOrderCell

- (void)setupCellData:(CTOrderCellData *)aCellData
{
    [super setupCellData:aCellData];
    
    self.lbTitle.text = [aCellData order].title;
    self.ivLogo.image = [UIImage imageNamed:[aCellData order].logoName];
}

@end
