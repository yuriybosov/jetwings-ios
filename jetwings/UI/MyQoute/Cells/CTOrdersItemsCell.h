//
//  CTOrdersItemsCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 2/4/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>
#import "CTBOOrdersItem.h"

@class CTOrdersItemsCell;

@protocol CTOrdersItemsCellProtocol <NSObject>

- (void)deleteOrderItem:(CTOrdersItemsCell *)cell item:(CTBOOrdersItem *)item;

@end

@interface CTOrdersItemsCellData : CTCellDataModeled

- (CTBOOrdersItem *)ordersItem;

@end


@interface CTOrdersItemsCell : CTCell

@property (nonatomic, weak) IBOutlet UILabel *lbName;
@property (nonatomic, weak) IBOutlet UILabel *lbDescr;
@property (nonatomic, weak) IBOutlet UIImageView *ivLogo;

@property (nonatomic, weak) id<CTOrdersItemsCellProtocol> delegate;

@end
