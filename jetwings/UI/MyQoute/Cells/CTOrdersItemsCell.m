//
//  CTOrdersItemsCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 2/4/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTOrdersItemsCell.h"

@implementation CTOrdersItemsCellData

- (id)initWithModel:(id)aModel
{
    self = [super initWithModel:aModel];
    if  (self)
    {
        self.cellSelectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (CTBOOrdersItem *)ordersItem
{
    return self.model;
}

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return 70;
}

- (NSString *)cellIdentifier
{
    return @"ordersItemsCell";
}
@end


@implementation CTOrdersItemsCell

- (void)setupCellData:(CTOrdersItemsCellData *)aCellData
{
    [super setupCellData:aCellData];
    
    self.lbName.text = [[aCellData ordersItem] nameText];
    self.lbDescr.text = [[aCellData ordersItem] descriptionText];
    self.ivLogo.image = [[aCellData ordersItem] logo];
}

- (IBAction)deleteButtonPressed:(id)sender
{
    CTOrdersItemsCellData *cellData = (CTOrdersItemsCellData *)self.cellData;
    [self.delegate deleteOrderItem:self item:[cellData ordersItem]];
}

@end
