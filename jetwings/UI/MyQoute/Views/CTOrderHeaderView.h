//
//  CTOrderHeaderView.h
//  jetwings
//
//  Created by Yuriy Bosov on 2/4/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseController.h"

@interface CTOrderHeaderView : UIView

@property (nonatomic, weak) IBOutlet CTTextField *tfName;
@property (nonatomic, weak) IBOutlet CTTextField *tfDescription;

@end
