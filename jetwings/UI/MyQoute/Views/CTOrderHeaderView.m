//
//  CTOrderHeaderView.m
//  jetwings
//
//  Created by Yuriy Bosov on 2/4/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTOrderHeaderView.h"

@implementation CTOrderHeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.tfName.placeholder = CTLocalizedString(@"Order name");
    self.tfDescription.placeholder = CTLocalizedString(@"Description");
    
    self.tfName.validator = [[CTValidatorNotEmpty alloc] init];
    self.tfName.validator.errorMessage = CTLocalizedString(@"Name is not empty");
    
    self.tfDescription.validator = [[CTValidatorNotEmpty alloc] init];
    self.tfDescription.validator.errorMessage = CTLocalizedString(@"Description is not empty");
}

@end
