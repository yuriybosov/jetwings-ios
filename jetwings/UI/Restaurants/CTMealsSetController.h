//
//  CTMealsSetController.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/19/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseController.h"
#import "CTBOMealsSet.h"

@interface CTMealsSetController : CTBaseController

@property (nonatomic, strong) CTBOMealsSet *mealsSet;

@end
