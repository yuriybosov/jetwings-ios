//
//  CTMealsSetController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/19/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTMealsSetController.h"

@interface CTMealsSetController ()

@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UILabel *lbTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbText;

@end

@implementation CTMealsSetController

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    self.title = _mealsSet.name;
    
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:_mealsSet.picture]];
    self.lbTitle.text = CTLocalizedString(@"Includes:");
    self.lbText.text = [_mealsSet menuSetsText];
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance backButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
