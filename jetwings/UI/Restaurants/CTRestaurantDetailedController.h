//
//  CTRestaurantDetailedController.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseTableController.h"
#import "BORestaurant.h"
#import "SwipeView.h"
#import "CTRestaurantPriceCell.h"

@interface CTRestaurantDetailedController : CTBaseTableController <SwipeViewDataSource, SwipeViewDelegate, CTRestaurantPriceCellProtocol>

@property (nonatomic, strong) BORestaurant *restaurant;

@end
