//
//  CTRestaurantDetailedController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTRestaurantDetailedController.h"
#import "CTHotelsGateway.h"
#import "CTTotalPriceView.h"
#import "CTRestaurantPriceCell.h"
#import "CTMealsSetController.h"

@interface CTRestaurantDetailedController ()
{
    CTSectionReadonly *pricingSection;
    CTTotalPriceView *totalPriceView;
}

@property (nonatomic, weak) IBOutlet SwipeView *swipeView;
@property (nonatomic, weak) IBOutlet UIPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UILabel *lbTitle;

@property (nonatomic, weak) IBOutlet UILabel *lbDescription;
@property (nonatomic, weak) IBOutlet UIView *descriptionView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *descriptionViewHeight;

@property (nonatomic, weak) IBOutlet UILabel *lbAddres;
@property (nonatomic, weak) IBOutlet UIView *addresView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *addresViewHeight;

@property (nonatomic, weak) IBOutlet UILabel *lbNearbyHotels;
@property (nonatomic, weak) IBOutlet UIView *nearbyHotelsView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *nearbyHotelsViewHeight;

@property (nonatomic, weak) IBOutlet UILabel *lbWork;
@property (nonatomic, weak) IBOutlet UIView *workView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *workViewHeight;

@end

@implementation CTRestaurantDetailedController

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    self.title = _restaurant.name;
}

- (void)setup
{
    [super setup];
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTRestaurantPriceCellData class]
                                                       forModel:[CTBOMealsSet class]];

    pricingSection = [CTSectionReadonly section];
    [tableDisposer addSection:pricingSection];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangePrice:) name:kDidChangeNumberOfPrice object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    tableDisposer.tableView.tableHeaderView = nil;
    
    totalPriceView = [CTTotalPriceView create];
    
    [self calculatePrice];
}

- (void)showAtractionInfo
{
    tableDisposer.tableView.tableHeaderView = self.tableViewHeader;
    
    self.title = _restaurant.name;
    
    self.lbTitle.text = _restaurant.name;
    self.pageControl.numberOfPages = [_restaurant.photos count];
    [self.swipeView reloadData];
    
    //description
    if (_restaurant.descriptionText)
    {
        self.lbDescription.text = _restaurant.descriptionText;
        self.descriptionView.hidden = NO;
        self.descriptionViewHeight.active = NO;
    }
    else
    {
        self.descriptionView.hidden = YES;
        self.descriptionViewHeight.active = YES;
        self.descriptionViewHeight.constant = 0;
    }
    
    //address
    if (_restaurant.address)
    {
        self.lbAddres.text = _restaurant.address;
        self.addresView.hidden = NO;
        self.addresViewHeight.active = NO;
    }
    else
    {
        self.addresView.hidden = YES;
        self.addresViewHeight.active = YES;
        self.addresViewHeight.constant = 0;
    }
    
    //nearbyHotels
    if ([_restaurant nearbyHotelsText])
    {
        self.lbNearbyHotels.attributedText = [_restaurant nearbyHotelsText];
        self.nearbyHotelsView.hidden = NO;
        self.nearbyHotelsViewHeight.active = NO;
    }
    else
    {
        self.nearbyHotelsView.hidden = YES;
        self.nearbyHotelsViewHeight.active = YES;
        self.nearbyHotelsViewHeight.constant = 0;
    }
    
    //work
    if (_restaurant.work)
    {
        self.lbWork.text = _restaurant.work;
        self.workView.hidden = NO;
        self.workViewHeight.active = NO;
    }
    else
    {
        self.workView.hidden = YES;
        self.workViewHeight.active = YES;
        self.workViewHeight.constant = 0;
    }
    
    [self resizeTableViewHeaderForWidth:self.view.frame.size.width];
    
    [pricingSection removeAllCellData];

    NSArray *pricings = _restaurant.mealsSet;
    totalPriceView.lbPriceValue.text = @"$0.00";

    if (pricings.count)
    {
        pricingSection.headerView = totalPriceView;
        [((CTTableDisposerModeled *)tableDisposer) setupModels:pricings forSection:pricingSection];
    }
    else
    {
        pricingSection.headerView = nil;
    }
    
    [tableDisposer reloadData];
    [self calculatePrice];
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance backButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Actions

- (IBAction)addressByttonPresses:(id)sender
{
    if (fabs(_restaurant.latitudeValue) > 0.01 &&
        fabs(_restaurant.longitudeValue) > 0.01)
    {
        NSString *mapString = [NSString stringWithFormat:@"http://maps.apple.com/?ll=%@,%@",
                               _restaurant.latitude,
                               _restaurant.longitude];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mapString]];
    }
}

- (IBAction)sendButtonPressd
{
    NSMutableArray *selectedItems = [NSMutableArray array];
    for (NSInteger i = 0; i < pricingSection.cellDataCount; i++)
    {
        CTRestaurantPriceCellData *cellData = (CTRestaurantPriceCellData *)[pricingSection cellDataAtIndex:i];
        if (cellData.selected && [cellData priceData])
        {
            NSString *strData = [NSString stringWithFormat:@"{'id':%@,'quantity':%lu}",
                                 [[cellData priceData] priceIdentifier],
                                 [cellData count]];
            
            [selectedItems addObject:strData];
        }
    }
    
    if (selectedItems.count > 0)
    {
        CTRequest *request = [[CTHotelsGateway sharedInstance] bookRestaurants:_restaurant.identifier bookData:selectedItems];
        [self showActivity];
        CTWeakSelf
        [request addResponseBlock:^(CTResponse *aResponse)
         {
             [weakSelf bookDidEndWithResponse:aResponse];
             
         } responseQueue:dispatch_get_main_queue()];
        
        [request start];
    }
}

- (void)bookDidEndWithResponse:(CTResponse *)response
{
    [self hideActivity];
    if (response.success)
    {
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kOrderScreenStoryBoardId];
        [self.mm_drawerController setCenterViewController:vc];
    }
    else if (response.textMessage)
    {
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

#pragma mark - Fetsh Data

- (void)fetchData
{
    [self showActivity];
    CTWeakSelf
    CTRequest *request = [[CTHotelsGateway sharedInstance] restaurantInfoById:_restaurant.identifier];
    
    [request addResponseBlock:^(CTResponse *aResponse)
     {
         [weakSelf fetchDataDidFinish:aResponse];
     } responseQueue:dispatch_get_main_queue()];
    
    [request start];
}

- (void)fetchDataDidFinish:(CTResponse *)response
{
    [self hideActivity];
    if (response.success)
    {
        [_restaurant setupDetailedDictionary:response.dataDictionary];
        
        [CTMainStorageInstance scheduleBlock:^
         {
             [CTMainStorageInstance save];
         }];
        
        [self showAtractionInfo];
    }
    else
    {
        [self showAtractionInfo];
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

#pragma mark - TableView

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CTRestaurantPriceCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[CTRestaurantPriceCell class]])
    {
        CTRestaurantPriceCellData *cellData = (CTRestaurantPriceCellData *)cell.cellData;
        cellData.selected = !cellData.selected;
        
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self calculatePrice];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[CTRestaurantPriceCell class]])
    {
        ((CTRestaurantPriceCell *)cell).delegate = self;
    }
}

#pragma mark - SwipeViewDataSource

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    return [_restaurant.photos count];
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIImageView *)view
{
    self.pageControl.currentPage = swipeView.currentItemIndex;
    if (!view)
    {
        view = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, swipeView.frame.size.width, swipeView.frame.size.height)];
        view.clipsToBounds = YES;
        view.contentMode = UIViewContentModeScaleAspectFill;
        view.userInteractionEnabled = YES;
    }
    
    CTBOPhotoInfo *photoInfo = [_restaurant.photos objectAtIndex:index];
    NSURL *selectedImageURL = [NSURL URLWithString:photoInfo.photoPath];
    [view sd_setImageWithURL:selectedImageURL
            placeholderImage:nil
                     options:SDWebImageRetryFailed];
    
    return view;
}

#pragma mark - SwipeViewDelegate

- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView
{
    self.pageControl.currentPage = swipeView.currentItemIndex;
}

#pragma mark - Calculate Price

- (void)calculatePrice
{
    double price = 0;
    for (NSInteger i = 0; i < pricingSection.cellDataCount; i++)
    {
        CTRestaurantPriceCellData *cellData = (CTRestaurantPriceCellData *)[pricingSection cellDataAtIndex:i];
        if (cellData.selected)
        {
            price += cellData.count ? cellData.count * [[[cellData priceData] priceValue] floatValue] : [[[cellData priceData] priceValue] floatValue];
        }
    }
    totalPriceView.lbPriceValue.text = [NSString stringWithFormat:@"%@%0.2f", [CTAppManagerInstance.currentUser currencySymbol], price];
}

#pragma mark - Notification
- (void)didChangePrice:(NSNotification *)notification
{
    [self calculatePrice];
}

#pragma mark - CTRestaurantPriceCellProtocol

- (void)didSelectMealsSet:(CTBOMealsSet *)mealsSet
{
    CTMealsSetController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kMealsSetScreenStoryBoardId];
    vc.mealsSet = mealsSet;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
