//
//  CTRestaurantsController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTRestaurantsController.h"
#import "CTRestaurantCell.h"
#import "CTRestaurantFetcher.h"
#import "CTRestaurantDetailedController.h"

@interface CTRestaurantsController ()
{
    IBOutlet UIBarButtonItem *sortedButton;
    BOOL isDescrSort;
    NSArray *restaurants;
    
    CTRestaurantFetcher *dataFetcher;
}

@end

@implementation CTRestaurantsController

#pragma mark - Setup

- (void)setup
{
    [super setup];
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTRestaurantCellData class]
                                                       forModel:[BORestaurant class]];
    dataFetcher = [[CTRestaurantFetcher alloc] initWithStorage:CTMainStorageInstance];
    
    [tableDisposer addSection:[CTSectionReadonly section]];
}

- (BOOL)needShowChageAraeButton
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    sortedButton.image = [UIImage imageNamed:@"ic_sort_desc"];
}

- (void)fetchData
{
    [self showActivity];
    CTWeakSelf
    [dataFetcher fetchDataByMessage:[CTFetcherMessage new] withCallback:^(CTResponse *aResponse)
     {
         dispatch_async(dispatch_get_main_queue(), ^
                        {
                            [weakSelf fetchDataDidResponse:aResponse];
                        });
     }];
}

-(void)fetchDataDidResponse:(CTResponse *)response
{
    [self hideActivity];
    if (response.success)
    {
        restaurants = response.boArray;
        NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"category" ascending:isDescrSort];
        
        [[tableDisposer sectionByIndex:0] removeAllCellData];
        [((CTTableDisposerModeled *)tableDisposer) setupModels:[restaurants sortedArrayUsingDescriptors:@[sd]] forSectionAtIndex:0];
        [tableDisposer reloadData];
    }
    else if (response.textMessage)
    {
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

#pragma mark - Button Pressed

- (IBAction)sortedButtonPressed:(id)sender
{
    isDescrSort = !isDescrSort;
    if (isDescrSort)
    {
        sortedButton.image = [UIImage imageNamed:@"ic_sort"];
    }
    else
    {
        sortedButton.image = [UIImage imageNamed:@"ic_sort_desc"];
    }
    
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"category" ascending:isDescrSort];
    
    [[tableDisposer sectionByIndex:0] removeAllCellData];
    [((CTTableDisposerModeled *)tableDisposer) setupModels:[restaurants sortedArrayUsingDescriptors:@[sd]] forSectionAtIndex:0];
    [tableDisposer reloadData];
}

#pragma mark - Languare

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    self.title = CTLocalizedString(@"Restaurants");
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance menuButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    if (self.mm_drawerController.openSide != MMDrawerSideLeft)
    {
        [self.mm_drawerController openDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    }
    else
    {
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    }
}

#pragma mark - Storyboard Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(CTRestaurantCell *)sender
{
    if ([sender isKindOfClass:[CTRestaurantCell class]])
    {
        CTRestaurantCellData *cellData = (CTRestaurantCellData *)[tableDisposer cellDataByIndexPath:[tableDisposer.tableView indexPathForCell:sender]];
        ((CTRestaurantDetailedController *)segue.destinationViewController).restaurant = [cellData restaurant];
    }
}

@end
