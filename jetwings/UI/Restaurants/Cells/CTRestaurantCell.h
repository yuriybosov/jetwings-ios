//
//  CTRestaurantCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>
#import "BORestaurant.h"

@interface CTRestaurantCellData : CTCellDataModeled

- (BORestaurant *)restaurant;

@end


@interface CTRestaurantCell : CTCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UIImageView *picture;
@property (nonatomic, weak) IBOutlet UILabel *lbName;

@end
