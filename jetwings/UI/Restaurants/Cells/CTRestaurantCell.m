//
//  CTRestaurantCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/15/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTRestaurantCell.h"


@implementation CTRestaurantCellData

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return 150;
}

- (BORestaurant *)restaurant
{
    return self.model;
}

- (NSString *)cellIdentifier
{
    return @"restaurantCell";
}

@end

@implementation CTRestaurantCell

- (void)setupCellData:(CTRestaurantCellData *)aCellData
{
    [super setupCellData:aCellData];
    
    [self.picture sd_setImageWithURL:[NSURL URLWithString:[aCellData restaurant].picture]];
    self.lbName.text = [[aCellData restaurant].name uppercaseString];
    
    [self.collectionView reloadData];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return ((CTRestaurantCellData *)self.cellData).restaurant.categoryValue;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellStar" forIndexPath:indexPath];
    UIImageView *imageView = [cell viewWithTag:10];
    imageView.image = [UIImage imageNamed:@"ic_dollar"];
    
    return cell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    CGFloat cellSpacing = ((UICollectionViewFlowLayout *) collectionViewLayout).minimumLineSpacing;
    CGFloat cellWidth = ((UICollectionViewFlowLayout *) collectionViewLayout).itemSize.width;
    NSInteger cellCount = [collectionView numberOfItemsInSection:section];
    CGFloat inset = (collectionView.bounds.size.width - (cellCount * (cellWidth + cellSpacing))) * 0.5;
    inset = MAX(inset, 0.0);
    return UIEdgeInsetsMake(0.0, inset, 0.0, 0.0);
}

@end
