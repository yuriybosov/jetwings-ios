//
//  CTRestaurantPriceCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/19/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTPriceCell.h"
#import "CTBOMealsSet.h"

@protocol  CTRestaurantPriceCellProtocol <NSObject>

- (void)didSelectMealsSet:(CTBOMealsSet *)mealsSet;

@end

@interface CTRestaurantPriceCellData : CTPriceCellData

@end

@interface CTRestaurantPriceCell : CTPriceCell

@property (nonatomic, weak) id<CTRestaurantPriceCellProtocol>delegate;

@end
