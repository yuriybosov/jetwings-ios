//
//  CTRestaurantPriceCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/19/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTRestaurantPriceCell.h"

@implementation CTRestaurantPriceCellData

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return self.selected ? 72 : 44;
}

- (NSString *)cellIdentifier
{
    return @"restaurantPriceCell";
}

@end


@implementation CTRestaurantPriceCell

- (IBAction)didMealsSetButtonPressed:(id)sender
{
    [self.delegate didSelectMealsSet:((CTRestaurantPriceCellData *)self.cellData).model];
}

@end
