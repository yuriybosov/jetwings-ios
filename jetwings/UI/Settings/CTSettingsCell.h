//
//  CTSettingsCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>

@interface CTSettingsCellData : CTCellDataMaped

@property (nonatomic, strong) NSString *settingIconName;
@property (nonatomic, assign) BOOL showAccessoryIcon;

@property (nonatomic, strong) NSString *settingTitle;
@property (nonatomic, strong) NSString *settingValue;
@property (nonatomic, strong) UIColor *settingValueColor;

@end


@interface CTSettingsCell : CTCell

@property (nonatomic, weak) IBOutlet UIImageView *settingIcon;
@property (nonatomic, weak) IBOutlet UIImageView *settingAccessoryIcon;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *settingAccessoryIconWidthConstraint;
@property (nonatomic, weak) IBOutlet UILabel *settingValue;
@property (nonatomic, weak) IBOutlet UILabel *settingTitle;

@end
