//
//  CTSettingsCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTSettingsCell.h"

@implementation CTSettingsCellData

- (id)init
{
    self = [super initWithObject:nil key:nil];
    if (self)
    {
        self.cellSelectionStyle = UITableViewCellSelectionStyleGray;
    }
    return self;
}

- (NSString *)cellIdentifier
{
    return @"settingsCell";
}

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return 46;
}

@end


@implementation CTSettingsCell

- (void)setupCellData:(CTSettingsCellData *)aCellData
{
    [super setupCellData:aCellData];
    
    self.settingIcon.image = [UIImage imageNamed:aCellData.settingIconName];
    self.settingTitle.text = aCellData.settingTitle;
    self.settingValue.text = aCellData.settingValue;
    self.settingValue.textColor = aCellData.settingValueColor;
    
    if (aCellData.showAccessoryIcon)
    {
        self.settingAccessoryIcon.hidden = NO;
        self.settingAccessoryIconWidthConstraint.constant = 15;
    }
    else
    {
        self.settingAccessoryIcon.hidden = YES;
        self.settingAccessoryIconWidthConstraint.constant = 0;
    }
}

@end
