//
//  CTSettingsController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTSettingsController.h"
#import "CTSettingsCell.h"
#import "CTPopupSimplePicker.h"
#import "CTUserGateway.h"

@interface CTSettingsController ()
{
    CTSettingsCellData *currencyCellData;
    CTSettingsCellData *languageCellData;
    
    CTPopupSimplePicker* pickerCurrency;
    CTPopupSimplePicker* pickerLanguage;
}

@property (nonatomic, weak) IBOutlet UIImageView *bgUserAvatar;
@property (nonatomic, weak) IBOutlet UIImageView *userAvatar;
@property (nonatomic, weak) IBOutlet UILabel *userName;

@property (nonatomic, weak) IBOutlet UILabel *userPhoneTitle;
@property (nonatomic, weak) IBOutlet UILabel *userPhoneValue;

@property (nonatomic, weak) IBOutlet UILabel *userIDTitle;
@property (nonatomic, weak) IBOutlet UILabel *userIDValue;

@property (nonatomic, weak) IBOutlet UILabel *userEmailTitle;
@property (nonatomic, weak) IBOutlet UILabel *userEmailValue;

- (void)reloadData;

@end

@implementation CTSettingsController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.bgUserAvatar.clipsToBounds = YES;
    self.bgUserAvatar.layer.cornerRadius = self.bgUserAvatar.frame.size.width/2;
    
    self.userAvatar.clipsToBounds = YES;
    self.userAvatar.layer.cornerRadius = self.userAvatar.frame.size.width/2;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPickerToolbarItemNotification:) name:CT_POPUPVIEW_TOOLBAR_ITEM_DID_PRESSED object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self reloadData];
}

#pragma mark - Languare

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    self.title = CTLocalizedString(@"Settings");
    [self reloadData];
}

#pragma mark - Table

- (void)reloadData
{
    [tableDisposer removeAllSections];
    
    [self.userAvatar sd_setImageWithURL:[NSURL URLWithString:CTAppManagerInstance.currentUser.userAvatar]];
    self.userName.text = [CTAppManagerInstance.currentUser fullName];
    
    self.userPhoneTitle.text = CTLocalizedString(@"Contact number");
    self.userPhoneValue.text = CTAppManagerInstance.currentUser.userPhone;
    
    self.userIDTitle.text = CTLocalizedString(@"ID");
    self.userIDValue.text = [CTAppManagerInstance.currentUser.userID stringValue];
    
    self.userEmailTitle.text = CTLocalizedString(@"Email");
    self.userEmailValue.text = CTAppManagerInstance.currentUser.userEmail;
    
    [self resizeTableViewHeaderForWidth:self.view.frame.size.width];
    
    CTSectionReadonly *section = [CTSectionReadonly section];
    [tableDisposer addSection:section];
    
    currencyCellData = [[CTSettingsCellData alloc] init];
    currencyCellData.settingIconName = @"ic_currency";
    currencyCellData.settingTitle = CTLocalizedString(@"Currency");
    currencyCellData.settingValue = CTAppManagerInstance.currentUser.userCurrency;
    currencyCellData.settingValueColor = [UIColor colorWithRedI:83 greenI:87 blueI:100 alphaI:255];
    currencyCellData.showAccessoryIcon = YES;
    [section addCellData:currencyCellData];
    
    languageCellData = [[CTSettingsCellData alloc] init];
    languageCellData.settingIconName = @"ic_language";
    languageCellData.settingTitle = CTLocalizedString(@"Language");
    languageCellData.settingValue = [CTLanguageMangerInstance langNameByID:CTAppManagerInstance.currentUser.userLang];
    languageCellData.settingValueColor = [UIColor colorWithRedI:57 greenI:169 blueI:255 alphaI:255];
    languageCellData.cellAccessoryType = UITableViewCellAccessoryNone;
    [section addCellData:languageCellData];
    
    [tableDisposer reloadData];
}

#pragma mark - Right Navbar Button

- (IBAction)rightNavbarButtonPressed
{
    [CTAppManagerInstance logout];
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance menuButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    if (self.mm_drawerController.openSide != MMDrawerSideLeft)
    {
        [self.mm_drawerController openDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    }
    else
    {
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    }
}

#pragma mark - TableVeiw

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        [self showCurrencyPicker];
    }
    else
    {
        [self showLanguarePicker];
    }
}

#pragma mark - Picker Veiw

- (void)showCurrencyPicker
{
    pickerCurrency = [[CTPopupSimplePicker alloc] init];
    pickerCurrency.backgroundColor = [UIColor colorWithRedI:246 greenI:247 blueI:250 alphaI:255];
    
    pickerCurrency.dataSource = @[[[CTTitledID alloc] initWithID:@"SGD" title:@"SGD"],
                                  [[CTTitledID alloc] initWithID:@"NTD" title:@"NTD"],
                                  [[CTTitledID alloc] initWithID:@"RMB" title:@"RMB"]];
    pickerCurrency.toolbar = [UIConfiguratorInstance defaultPickerToolbarWithTitle:CTLocalizedString(@"Currency")];
    
    CTTitledID *titleID = [pickerCurrency.dataSource titledIDByID:CTAppManagerInstance.currentUser.userCurrency];
    
    [pickerCurrency prepareToShow];
    [pickerCurrency setSelectedItem: titleID];
    [pickerCurrency showWithAnimation:YES inView:self.navigationController.view];
}

- (void)showLanguarePicker
{
    pickerLanguage = [[CTPopupSimplePicker alloc] init];
    pickerLanguage.backgroundColor = [UIColor colorWithRedI:246 greenI:247 blueI:250 alphaI:255];
    
    pickerLanguage.dataSource = @[[[CTTitledID alloc] initWithID:kLanguageEN title:[CTLanguageMangerInstance langNameByID:kLanguageEN]],
                                      [[CTTitledID alloc] initWithID:kLanguageCH title:[CTLanguageMangerInstance langNameByID:kLanguageCH]]];
    pickerLanguage.toolbar = [UIConfiguratorInstance defaultPickerToolbarWithTitle:CTLocalizedString(@"Language")];
    
    CTTitledID *titleID = [pickerLanguage.dataSource titledIDByID:CTAppManagerInstance.currentUser.userLang];
    
    [pickerLanguage prepareToShow];
    [pickerLanguage setSelectedItem:titleID];
    [pickerLanguage showWithAnimation:YES inView:self.navigationController.view];
}

#pragma mark - Notifikation picker
- (void)didPickerToolbarItemNotification:(NSNotification*)aNotification
{
    if (aNotification.object == pickerCurrency)
    {
        [pickerCurrency hideWithAnimation:YES];
        CTTitledID *currency = pickerCurrency.selectedItem;
        
        if ([currency.ID isEqualToString:CTAppManagerInstance.currentUser.userCurrency])
            return;
        
        [self showActivity];
        __weak CTSettingsController *weakSelf = self;
        
        CTRequest *request = [[CTUserGateway sharedInstance] changeCurensy:currency.ID];
        [request addResponseBlock:^(CTResponse *aResponse)
        {
            [weakSelf hideActivity];
            if (aResponse.success)
            {
                CTAppManagerInstance.currentUser.userCurrency = currency.ID;
                [CTAppManagerInstance saveCurrentUser];
                [weakSelf reloadData];
            }
            else
            {
                [weakSelf showAlertViewWithTitle:CTLocalizedString(@"Error") message:aResponse.textMessage];
            }
            
        } responseQueue:dispatch_get_main_queue()];
        [request start];
    }
    else if (aNotification.object == pickerLanguage)
    {
        [pickerLanguage hideWithAnimation:YES];
        CTTitledID *lang = pickerLanguage.selectedItem;
        
        if ([lang.ID isEqualToString:CTAppManagerInstance.currentUser.userLang])
            return;
        
        [self showActivity];
        __weak CTSettingsController *weakSelf = self;
        
        CTRequest *request = [[CTUserGateway sharedInstance] changeLanguare:lang.ID];
        [request addResponseBlock:^(CTResponse *aResponse)
         {
             [weakSelf hideActivity];
             if (aResponse.success)
             {
                 CTAppManagerInstance.currentUser.userLang = lang.ID;
                 [CTAppManagerInstance saveCurrentUser];
                 CTLanguageMangerInstance.currentLanguage = CTAppManagerInstance.currentUser.userLang;
             }
             else
             {
                 [weakSelf showAlertViewWithTitle:CTLocalizedString(@"Error") message:aResponse.textMessage];
             }
             
         } responseQueue:dispatch_get_main_queue()];
        [request start];
    }
}

@end
