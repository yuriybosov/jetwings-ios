//
//  CTTourDetailedController.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/25/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseTableController.h"
#import "BOTour.h"
#import "SwipeView.h"


@interface CTTourDetailedController : CTBaseTableController <SwipeViewDataSource, SwipeViewDelegate>

@property (nonatomic, strong) BOTour* tour;

@end
