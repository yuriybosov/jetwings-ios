//
//  CTTourDetailedController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/25/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTTourDetailedController.h"
#import "CTTourTicketCell.h"
#import "CTTotalPriceView.h"
#import "CTHotelsGateway.h"

@interface CTTourDetailedController ()
{
    CTSectionReadonly *pricingSection;
    CTTotalPriceView *totalPriceView;
}

@property (nonatomic, weak) IBOutlet SwipeView *swipeView;
@property (nonatomic, weak) IBOutlet UIPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UILabel *lbTitle;

@property (nonatomic, weak) IBOutlet UILabel *lbDescription;
@property (nonatomic, weak) IBOutlet UIView *descriptionView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *descriptionViewHeight;

@property (nonatomic, weak) IBOutlet UILabel *lbStarDateTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbEndDateTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbStarDate;
@property (nonatomic, weak) IBOutlet UILabel *lbEndDate;
@property (nonatomic, weak) IBOutlet UIView *dateView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *dateViewHeight;

@end


@implementation CTTourDetailedController

#pragma mark - Languare

- (void)setup
{
    [super setup];
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTTourTicketCellData class]
                                                       forModel:[CTBOTourTicket class]];
    
    pricingSection = [CTSectionReadonly section];
    [tableDisposer addSection:pricingSection];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangePrice:) name:kDidChangeNumberOfPrice object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    tableDisposer.tableView.tableHeaderView = nil;
    
    totalPriceView = [CTTotalPriceView create];
    
    [self calculatePrice];
}

- (void)showAtractionInfo
{
    tableDisposer.tableView.tableHeaderView = self.tableViewHeader;
    
    self.title = _tour.name;
    
    self.lbTitle.text = _tour.name;
    self.pageControl.numberOfPages = [_tour.tourPictures count];
    [self.swipeView reloadData];
    
    //description
    if (_tour.descriptionText)
    {
        self.lbDescription.text = _tour.descriptionText;
        self.descriptionView.hidden = NO;
        self.descriptionViewHeight.active = NO;
    }
    else
    {
        self.descriptionView.hidden = YES;
        self.descriptionViewHeight.active = YES;
        self.descriptionViewHeight.constant = 0;
    }
    
    //date
    if (_tour.startDate || _tour.endDate)
    {
        static NSDateFormatter *dateFormatter = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^
                      {
                          dateFormatter = [NSDateFormatter new];
                          dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
                          dateFormatter.dateFormat = @"dd.MM.yyyy";
                      });
        
        self.lbStarDate.text = [dateFormatter stringFromDate:_tour.startDate] ?: @"-";
        self.lbEndDate.text = [dateFormatter stringFromDate:_tour.endDate] ?: @"-";
        
        self.lbStarDateTitle.text = CTLocalizedString(@"Start date");
        self.lbEndDateTitle.text = CTLocalizedString(@"End date");
        
        self.dateView.hidden = NO;
        self.dateViewHeight.active = NO;
    }
    else
    {
        self.dateView.hidden = YES;
        self.dateViewHeight.active = YES;
        self.dateViewHeight.constant = 0;
    }
    
    [self resizeTableViewHeaderForWidth:self.view.frame.size.width];
    
    [pricingSection removeAllCellData];
    
    NSArray *pricings = _tour.tourTicket;
    totalPriceView.lbPriceValue.text = @"$0.00";
    
    if (pricings.count)
    {
        pricingSection.headerView = totalPriceView;
        [((CTTableDisposerModeled *)tableDisposer) setupModels:pricings forSection:pricingSection];
    }
    else
    {
        pricingSection.headerView = nil;
    }
    
    [tableDisposer reloadData];
    [self calculatePrice];
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance backButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Fetsh Data

- (void)fetchData
{
    [self showActivity];
    CTWeakSelf
    CTRequest *request = [[CTHotelsGateway sharedInstance] tourInfoById:_tour.identifier];
    
    [request addResponseBlock:^(CTResponse *aResponse)
     {
         [weakSelf fetchDataDidFinish:aResponse];
     } responseQueue:dispatch_get_main_queue()];
    
    [request start];
}

- (void)fetchDataDidFinish:(CTResponse *)response
{
    [self hideActivity];
    if (response.success)
    {
        [_tour setupDetailedDictionary:response.dataDictionary];
        
        [CTMainStorageInstance scheduleBlock:^
         {
             [CTMainStorageInstance save];
         }];
        
        [self showAtractionInfo];
    }
    else
    {
        [self showAtractionInfo];
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

#pragma mark - Button Actiom

- (IBAction)sendButtonPressd
{
    NSMutableArray *selectedItems = [NSMutableArray array];
    for (NSInteger i = 0; i < pricingSection.cellDataCount; i++)
    {
        CTTourTicketCellData *cellData = (CTTourTicketCellData *)[pricingSection cellDataAtIndex:i];
        if (cellData.selected && [cellData priceData])
        {
            NSString *strData = [NSString stringWithFormat:@"{'id':%@,'quantity':%lu}",
                                 [[cellData priceData] priceIdentifier],
                                 [cellData count]];
            
            [selectedItems addObject:strData];
        }
    }
    
    if (selectedItems.count > 0)
    {
        CTRequest *request = [[CTHotelsGateway sharedInstance] bookTourPackage:_tour.identifier bookData:selectedItems];
        [self showActivity];
        CTWeakSelf
        [request addResponseBlock:^(CTResponse *aResponse)
         {
             [weakSelf bookDidEndWithResponse:aResponse];
             
         } responseQueue:dispatch_get_main_queue()];
        
        [request start];
    }
}

- (void)bookDidEndWithResponse:(CTResponse *)response
{
    [self hideActivity];
    if (response.success)
    {
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kOrderScreenStoryBoardId];
        [self.mm_drawerController setCenterViewController:vc];
    }
    else if (response.textMessage)
    {
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

#pragma mark - TableView

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CTTourTicketCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[CTTourTicketCell class]])
    {
        CTTourTicketCellData *cellData = (CTTourTicketCellData *)cell.cellData;
        cellData.selected = !cellData.selected;
        
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self calculatePrice];
    }
}

#pragma mark - SwipeViewDataSource

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    return [_tour.tourPictures count];
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIImageView *)view
{
    self.pageControl.currentPage = swipeView.currentItemIndex;
    if (!view)
    {
        view = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, swipeView.frame.size.width, swipeView.frame.size.height)];
        view.clipsToBounds = YES;
        view.contentMode = UIViewContentModeScaleAspectFill;
        view.userInteractionEnabled = YES;
    }
    
    CTBOPhotoInfo *photoInfo = [_tour.tourPictures objectAtIndex:index];
    NSURL *selectedImageURL = [NSURL URLWithString:photoInfo.photoPath];
    [view sd_setImageWithURL:selectedImageURL
            placeholderImage:nil
                     options:SDWebImageRetryFailed];
    
    return view;
}

#pragma mark - SwipeViewDelegate

- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView
{
    self.pageControl.currentPage = swipeView.currentItemIndex;
}

#pragma mark - Calculate Price

- (void)calculatePrice
{
    double price = 0;
    for (NSInteger i = 0; i < pricingSection.cellDataCount; i++)
    {
        CTTourTicketCellData *cellData = (CTTourTicketCellData *)[pricingSection cellDataAtIndex:i];
        if (cellData.selected)
        {
            price += cellData.count ? cellData.count * [[[cellData priceData] priceValue] floatValue] : [[[cellData priceData] priceValue] floatValue];
        }
    }
    totalPriceView.lbPriceValue.text = [NSString stringWithFormat:@"%@%0.2f", [CTAppManagerInstance.currentUser currencySymbol], price];
}

#pragma mark - Notification
- (void)didChangePrice:(NSNotification *)notification
{
    [self calculatePrice];
}

@end
