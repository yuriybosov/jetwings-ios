//
//  CTToursController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTToursController.h"
#import "CTToursFetcher.h"
#import "CTTourCell.h"
#import "CTTourDetailedController.h"

@interface CTToursController ()

@end

@implementation CTToursController

#pragma mark - Setup

- (void)setup
{
    [super setup];
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTTourCellData class]
                                                       forModel:[BOTour class]];
    moduleList.dataFetcher = [[CTToursFetcher alloc] initWithStorage:CTMainStorageInstance];
}

- (BOOL)needShowChageAraeButton
{
    return YES;
}

#pragma mark - Languare

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    self.title = CTLocalizedString(@"Tour Packages");
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance menuButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    if (self.mm_drawerController.openSide != MMDrawerSideLeft)
    {
        [self.mm_drawerController openDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    }
    else
    {
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    }
}

#pragma mark - Storyboard Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(CTTourCell *)sender
{
    if ([sender isKindOfClass:[CTTourCell class]])
    {
        CTTourCellData *cellData = (CTTourCellData *)[tableDisposer cellDataByIndexPath:[tableDisposer.tableView indexPathForCell:sender]];
        ((CTTourDetailedController *)segue.destinationViewController).tour = [cellData tour];
    }
}

@end
