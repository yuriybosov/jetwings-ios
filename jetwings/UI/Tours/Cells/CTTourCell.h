//
//  CTTourCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/25/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>
#import "BOTour.h"

@interface CTTourCellData : CTCellDataModeled

- (BOTour *)tour;

@end


@interface CTTourCell : CTCell

@property (nonatomic, weak) IBOutlet UIImageView *picture;
@property (nonatomic, weak) IBOutlet UILabel *lbName;
@property (nonatomic, weak) IBOutlet UILabel *lbTime;

@end
