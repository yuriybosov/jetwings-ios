//
//  CTTourCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/25/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTTourCell.h"

@implementation CTTourCellData

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return 150;
}

- (BOTour *)tour
{
    return self.model;
}

- (NSString *)cellIdentifier
{
    return @"tourCell";
}

@end


@implementation CTTourCell

- (void)setupCellData:(CTTourCellData *)aCellData
{
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      dateFormatter = [NSDateFormatter new];
                      dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
                      dateFormatter.dateFormat = @"dd MMM";
                  });
    
    [super setupCellData:aCellData];
    
    [self.picture sd_setImageWithURL:[NSURL URLWithString:[aCellData tour].picture]];
    self.lbName.text = [[aCellData tour].name uppercaseString];
    
    NSMutableArray *temp = [NSMutableArray array];
    
    if ([aCellData tour].startDate)
    {
        NSString *start = [NSString stringWithFormat:@"%@: %@", CTLocalizedString(@"Start date"), [dateFormatter stringFromDate:[aCellData tour].startDate]];
        [temp addObject:start];
    }
    
    if ([aCellData tour].endDate)
    {
        NSString *end = [NSString stringWithFormat:@"%@: %@", CTLocalizedString(@"End date"), [dateFormatter stringFromDate:[aCellData tour].endDate]];
        [temp addObject:end];
    }
    
    self.lbTime.text = [temp componentsJoinedByString:@"  "];
}

@end
