//
//  CTTourTicket.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/25/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTPriceCell.h"

@interface CTTourTicketCellData : CTPriceCellData

@end


@interface CTTourTicketCell : CTPriceCell

@end
