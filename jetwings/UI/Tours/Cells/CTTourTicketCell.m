//
//  CTTourTicket.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/25/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTTourTicketCell.h"

@implementation CTTourTicketCellData

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return self.selected ? 72 : 44;
}

- (NSString *)cellIdentifier
{
    return @"tourTicketCell";
}

@end


@implementation CTTourTicketCell

@end
