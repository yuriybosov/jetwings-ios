//
//  CTTransportController.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseTableController.h"
#import "CTTransportCell.h"

@interface CTTransportController : CTBaseTableController <CTTransportCellProtocol>

@end
