//
//  CTTransportController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/13/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTTransportController.h"
#import "CTTransportsFetcher.h"
#import "CTTransportCell.h"
#import "CTVehiclesCell.h"
#import "CTTransportDetailedController.h"
#import "CTTransportPriceView.h"
#import "CTHotelsGateway.h"

@interface CTTransportController ()
{
    CTTransportsFetcher *fetcher;
    
    NSArray *itemised;
    NSArray *packages;
    NSArray *vehicle;
    
    BOOL showAllVehicles;
    CTSectionReadonly *vehicleSection;
    CTSectionReadonly *transportSection;
    
    CTTransportPriceView* transportPriceView;
}

@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic, weak) IBOutlet UIButton *showAllVehiclesButton;

- (void)calculatePrice;

@end

@implementation CTTransportController

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    self.title = CTLocalizedString(@"Transport");
    
    [self.segmentedControl setTitle:CTLocalizedString(@"Itemised") forSegmentAtIndex:0];
    [self.segmentedControl setTitle:CTLocalizedString(@"Packages") forSegmentAtIndex:1];
    
    [self.showAllVehiclesButton setTitle:[NSString stringWithFormat:@"  %@  ", CTLocalizedString(@"View all vehicles")]
                                forState:UIControlStateNormal];
}

- (void)setup
{
    [super setup];
    fetcher = [CTTransportsFetcher new];
    
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTVehiclesCellData class]
                                                       forModel:[BOVehicle class]];
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTTransportCellData class]
                                                       forModel:[BOTransportItemised class]];
    [((CTTableDisposerModeled *)tableDisposer) registerCellData:[CTTransportCellData class]
                                                       forModel:[BOTransportPackages class]];
    
    vehicleSection = [CTSectionReadonly section];
    transportSection = [CTSectionReadonly section];
    [tableDisposer addSection:vehicleSection];
    [tableDisposer addSection:transportSection];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangePrice:) name:kDidChangeNumberOfPrice object:nil];
}

- (BOOL)needShowChageAraeButton
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init]
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    
    transportPriceView = [CTTransportPriceView create];
    [transportPriceView.seatsSwitch addTarget:self action:@selector(seatsSwitchChangeValue:) forControlEvents:UIControlEventValueChanged];
    transportSection.headerView = transportPriceView;
    
    [self calculatePrice];
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance menuButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    if (self.mm_drawerController.openSide != MMDrawerSideLeft)
    {
        [self.mm_drawerController openDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    }
    else
    {
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    }
}

#pragma mark - Fetch Data

- (void)fetchData
{
    [self showActivity];
    CTWeakSelf
    [fetcher fetchDataByMessage:[CTFetcherMessage new] withCallback:^(CTResponse *aResponse)
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            [weakSelf didFetchDataWithResponse:aResponse];
        });
    }];
}

- (void)didFetchDataWithResponse:(CTResponse *)response
{
    [self hideActivity];
    if (response.success)
    {
        itemised = [response.dataDictionary objectForKey:@"Itemised"];
        packages = [response.dataDictionary objectForKey:@"Packages"];
        vehicle = [response.dataDictionary objectForKey:@"Vehicle"];

        [vehicleSection removeAllCellData];
        [transportSection removeAllCellData];
        
        if (showAllVehicles)
        {
            [((CTTableDisposerModeled *)tableDisposer) setupModels:vehicle forSection:vehicleSection];
        }
        
        if (self.segmentedControl.selectedSegmentIndex == 0)
        {
            [((CTTableDisposerModeled *)tableDisposer) setupModels:itemised forSection:transportSection];
        }
        else
        {
            [((CTTableDisposerModeled *)tableDisposer) setupModels:packages forSection:transportSection];
        }
        [tableDisposer reloadData];
        [self calculatePrice];
    }
    else if (response.textMessage)
    {
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

#pragma mark - Button Actions

- (void)seatsSwitchChangeValue:(UISwitch *)aSwitch
{
    [tableDisposer reloadData];
    [self calculatePrice];
}

- (IBAction)sendButtonPressd
{
    NSMutableArray *selectedItems = [NSMutableArray array];
    for (NSInteger i = 0; i < transportSection.cellDataCount; i++)
    {
        CTTransportCellData *cellData = (CTTransportCellData *)[transportSection cellDataAtIndex:i];
        if (cellData.selected && [cellData priceData])
        {
            NSString *strData = [NSString stringWithFormat:@"{'id':%@,'quantity':%lu,'small':%@}",
                                 [[cellData priceData] priceIdentifier],
                                  [cellData count],
                                 transportPriceView.seatsSwitch.on ? @"false" : @"true"];
            
            [selectedItems addObject:strData];
        }
    }
    
    if (selectedItems.count > 0)
    {
        CTRequest *request = [[CTHotelsGateway sharedInstance] bookTransportWithData:selectedItems];
        [self showActivity];
        CTWeakSelf
        [request addResponseBlock:^(CTResponse *aResponse)
        {
            [weakSelf bookDidEndWithResponse:aResponse];
            
        } responseQueue:dispatch_get_main_queue()];
        
        [request start];
    }
}

- (void)bookDidEndWithResponse:(CTResponse *)response
{
    [self hideActivity];
    if (response.success)
    {
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kOrderScreenStoryBoardId];
        [self.mm_drawerController setCenterViewController:vc];
    }
    else if (response.textMessage)
    {
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

- (IBAction)viewAllVehiclesButtonPressd
{
    if (showAllVehicles)
    {
        [vehicleSection removeAllCellData];
        [vehicleSection reloadWithAnimation:UITableViewRowAnimationAutomatic];
        
        [self.showAllVehiclesButton setImage:[UIImage imageNamed:@"ic_arrow_down"] forState:UIControlStateNormal];
    }
    else
    {
        [tableDisposer.tableView setContentOffset:CGPointZero animated:YES];
        [((CTTableDisposerModeled *)tableDisposer) setupModels:vehicle forSection:vehicleSection];
        [vehicleSection reloadWithAnimation:UITableViewRowAnimationAutomatic];
        
        [self.showAllVehiclesButton setImage:[UIImage imageNamed:@"ic_arrow_up"] forState:UIControlStateNormal];
    }
    
    showAllVehicles = !showAllVehicles;
}

- (IBAction)segmentedControlDiDChangeValue:(UISegmentedControl *)sender;
{
    [transportSection removeAllCellData];
    
    if (self.segmentedControl.selectedSegmentIndex == 0)
    {
        [((CTTableDisposerModeled *)tableDisposer) setupModels:itemised forSection:transportSection];
    }
    else
    {
        [((CTTableDisposerModeled *)tableDisposer) setupModels:packages forSection:transportSection];
    }
    [tableDisposer reloadData];
    [self calculatePrice];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(CTTransportCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[CTTransportCell class]])
    {
        cell.transportCellDelegate = self;
        [cell setupCellData:[cell cellData]];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CTTransportCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[CTTransportCell class]])
    {
        CTTransportCellData *cellData = (CTTransportCellData *)cell.cellData;
        cellData.selected = !cellData.selected;
        
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self calculatePrice];

    }
}

#pragma mark - Storyboard Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(CTVehiclesCell *)sender
{
    if ([sender isKindOfClass:[CTVehiclesCell class]])
    {
        CTVehiclesCellData *cellData = (CTVehiclesCellData *)sender.cellData;
        ((CTTransportDetailedController *)segue.destinationViewController).vehicle = [cellData vehicle];
    }
}

#pragma mark - Calculate Price

- (void)calculatePrice
{
    double price = 0;
    for (NSInteger i = 0; i < transportSection.cellDataCount; i++)
    {
        CTTransportCellData *cellData = (CTTransportCellData *)[transportSection cellDataAtIndex:i];
        if (cellData.selected)
        {
            double priceTemp = 0;
            if ([self showSmallPrice])
            {
                if ([cellData.model respondsToSelector:@selector(priceSmall)])
                {
                    priceTemp = [[cellData.model priceSmall] floatValue];
                }
            }
            else
            {
                if ([cellData.model respondsToSelector:@selector(priceBig)])
                {
                    priceTemp = [[cellData.model priceBig] floatValue];
                }
            }
            
            price += cellData.count ? cellData.count * priceTemp : priceTemp;
        }
    }
    transportPriceView.lbPriceValue.text = [NSString stringWithFormat:@"%@%0.2f", [CTAppManagerInstance.currentUser currencySymbol], price];
}

#pragma mark - Notification
- (void)didChangePrice:(NSNotification *)notification
{
    [self calculatePrice];
}

#pragma mark - CTTransportCellProtocol

- (BOOL)showSmallPrice
{
    return !transportPriceView.seatsSwitch.on;
}

@end
