//
//  CTTransportDetailedController.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/17/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTBaseController.h"
#import "BOVehicle.h"
#import "SwipeView.h"

@interface CTTransportDetailedController : CTBaseController <SwipeViewDataSource, SwipeViewDelegate>

@property (nonatomic, strong) BOVehicle *vehicle;

@end
