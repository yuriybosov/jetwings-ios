//
//  CTTransportDetailedController.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/17/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTTransportDetailedController.h"
#import "CTHotelsGateway.h"

@interface CTTransportDetailedController ()

@property (nonatomic, weak) IBOutlet SwipeView *swipeView;
@property (nonatomic, weak) IBOutlet UIPageControl *pageControl;

@property (nonatomic, weak) IBOutlet UILabel *lbYeadTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbYeadValue;

@property (nonatomic, weak) IBOutlet UILabel *lbNumberSeatsTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbNumberSeatsValue;

@end

@implementation CTTransportDetailedController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CTRequest *request = [[CTHotelsGateway sharedInstance] vehicleInfoById:self.vehicle.identifier];
    [self showActivity];
    CTWeakSelf
    [request addResponseBlock:^(CTResponse *aResponse)
    {
        [weakSelf didEndRequestWithResponse:aResponse];
    } responseQueue:dispatch_get_main_queue()];
    
    [request start];
}

- (void)setupLanguareValues
{
    [super setupLanguareValues];
    
    self.title = self.vehicle.model;
    
    self.lbYeadTitle.text = CTLocalizedString(@"Year of production");
    self.lbYeadValue.text = self.vehicle.year;
    
    self.lbNumberSeatsTitle.text = CTLocalizedString(@"Number of seats");
    self.lbNumberSeatsValue.text = self.vehicle.seats;
    
    [self.swipeView reloadData];
    self.pageControl.numberOfPages = [self.vehicle.photos count];
}

- (void)didEndRequestWithResponse:(CTResponse *)response
{
    [self hideActivity];
    if (response.success)
    {
        [self.vehicle setupDetailedDictionary:response.dataDictionary];
        [CTMainStorageInstance scheduleBlock:^
         {
             [CTMainStorageInstance save];
         }];
        [self setupLanguareValues];
    }
    else if (response.textMessage)
    {
        [self showAlertViewWithTitle:CTLocalizedString(@"Error") message:response.textMessage];
    }
}

#pragma mark - Left Navbar Button

- (UIBarButtonItem *)createLeftNavButton
{
    return [UIConfiguratorInstance backButton];
}

- (void)leftNavButtonPressed:(id)aSender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SwipeViewDataSource

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    return [self.vehicle.photos count];
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIImageView *)view
{
    self.pageControl.currentPage = swipeView.currentItemIndex;
    if (!view)
    {
        view = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, swipeView.frame.size.width, swipeView.frame.size.height)];
        view.clipsToBounds = YES;
        view.contentMode = UIViewContentModeScaleAspectFill;
        view.userInteractionEnabled = YES;
    }
    
    CTBOPhotoInfo *photoInfo = [self.vehicle.photos objectAtIndex:index];
    NSURL *selectedImageURL = [NSURL URLWithString:photoInfo.photoPath];
    [view sd_setImageWithURL:selectedImageURL
            placeholderImage:nil
                     options:SDWebImageRetryFailed];
    
    return view;
}

#pragma mark - SwipeViewDelegate

- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView
{
    self.pageControl.currentPage = swipeView.currentItemIndex;
}

@end
