//
//  CTTransportCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/16/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>
#import "BOTransportItemised.h"
#import "BOTransportPackages.h"
#import "CTPriceCell.h"

@protocol CTTransportCellProtocol <NSObject>

- (BOOL)showSmallPrice;

@end

@interface CTTransportCellData : CTPriceCellData

@end


@interface CTTransportCell : CTPriceCell

@property (nonatomic, weak) id<CTTransportCellProtocol>transportCellDelegate;

@end
