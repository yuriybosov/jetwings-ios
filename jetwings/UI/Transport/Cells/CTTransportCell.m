//
//  CTTransportCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/16/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTTransportCell.h"

@implementation CTTransportCellData

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return self.selected ? 80 : 44;
}

- (NSString *)cellIdentifier
{
    return @"transportCell";
}

@end


@implementation CTTransportCell

- (NSNumber *)priceOfModel
{
    CTTransportCellData *aCellData = (CTTransportCellData *)self.cellData;
    
    NSNumber *price = nil;
    
    if ([self.transportCellDelegate showSmallPrice])
    {
        if ([aCellData.model respondsToSelector:@selector(priceSmall)])
        {
            price = [aCellData.model priceSmall];
        }
    }
    else
    {
        if ([aCellData.model respondsToSelector:@selector(priceBig)])
        {
            price = [aCellData.model priceBig];
        }
    }
    
    price = price ?: [[aCellData priceData] priceValue];
    
    return price;
}

@end
