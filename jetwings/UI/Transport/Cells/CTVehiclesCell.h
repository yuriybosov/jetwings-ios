//
//  CTVehiclesCell.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/16/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <CaigudaKit/CaigudaKit.h>
#import "BOVehicle.h"

@interface CTVehiclesCellData : CTCellDataModeled

- (BOVehicle *)vehicle;

@end


@interface CTVehiclesCell : CTCell

@property (nonatomic, weak) IBOutlet UILabel *lbTitle;

@end
