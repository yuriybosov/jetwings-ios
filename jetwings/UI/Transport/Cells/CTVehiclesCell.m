//
//  CTVehiclesCell.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/16/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTVehiclesCell.h"

@implementation CTVehiclesCellData

- (id)initWithModel:(id)aModel
{
    self = [super initWithModel:aModel];
    if (self)
    {
        self.cellSelectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (BOVehicle *)vehicle
{
    return self.model;
}

- (CGFloat)cellHeightForWidth:(CGFloat)aWidth
{
    return 35;
}

- (NSString *)cellIdentifier
{
    return @"vehiclesCell";
}

@end


@implementation CTVehiclesCell

- (void)setupCellData:(CTVehiclesCellData *)aCellData
{
    [super setupCellData:aCellData];
    self.lbTitle.text = aCellData.vehicle.model;
}

@end
