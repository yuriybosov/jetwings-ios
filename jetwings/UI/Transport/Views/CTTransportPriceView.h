//
//  CTTransportPriceView.h
//  jetwings
//
//  Created by Yuriy Bosov on 1/17/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTTransportPriceView : UIView

@property (nonatomic, weak) IBOutlet UILabel *lbSeatsTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbPriceTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbPriceValue;
@property (nonatomic, weak) IBOutlet UISwitch *seatsSwitch;

+ (CTTransportPriceView *)create;

@end
