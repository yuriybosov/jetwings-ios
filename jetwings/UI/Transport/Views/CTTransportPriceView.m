//
//  CTTransportPriceView.m
//  jetwings
//
//  Created by Yuriy Bosov on 1/17/16.
//  Copyright © 2016 jetwings. All rights reserved.
//

#import "CTTransportPriceView.h"

@implementation CTTransportPriceView

+ (CTTransportPriceView *)create
{
    CTTransportPriceView *view = [[[NSBundle mainBundle] loadNibNamed:@"CTTransportPriceView" owner:nil options:nil] firstObject];
    
    view.lbSeatsTitle.text = CTLocalizedString(@"12 and more seats");
    view.lbPriceTitle.text = CTLocalizedString(@"Total price");
    view.lbPriceValue.text = [NSString stringWithFormat:@"%@0.00",[CTAppManagerInstance.currentUser currencySymbol]];
    view.seatsSwitch.on = NO;
    
    view.frame = CGRectMake(0, 0, 320, 120);
    view.translatesAutoresizingMaskIntoConstraints = YES;
    
    return view;
}

@end
